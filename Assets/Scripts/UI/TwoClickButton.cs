﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class TwoClickButton : MonoBehaviour {
    [SerializeField]
    TextMeshProUGUI mainText;
    [SerializeField]
    Image checkmark;
    [SerializeField]
    UnityEvent unityEvent;
    System.Action click;
    System.Action<Tile> clickTile;
    System.Action<int> clickInt;
    bool confirm;
    string otherText;
    Tile tile;
    int pos;

    private void Awake()
    {
        if (checkmark == null)
        {
            checkmark = GetComponent<Image>();
        }
        if(mainText == null)
        {
            mainText = GetComponentInChildren<TextMeshProUGUI>();
        }
        if(mainText == null)
        {
            Debug.Log("I Dont have a TextmeshPro " + gameObject.name);
        }
        if (unityEvent != null)
        {
            //TouchMaster.instance.ClearTwoClick += ResetConfirm;
            otherText = mainText.text;
        }
    }
    private void Start()
    {
        SetupParied();
    }
    public void Setup(System.Action<Tile> tileClick, string text, Tile t)
    {
        tile = t;
        clickTile = tileClick;
        otherText = text;
        mainText.text = text;
    }
    public void ChangeText(string text)
    {
        otherText = text;
    }
    public void Setup(System.Action confirmClick, string text)
    {
        mainText.text = text;
        otherText = text;
        click = confirmClick;
    }
    public void Setup(System.Action<int> confirmClick, string text,int pos)
    {
        this.pos = pos;
        mainText.text = text;
        otherText = text;
        clickInt = confirmClick;
    }
    public List<TwoClickButton> pairedList = new List<TwoClickButton>();
    public System.Action<TwoClickButton> EventPaired;
    void SetupParied()
    {
        for (int i = 0; i < pairedList.Count; i++)
        {
            pairedList[i].EventPaired += ResetPaired;
        }
    }
    void ResetPaired(TwoClickButton b)
    {
        for (int i = 0; i < pairedList.Count; i++)
        {
            if (pairedList[i] == b) continue;
            pairedList[i].ResetConfirm(pairedList[i]);
        }
    }
    public void ResetConfirm(TwoClickButton p)
    {
        if (gameObject == null) return;
        if (mainText == null) return;
        confirm = false;
        checkmark.gameObject.SetActive(false);
        mainText.text = otherText;
    }
    public void Click()
    {
        if(!confirm)
        {
            confirm = true;
            checkmark.gameObject.SetActive(true);
            EventPaired?.Invoke(this);
        }
        else
        {
            if(clickTile != null)
            {
                clickTile(tile);
            }
            if(click != null)
            {
                click();
            }
            if(clickInt != null)
            {
                clickInt(pos);
            }
            if(unityEvent != null)
            {
                unityEvent.Invoke();
            }
        }
    }
    float resetTimer = 5;
    float currentResetTime;
    private void Update()
    {
        if(confirm)
        {
            currentResetTime -= Time.deltaTime;
            if(currentResetTime < 0)
            {
                ResetConfirm(null);
            }
        }
        else
        {
            currentResetTime = resetTimer;
        }
    }
}
