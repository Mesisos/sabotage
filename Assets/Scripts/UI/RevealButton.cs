﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class RevealButton : MonoBehaviour {
    [SerializeField]
    TextMeshProUGUI mainText;
    [SerializeField]
    TwoClickButton[] buttons;
    Tile tile;
    System.Action<Tile, int> callBack;
    public void Setup(Tile t, System.Action<Tile, int> call, string[] buttonTexts, string showText)
    {
        mainText.text = showText;
        callBack = call;
        tile = t;
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < buttonTexts.Length; i++)
        {
            buttons[i].gameObject.SetActive(true);
            buttons[i].Setup(Clicked, buttonTexts[i], i);
        }
    }
    public void Clicked(int pos)
    {
        if(callBack != null)
        {
            callBack(tile, pos);
        }
        Destroy(buttons[pos].gameObject);
    }

}
