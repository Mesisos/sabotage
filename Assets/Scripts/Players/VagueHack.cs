﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VagueHack : MonoBehaviour {
    [SerializeField]
    GameObject button;

    public void ButtonConfirmed()
    {
        button.GetComponent<TwoClickButton>().ResetConfirm(null);
        button.gameObject.SetActive(false);
        Debug.Log("Vague Hacked");
        GuardMaster.instance.modifiers++;
        HackMaster.instnace.AddHack();
    }
}
