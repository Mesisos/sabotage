﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Doozy.Engine.UI;

public class PushGuard : OverlayBase {
    public static PushGuard instance;

    private void Awake()
    {
        instance = this;
    }
    [SerializeField]
    TwoClickButton pre;
    [SerializeField]
    Button mainButton;
    public List<TwoClickButton> list = new List<TwoClickButton>();
    [SerializeField]
    UIView pushView;
    [SerializeField]
    UIView playerView;
    public override void Setup()
    {
        base.Setup();
        mainButton.GetComponentInChildren<TextMeshProUGUI>().text = "Cancel";
        CreateButtons();
        pushView.Show();
        playerView.Hide();
    }
    public void ButtonPressed()
    {
        if (list.Count > 0)
        {
            ClearOverlay();
        }
        else
        {
            Setup();
        }
    }
    void CreateButtons()
    {
        for (int i = 0; i < GridMaster.instnace.allTiles.Count; i++)
        {
            CreateButton(GridMaster.instnace.allTiles[i]);
        }
        for (int i = 0; i < list.Count; i++)
        {
            list[0].pairedList.Add(list[i]);
        }
    }
    void CreateButton(Tile til)
    {
        Transform t = Instantiate(pre.transform) as Transform;
        TwoClickButton b = t.GetComponent<TwoClickButton>();
        Vector3 tilePos = til.transform.position;
        float amount = til.GetComponent<RectTransform>().sizeDelta.x / 2;
        tilePos.y += amount;
        tilePos.x += amount;
        t.SetParent(transform, false);
        t.transform.position = tilePos;
        b.Setup(ButtonPressed, "False Alarm", til);
        list.Add(b);
    }
    public override void ClearOverlay()
    {
        if(overlayShown)
        {
            pushView.Hide();
            playerView.Show();
        }
        base.ClearOverlay();
        TwoClickButton[] temp = list.ToArray();
        for (int i = 0; i < temp.Length; i++)
        {
            Destroy(temp[i].gameObject);
        }
        list.Clear();
        mainButton.GetComponentInChildren<TextMeshProUGUI>().text = "False Alarm";
    }
    public void ButtonPressed(Tile t)
    {
        Debug.Log(t.cord.ToString() + " Stunned");
        Guard temp = null;
        for (int i = 0; i < GuardMaster.instance.guard.Length; i++)
        {
            if (GuardMaster.instance.guard[i].tile.cord == t.cord)
            {
                temp = GuardMaster.instance.guard[i];
            }
        }
        if (temp != null)
        {
            for (int i = 0; i < GuardMaster.instance.guard.Length; i++)
            {
                if (temp != GuardMaster.instance.guard[i])
                {
                    temp.Move(GuardMaster.instance.guard[i].tile);
                    break;
                }
            }
        }
        ClearOverlay();
    }
}
