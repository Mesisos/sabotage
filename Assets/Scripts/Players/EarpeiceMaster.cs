﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarpeiceMaster : MonoBehaviour {
    [SerializeField]
    GameObject button;

    public void ButtonConfirmed()
    {
        button.GetComponent<TwoClickButton>().ResetConfirm(null);
        button.gameObject.SetActive(false);
        Debug.Log("Ear Piece");
        ScanFinder.instance.Ear(PlayerMaster.instnace.playerOne ? PlayerMaster.instnace.players[1].playerID : PlayerMaster.instnace.players[0].playerID);
    }
}
