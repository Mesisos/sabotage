﻿using Doozy.Engine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SmokeMaster : OverlayBase
{
    public static SmokeMaster instnace;

    private void Awake()
    {
        instnace = this;
    }
    [SerializeField]
    TwoClickButton pre;
    [SerializeField]
    Button mainButton;
    public List<TwoClickButton> list = new List<TwoClickButton>();
    [SerializeField]
    UIView stunView;
    [SerializeField]
    UIView playerView;
    public override void Setup()
    {
        base.Setup();
        mainButton.GetComponentInChildren<TextMeshProUGUI>().text = "Cancel";
        CreateButtons();
        stunView.Show();
        playerView.Hide();
        overlayShown = true;
    }
    public void ButtonPressed()
    {
        if (list.Count > 0)
        {
            ClearOverlay();
        }
        else
        {
            Setup();
        }
    }
    void CreateButtons()
    {
        for (int i = 0; i < GridMaster.instnace.allTiles.Count; i++)
        {
            CreateButton(GridMaster.instnace.allTiles[i]);
        }
        for (int i = 0; i < list.Count; i++)
        {
            list[0].pairedList.Add(list[i]);
        }
    }
    void CreateButton(Tile til)
    {
        Transform t = Instantiate(pre.transform) as Transform;
        TwoClickButton b = t.GetComponent<TwoClickButton>();
        Vector3 tilePos = til.transform.position;
        float amount = til.GetComponent<RectTransform>().sizeDelta.x / 2;
        tilePos.y += amount;
        tilePos.x += amount;
        t.SetParent(transform, false);
        t.transform.position = tilePos;
        b.Setup(ButtonPressed, "Smoke", til);
        list.Add(b);
    }
    public override void ClearOverlay()
    {
        if (overlayShown)
        {
            stunView.Hide();
            playerView.Show();
        }
        base.ClearOverlay();
        TwoClickButton[] temp = list.ToArray();
        for (int i = 0; i < temp.Length; i++)
        {
            Destroy(temp[i].gameObject);
        }
        list.Clear();
        mainButton.GetComponentInChildren<TextMeshProUGUI>().text = "Smoke Bomb";
        overlayShown = false;
    }
    public void ButtonPressed(Tile t)
    {
        mainButton.gameObject.SetActive(false);
        ClearOverlay();
    }
}
