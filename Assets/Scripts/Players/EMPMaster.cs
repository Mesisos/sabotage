﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Doozy.Engine.UI;

public class EMPMaster : OverlayBase {
    public static EMPMaster instnace;

    private void Awake()
    {
        instnace = this;
    }
    [SerializeField]
    TwoClickButton pre;
    public List<TwoClickButton> list = new List<TwoClickButton>();
    [SerializeField]
    Button mainButton;
    [SerializeField]
    UIView empView;
    [SerializeField]
    UIView playerView;
    public override void Setup()
    {
        base.Setup();
        CreateButtons();
        empView.Show();
        playerView.Hide();
    }
    public void ButtonPressed()
    {
        if (list.Count > 0)
        {
            ClearOverlay();
        }
        else
        {
            Setup();
        }
    }
    void CreateButtons()
    {
        for (int i = 0; i < GridMaster.instnace.allTiles.Count; i++)
        {
            CreateButton(GridMaster.instnace.allTiles[i]);
        }
        for (int i = 0; i < list.Count; i++)
        {
            list[0].pairedList.Add(list[i]);
        }
    }
    void CreateButton(Tile til)
    {
        Transform t = Instantiate(pre.transform) as Transform;
        TwoClickButton b = t.GetComponent<TwoClickButton>();
        Vector3 tilePos = til.transform.position;
        float amount = til.GetComponent<RectTransform>().sizeDelta.x / 2;
        tilePos.y += amount;
        tilePos.x += amount;
        t.SetParent(transform, false);
        t.transform.position = tilePos;
        b.Setup(ButtonPressed, "EMP", til);
        list.Add(b);
    }
    public override void ClearOverlay()
    {
        if (overlayShown)
        {
            empView.Hide();
            playerView.Show();
        }
        base.ClearOverlay();
        TwoClickButton[] temp = list.ToArray();
        for (int i = 0; i < temp.Length; i++)
        {
            Destroy(temp[i].gameObject);
        }
        list.Clear();
    }
    public void ButtonPressed(Tile t)
    {
        Debug.Log(t.cord.ToString() + " EMPED");
        Tile[] adj = GridMaster.instnace.GetAllTilesAround(t, false);
        for (int i = 0; i < adj.Length; i++)
        {
            if (adj == null) continue;
            HackMaster.instnace.HackTile(adj[i]);
        }
        ScanFinder.instance.PlayerHere(t, PlayerMaster.instnace.GetCurrentPlayer().playerID);
        mainButton.gameObject.SetActive(false);
        ClearOverlay();
    }
}
