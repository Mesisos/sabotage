﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Doozy.Engine.UI;

public class HackMaster : OverlayBase
{
    public static HackMaster instnace;

    private void Awake()
    {
        instnace = this;
    }
    public int numberOfHacks;
    [SerializeField]
    TwoClickButton pre;
    public TextMeshProUGUI mainText;
    [SerializeField]
    Button mainButton;
    public List<TwoClickButton> list = new List<TwoClickButton>();
    [SerializeField]
    UIView hackView;
    [SerializeField]
    UIView playerView;

    public override void Setup()
    {
        base.Setup();
        mainButton.GetComponentInChildren<TextMeshProUGUI>().text = "Cancel";
        CreateButtons();
        hackView.Show();
        playerView.Hide();
    }
    public void ButtonPressed()
    {
        if (list.Count > 0)
        {
            ClearOverlay();
        }
        else
        {
            Setup();
        }
    }
    void CreateButtons()
    {
        for (int i = 0; i < GridMaster.instnace.doomsDays.Count; i++)
        {
            if (TearGasMaster.instance.tearGasedTiles.Contains(GridMaster.instnace.doomsDays[i]))
            {
                continue;
            }
            CreateButton(GridMaster.instnace.doomsDays[i]);
        }
        for (int i = 0; i < GridMaster.instnace.generators.Count; i++)
        {
            if (TearGasMaster.instance.tearGasedTiles.Contains(GridMaster.instnace.generators[i]))
            {
                continue;
            }
            CreateButton(GridMaster.instnace.generators[i]);
        }
        for (int i = 0; i < list.Count; i++)
        {
            list[0].pairedList.Add(list[i]);
        }
    }
    void CreateButton(Tile til)
    {
        Transform t = Instantiate(pre.transform) as Transform;
        TwoClickButton b = t.GetComponent<TwoClickButton>();
        Vector3 tilePos = til.transform.position;
        float amount = til.GetComponent<RectTransform>().sizeDelta.x / 2;
        tilePos.y += amount;
        tilePos.x += amount;
        t.SetParent(transform, false);
        t.transform.position = tilePos;
        b.Setup(ButtonPressed, "Hack", til);
        list.Add(b);
    }
    public override void ClearOverlay()
    {
        if(overlayShown)
        {
            hackView.Hide();
            playerView.Show();
        }
        base.ClearOverlay();
        TwoClickButton[] temp = list.ToArray();
        for (int i = 0; i < temp.Length; i++)
        {
            Destroy(temp[i].gameObject);
        }
        list.Clear();
        mainButton.GetComponentInChildren<TextMeshProUGUI>().text = "Hack";
    }
    public void ButtonPressed(Tile t)
    {
        ScanFinder.instance.Hack(t);
        HackTile(t);
        ClearOverlay();
    }
    public void HackTile(Tile t)
    {
        Debug.Log(t.cord.ToString() + " Hacked");
        GeneratorMaster.instance.Hack(t);
        for (int i = 0; i < GridMaster.instnace.doomsDays.Count; i++)
        {
            if (t.cord == GridMaster.instnace.doomsDays[i].cord)
            {
                GuardMaster.instance.AddModify();
                GridMaster.instnace.doomsDays[i].TakeDoomsDayCount();
                AddHack();
                break;
            }
        }
        mainButton.gameObject.SetActive(false);
    }
    public void AddHack()
    {
        numberOfHacks++;
        mainText.text = "Hacks: " + numberOfHacks + "/" + (5 + (int)GameMaster.instance.currentUnlock);
        if(numberOfHacks >= 5 + (int)GameMaster.instance.currentUnlock)
        {
            GameOver.instance.GameFinished(true);
        }
    }
}
