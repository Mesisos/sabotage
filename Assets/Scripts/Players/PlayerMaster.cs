﻿using Doozy.Engine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class PlayerMaster : PhaseBase {
    public static PlayerMaster instnace;

    private void Awake()
    {
        instnace = this;
    }
    [SerializeField]
    OverlayBase[] allOverlays;
    [SerializeField]
    Button endTurnBut;
    [SerializeField]
    Button[] playerChosenButtons;
    [SerializeField]
    Image[] playerChooseImages;
    [SerializeField]
    TextMeshProUGUI playerText;
    public bool playerOne;
    [SerializeField]
    GameObject[] hawkButtons;
    [SerializeField]
    GameObject[] hackerButtons;
    [SerializeField]
    GameObject[] juicerButtons;
    [SerializeField]
    GameObject[] acrobatButtons;
    public PlayerInfo[] players;
    [SerializeField]
    Image playerImage;
    [SerializeField]
    GameObject parachuteButton;
    [SerializeField]
    GameObject[] basicButtons;
    [SerializeField]
    Button hackButton;
    [SerializeField]
    UIView uIView;
    [SerializeField]
    UIView chooseView;
    private void Start()
    {
        gameObject.SetActive(false);
        endTurnBut.GetComponent<TwoClickButton>().Setup(NextPlayer, "Next Player Turn");
    }
    public void ResetAllOverlays()
    {
        for (int i = 0; i < allOverlays.Length; i++)
        {
            allOverlays[i].ClearOverlay();
        }
    }
    public void AddPlayer(string str)
    {
        if(players == null)
        {
            players = new PlayerInfo[2];
        }
        for (int i = 0; i < players.Length; i++)
        {
            if(players[i] == null)
            {
                players[i] = new PlayerInfo(str);
                break;
            }
        }
    }
    public override void StartPhase()
    {
        if(players == null)
        {
            players = new PlayerInfo[2];
            players[0] = new PlayerInfo("Hacker");
            players[1] = new PlayerInfo("Hawk");
        }
        base.StartPhase();
        gameObject.SetActive(true);
        playerOne = true;
        if (players[0].killed && players[1].killed)
        {
            players[0].killed = false;
            players[1].killed = false;
            EndPhase();
            return;
        }else if(players[0].killed)
        {
            players[0].killed = false;
            NextPlayer();
            return;
        }else if(players[1].killed)
        {
            players[1].killed = false;
            PlayerInfo temp = players[0];
            players[0] = players[1];
            players[1] = temp;
            NextPlayer();
            return;
        }
        chooseView.Show();
        for (int i = 0; i < 2; i++)
        {
            playerChosenButtons[i].GetComponentInChildren<TextMeshProUGUI>().text = "Take\n" + players[i].className + "\nTurn";
            playerChooseImages[i].sprite = IconMaster.instnace.PlayerNameToSprite(players[i].className);
        }
    }
    public void StartPlayersTurn()
    {
        endTurnBut.GetComponent<TwoClickButton>().ResetConfirm(null);
        endTurnBut.GetComponent<TwoClickButton>().ChangeText("Next Players Turn");
        endTurnBut.GetComponentInChildren<TextMeshProUGUI>().text = "Next Players Turn";
        TurnOnButtons(players[0].className, players[0].parachute);
        playerText.text = players[0].className + " Turn";
        playerImage.gameObject.SetActive(true);
        playerImage.sprite = IconMaster.instnace.PlayerNameToSprite(players[0].className);
    }
    public void Parachute()
    {
        if(playerOne)
        {
            players[0].parachute = false;
            TurnOnButtons(players[0].className, false);
        }
        else
        {
            players[1].parachute = false;
            TurnOnButtons(players[1].className, false);
        }
        for (int i = 0; i < basicButtons.Length; i++)
        {
            basicButtons[i].gameObject.SetActive(true);
        }
        hackButton.interactable = false;
        hawkButtons[0].GetComponent<Button>().interactable = false;
        juicerButtons[1].GetComponent<Button>().interactable = false;
        parachuteButton.gameObject.SetActive(false);
    }
    public void NextPlayer()
    {
        if(GameMaster.instance.currentPhase != Phase.Spy)
        {
            Debug.Log("Not in Player Phase: " + GameMaster.instance.currentPhase);
            return;
        }
        if (!playerOne)
        {
            EndPhase();
            return;
        }
        uIView.Show();
        endTurnBut.GetComponent<TwoClickButton>().ResetConfirm(null);
        playerText.text = players[1].className + " Turn";
        TurnOnButtons(players[1].className, players[1].parachute);
        playerImage.sprite = IconMaster.instnace.PlayerNameToSprite(players[1].className);
        endTurnBut.GetComponentInChildren<TextMeshProUGUI>().text = "End Players Turn";
        endTurnBut.GetComponent<TwoClickButton>().ChangeText("End Players Turn");
        playerOne = false;
    }
    public string GetCurrentPlayerName()
    {
        return GetCurrentPlayer().className;
    }
    public PlayerInfo GetCurrentPlayer()
    {
        if(playerOne)
        {
            return players[0];
        }
        return players[1];
    }
    void TurnOnButtons(string name, bool parachute)
    {
        for (int i = 0; i < hackerButtons.Length; i++)
        {
            hackerButtons[i].SetActive(false);
            hackerButtons[i].GetComponent<Button>().interactable = true;
        }
        for (int i = 0; i < hawkButtons.Length; i++)
        {
            hawkButtons[i].SetActive(false);
            hawkButtons[i].GetComponent<Button>().interactable = true;
        }
        for (int i = 0; i < juicerButtons.Length; i++)
        {
            juicerButtons[i].SetActive(false);
            juicerButtons[i].GetComponent<Button>().interactable = true;
        }
        for (int i = 0; i < acrobatButtons.Length; i++)
        {
            acrobatButtons[i].SetActive(false);
            acrobatButtons[i].GetComponent<Button>().interactable = true;
        }
        if(parachute)
        {
            for (int i = 0; i < basicButtons.Length; i++)
            {
                basicButtons[i].gameObject.SetActive(false);
            }
            parachuteButton.gameObject.SetActive(true);
            return;
        }
        else
        {
            hackButton.interactable = true;
            for (int i = 0; i < basicButtons.Length; i++)
            {
                basicButtons[i].gameObject.SetActive(true);
            }
            parachuteButton.gameObject.SetActive(false);
        }
        if (GameMaster.instance.currentUnlock == GameUnlock.Tier0) return;
        switch(name)
        {
            case "Hawk":
                for (int i = 0; i < hawkButtons.Length; i++)
                {
                    hawkButtons[i].SetActive(true);
                }
                return;
            case "Hacker":
                for (int i = 0; i < hackerButtons.Length; i++)
                {
                    hackerButtons[i].SetActive(true);
                }
                return;
            case "Juicer":
                for (int i = 0; i < juicerButtons.Length; i++)
                {
                    juicerButtons[i].SetActive(true);
                }
                return;
            case "Acrobat":
                for (int i = 0; i < acrobatButtons.Length; i++)
                {
                    acrobatButtons[i].SetActive(true);
                }
                return;
        }
    }
    public override void EndPhase()
    {
        base.EndPhase();
        uIView.Hide();
    }
    public void PlayerStartChosen(int pos)
    {
        if(pos == 1)
        {
            PlayerInfo p = players[0];
            players[0] = players[1];
            players[1] = p;
        }
        chooseView.Hide();
        uIView.Show();
        StartPlayersTurn();
    }
    public void PlayerShot(int pos)
    {
        players[pos].killed = true;
        players[pos].parachute = true;
    }
    public int GetPlayersID()
    {
        if(playerOne)
        {
            return players[0].playerID;
        }
        return players[1].playerID;
    }
    public PlayerInfo IdToPlayer(int id)
    {
        return players[id];
    }
    public class PlayerInfo
    {
        public string className;
        public string revealableItem;
        public string shootableItem;
        public bool parachute = true;
        public bool killed;
        public int playerID;
        public PlayerInfo(string name)
        {
            className = name;
            Setup();
        }
        void Setup()
        {
            playerID = UnityEngine.Random.Range(int.MinValue, int.MaxValue);
            switch(className)
            {
                case "Hacker":
                    shootableItem = "Hologram";
                    revealableItem = "Hologram";
                    break;
                case "Hawk":
                    revealableItem = "Beacon";
                    shootableItem = "Beacon";
                    break;
                case "Juicer":
                    revealableItem = "Feedback";
                    shootableItem = "Feedback";
                    break;
                case "Acrobat":
                    revealableItem = "Virus";
                    shootableItem = "Virus";
                    break;

            }
        }

    }
}
