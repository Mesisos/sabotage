﻿using Doozy.Engine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ParachuteMaster : OverlayBase
{
    public static ParachuteMaster instance;

    private void Awake()
    {
        instance = this;
    }
    [SerializeField]
    Button scanButtons;
    [SerializeField]
    Button mainButton;
    [SerializeField]
    List<ScanButton> allButtons = new List<ScanButton>();
    [SerializeField]
    UIView view;
    [SerializeField]
    UIView playerView;
    bool blockDoubleClick;
    public override void Setup()
    {
        base.Setup();
        playerView.Hide();
        view.Show();
        blockDoubleClick = false;
        for (int i = 0; i < allButtons.Count; i++)
        {
            allButtons[i].PlacedInit(HighlightTiles, ButtonPressed);
        }
    }
    public void ButtonPressed()
    {
        if (view.IsVisible)
        {
            ClearOverlay();
        }
        else
        {
            Setup();
        }
    }
    public override void ClearOverlay()
    {
        if(overlayShown)
        {
            view.Hide();
            playerView.Show();
        }
        base.ClearOverlay();
        ClearHightlights();
        for (int i = 0; i < allButtons.Count; i++)
        {
            allButtons[i].Clear();
        }
    }
    void CreateButtons()
    {
        string[] rows = new string[]
        {
            "Bottom Half",
            "Top Half"
        };
        string[] columes = new string[]
        {
            "Left Half",
            "Right Half",
        };
        int rowCount = 0;
        int columeCount = 0;
        for (int i = 0; i < GridMaster.instnace.allTiles.Count; i++)
        {
            Tile t = GridMaster.instnace.allTiles[i];
            if (t.cord.x == 0 && (t.cord.y == 0 || t.cord.y == GridMaster.instnace.gridSizeY - 1))
            {
                CreateButton(t, ScanType.Row, rows[rowCount]);
                rowCount++;
            }
            if (t.cord.y == GridMaster.instnace.gridSizeY - 1 && (t.cord.x == 0 || t.cord.x == GridMaster.instnace.gridSizeX - 1))
            {
                CreateButton(t, ScanType.Colume, columes[columeCount]);
                columeCount++;
            }
        }
    }
    void CreateButton(Tile pos, ScanType s, string subText)
    {
        Transform t = Instantiate(scanButtons.transform) as Transform;
        t.SetParent(transform, false);
        ScanButton b = t.GetComponent<ScanButton>();
        b.Setup(pos, s, HighlightTiles, ButtonPressed, subText, "Parachute");
        allButtons.Add(b);
    }
    public void ResetAllButtonExcept(Tile t)
    {
        for (int i = 0; i < allButtons.Count; i++)
        {
            if (allButtons[i].tile.cord != t.cord)
            {
                allButtons[i].Clear();
            }
        }
    }
    public void ClearAllButtons()
    {
        for (int i = 0; i < allButtons.Count; i++)
        {
            allButtons[i].Clear();
        }
    }
    public void HighlightTiles(Tile t, ScanType type)
    {
        if(blockDoubleClick)
        {
            return;
        }
        ResetAllButtonExcept(t);
        ClearHightlights();
        Tile[] tiles = GetTilesScanned(t, type);
        for (int i = 0; i < tiles.Length; i++)
        {
            tiles[i].Highlight(Color.yellow);
        }
    }
    void ClearHightlights()
    {
        for (int i = 0; i < GridMaster.instnace.allTiles.Count; i++)
        {
            GridMaster.instnace.allTiles[i].ClearHightlight();
        }
    }
    Tile[] GetTilesScanned(Tile t, ScanType type)
    {
        List<Tile> list = new List<Tile>();
        for (int i = 0; i < GridMaster.instnace.allTiles.Count; i++)
        {
            Tile temp = GridMaster.instnace.allTiles[i];
            if (type == ScanType.Colume)
            {
                if(t.cord.x == 0 && (temp.cord.x == 0 || temp.cord.x == 1))
                {
                    list.Add(temp);
                }
                else if(t.cord.x == GridMaster.instnace.gridSizeX - 1 && (temp.cord.x == 2 || temp.cord.x == 3))
                {
                    list.Add(temp);
                }
            }
            else {
                if (t.cord.y == 0 && (temp.cord.y == 0 || temp.cord.y == 1))
                {
                    list.Add(temp);
                }
                else if (t.cord.y == GridMaster.instnace.gridSizeY - 1 && (temp.cord.y == 2 || temp.cord.y == 3))
                {
                    list.Add(temp);
                }
            }
        }
        return list.ToArray();
    }
    public void ButtonPressed(ScanButton s)
    {
        if (blockDoubleClick)
        {
            return;
        }
        blockDoubleClick = true;
        ClearOverlay();
        ScanFinder.instance.Scanned(GetTilesScanned(s.tile, s.type));
        PlayerMaster.instnace.Parachute();
    }
}
