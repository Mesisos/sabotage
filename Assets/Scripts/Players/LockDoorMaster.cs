﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LockDoorMaster : OverlayBase {
    public static LockDoorMaster instnace;

    private void Awake()
    {
        instnace = this;
    }
    public List<Tile> lockedList = new List<Tile>();

    public void Add(Tile t)
    {
        lockedList.Add(t);
    }
    public void Remove(Tile t)
    {
        lockedList.Remove(t);
    }
    [SerializeField]
    TwoClickButton pre;
    [SerializeField]
    Button mainButton;
    public List<TwoClickButton> list = new List<TwoClickButton>();
    public override void Setup()
    {
        base.Setup();
        //mainButton.GetComponentInChildren<Text>().text = "Cancel";
        CreateButtons();
    }
    public void ButtonPressed()
    {
        if (list.Count > 0)
        {
            ClearOverlay();
        }
        else
        {
            Setup();
        }
    }
    void CreateButtons()
    {
        for (int i = 0; i < GridMaster.instnace.allTiles.Count; i++)
        {
            if (lockedList.Contains(GridMaster.instnace.allTiles[i])) continue;
            CreateButton(GridMaster.instnace.allTiles[i]);
        }
    }
    void CreateButton(Tile til)
    {
        Transform t = Instantiate(pre.transform) as Transform;
        TwoClickButton b = t.GetComponent<TwoClickButton>();
        Vector3 tilePos = til.transform.position;
        float amount = til.GetComponent<RectTransform>().sizeDelta.x / 2;
        tilePos.y += amount;
        tilePos.x += amount;
        t.SetParent(transform, false);
        t.transform.position = tilePos;
        b.Setup(ButtonPressed, "Lock", til);
        list.Add(b);
    }
    public override void ClearOverlay()
    {
        base.ClearOverlay();
        TwoClickButton[] temp = list.ToArray();
        for (int i = 0; i < temp.Length; i++)
        {
            Destroy(temp[i].gameObject);
        }
        list.Clear();
        //mainButton.GetComponentInChildren<Text>().text = "Locked Doors";
    }
    public void ButtonPressed(Tile t)
    {
        Debug.Log(t.cord.ToString() + " Locked");
        Add(t);
        mainButton.gameObject.SetActive(false);
        ClearOverlay();
    }
}
