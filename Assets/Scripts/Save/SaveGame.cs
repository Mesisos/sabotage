﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
[System.Serializable]
public class SaveGame
{
    public UnityEngine.Random.State state;
    public WallCord[] walls;
    public Cord[] dooms;
    public Cord[] gens;
    public static string SaveDir = "/Save/";
    public static string SavePath = "TheSave.sav";
    public static void Save(SaveGame s)
    {
        if (!System.IO.Directory.Exists(Application.persistentDataPath + SaveDir))
        {
            System.IO.Directory.CreateDirectory(Application.persistentDataPath + SaveDir);
        }
        if (File.Exists(Application.persistentDataPath + SaveDir + SavePath))
        {
            File.Delete(Application.persistentDataPath + SaveDir + SavePath);
        }
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + SaveDir + SavePath);
        bf.Serialize(file, s);
        file.Close();
    }
    public static SaveGame Load(byte[] path)
    {
        MemoryStream memStream = new MemoryStream();
        BinaryFormatter binForm = new BinaryFormatter();

        memStream.Write(path, 0, path.Length);
        memStream.Seek(0, SeekOrigin.Begin);

        SaveGame obj = (SaveGame)binForm.Deserialize(memStream);

        return obj;
    }
    public static SaveGame Load()
    {
        if (File.Exists(Application.persistentDataPath + SaveDir + SavePath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + SaveDir + SavePath, FileMode.Open);
            SaveGame s = (SaveGame)bf.Deserialize(file);
            file.Close();
            return s;
        }
        else
        {
            return null;
        }
    }
    public static void Delete()
    {
        if (File.Exists(Application.persistentDataPath + SaveDir + SavePath))
        {
            File.Delete(Application.persistentDataPath + SaveDir + SavePath);
        }
    }
}
