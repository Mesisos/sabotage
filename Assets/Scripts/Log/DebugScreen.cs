﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class DebugScreen : MonoBehaviour {
    [SerializeField]
    TextMeshProUGUI debugText;
    [SerializeField]
    GameObject parent;
    public static DebugScreen instnace;
    private void Awake()
    {
        instnace = this;
    }
    private void Start()
    {
        Application.logMessageReceivedThreaded += Application_logMessageReceived;
    }
    private void Application_logMessageReceived(string condition, string stackTrace, LogType type)
    {
        if (type != LogType.Error) return;
        LogDebug(condition + stackTrace);
    }
    private void OnDestroy()
    {
        Application.logMessageReceivedThreaded -= Application_logMessageReceived;
    }
    public void LogDebug(string s)
    {
        if (debugText == null) return;
        debugText.text = s;
        parent.gameObject.SetActive(true);
    }
    public void Cancel()
    {
        parent.gameObject.SetActive(false);
    }
}
