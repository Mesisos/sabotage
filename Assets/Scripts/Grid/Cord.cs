﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public struct Cord
{
    public int x;
    public int y;
    public Cord(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    public static Cord[] TilesToCords(Tile[] t)
    {
        Cord[] c = new Cord[t.Length];
        for (int i = 0; i < c.Length; i++)
        {
            c[i] = t[i].cord;
        }
        return c;
    }
    public static Cord TileToCord(Tile t)
    {
        return t.cord;
    }
    public static Cord NodeToCord(ANode n)
    {
        return new Cord(n.gridx, n.gridy);
    }
    public static int SumCord(Cord c1)
    {
        return c1.x + c1.y;
    }
    public static bool operator ==(Cord c1, Cord c2)
    {
        return c1.x == c2.x && c1.y == c2.y;
    }
    public static bool operator !=(Cord c1, Cord c2)
    {
        return c1.x != c2.x || c1.y != c2.y;
    }
    public static Cord operator +(Cord c1, Cord c2)
    {
        return new Cord(c1.x + c2.x, c1.y + c2.y);
    }
    public static Cord operator -(Cord c1, Cord c2)
    {
        return new Cord(c1.x - c2.x, c1.y - c2.y);
    }
    public static Cord operator *(Cord c1, int f)
    {
        return new Cord(c1.x * f, c1.y * f);
    }
    public bool CheckAdjcentCord(Cord c)
    {
        if (c.x + 1 == x && c.y == y)
        {
            return true;
        }
        if (c.x - 1 == x && c.y == y)
        {
            return true;
        }
        if (c.x == x && c.y + 1 == y)
        {
            return true;
        }
        if (c.x == x && c.y - 1 == y)
        {
            return true;
        }
        return false;
    }
    public override string ToString()
    {
        return "X: " + x + " Y: " + y;
    }
    public string CordToLetter()
    {
        if(x == 0 && y == 3)
        {
            return "A";
        }
        if (x == 1 && y == 3)
        {
            return "B";
        }
        if (x == 2 && y == 3)
        {
            return "C";
        }
        if (x == 3 && y == 3)
        {
            return "D";
        }
        if (x == 0 && y == 2)
        {
            return "E";
        }
        if (x == 1 && y == 2)
        {
            return "F";
        }
        if (x == 2 && y == 2)
        {
            return "G";
        }
        if (x == 3 && y == 2)
        {
            return "H";
        }
        if (x == 0 && y == 1)
        {
            return "I";
        }
        if (x == 1 && y == 1)
        {
            return "J";
        }
        if (x == 2 && y == 1)
        {
            return "K";
        }
        if (x == 3 && y == 1)
        {
            return "L";
        }
        if (x == 0 && y == 0)
        {
            return "M";
        }
        if (x == 1 && y == 0)
        {
            return "N";
        }
        if (x == 2 && y == 0)
        {
            return "O";
        }
        if (x == 3 && y == 0)
        {
            return "P";
        }
        return "Unknown";
    }
}
