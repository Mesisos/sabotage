﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridMaster : MonoBehaviour {
    static GridMaster _instnace;
    public static GridMaster instnace { get { return _instnace; } }

    private void Awake()
    {
        _instnace = this;
    }
    public int doomsDayCount;
    public int generatorCount;

    internal Wall GetWall(Tile end, WallPos up)
    {
        for (int i = 0; i < allWalls.Count; i++)
        {
            if(allWalls[i].cord.cord == end.cord && allWalls[i].cord.pos == up)
            {
                return allWalls[i];
            }
        }
        return null;
    }

    public int gridSizeY;
    public int gridSizeX;
    [SerializeField]
    float gridWidth;
    [SerializeField]
    float gridHeight;
    [SerializeField]
    float spacing;
    public int wallCount;
    [SerializeField]
    float wallSpacing;
    [SerializeField]
    Transform tile;
    [SerializeField]
    Transform wall;
    public List<Tile> allTiles = new List<Tile>();
    public List<Wall> allWalls = new List<Wall>();
    public List<Wall> allPossibleWalls = new List<Wall>();
    public List<Tile> doomsDays = new List<Tile>();
    public List<Tile> generators = new List<Tile>();
    public List<Cord> allCords = new List<Cord>();
    SaveGame save;
    void Start()
    {
        LoadState();
        if(save == null || save.walls == null || save.walls.Length < wallCount)
        {
            CreateGrid();
        }
    }
    public void SaveState()
    {
        if(save == null)
        {
            save = new SaveGame();
        }
        save.walls = WallCheck.ConvertWallToWallCord(allWalls.ToArray());
        save.dooms = TilesToCord(doomsDays.ToArray());
        save.gens = TilesToCord(generators.ToArray());
        SaveGame.Save(save);
    }
    void LoadState()
    {
        ClearMap();
        save = SaveGame.Load();
        if(save == null || save.walls == null || save.walls.Length < 1)
        {
            save = null;
            return;
        }
        LoadWalls(save.walls);
        LoadDoom(save.dooms);
        LoadGen(save.gens);
        MapFinished();
    }
    void LoadWalls(WallCord[] w)
    {
        for (int i = 0; i < w.Length; i++)
        {
            for (int k = 0; k < allPossibleWalls.Count; k++)
            {
                if(w[i] == allPossibleWalls[k].cord)
                {
                    AddWall(allPossibleWalls[k]);
                }
            }
        }
    }
    void LoadDoom(Cord[] c)
    {
        for (int i = 0; i < c.Length; i++)
        {
            for (int k = 0; k < allTiles.Count; k++)
            {
                if(c[i] == allTiles[k].cord)
                {
                    AddDoomsDay(allTiles[k]);
                }
            }
        }
    }
    void LoadGen(Cord[] c)
    {
        for (int i = 0; i < c.Length; i++)
        {
            for (int k = 0; k < allTiles.Count; k++)
            {
                if (c[i] == allTiles[k].cord)
                {
                    AddGen(allTiles[k]);
                }
            }
        }
    }
    void MapFinished()
    {
        GeneratorMaster.instance.LoadGenerators();
    }
    public void ClearMap()
    {
        for (int i = 0; i < allTiles.Count; i++)
        {
            allTiles[i].ResetTile();
        }
        for (int i = 0; i < allWalls.Count; i++)
        {
            allWalls[i].ResetWall();
        }
        allWalls.Clear();
        doomsDays.Clear();
        generators.Clear();
    }
    public void ResetMap()
    {
        ClearMap();
        CreateGrid();
    }
    void CreateGrid()
    {
        /*
        for (int i = 0; i < gridSizeX; i++)
        {
            for (int y = 0; y < gridSizeY; y++)
            {
                allCords.Add(new Cord(i, y));
                CreateTile(new Cord(i, y));
            }
        }
        */ 
        PlaceWalls(allCords.ToArray());
        ChooseDoomsDays();
        ChooseGenerators();
        GeneratorMaster.instance.LoadGenerators();
        SaveState();
    }
    public void PlaceWalls(Cord[] cords)
    {
        List<WallCord> allWalls = new List<WallCord>();
        for (int i = 0; i < wallCount; i++)
        {
            WallCord w = WallCheck.GetWallCord(cords, allWalls.ToArray(), gridSizeY - 1, gridSizeX - 1);
            allWalls.Add(w);
            AddPossbileWallToAllWalls(w);
        }
    }
    public void AddDoomsDay(Tile g)
    {
        doomsDays.Add(g);
        g.SetToDoomsDay();
    }
    public void AddGen(Tile g)
    {
        generators.Add(g);
        g.SetToGenerator();
        g.SetGeneratorCount(0);
    }
    public void ChooseDoomsDays()
    {
        Cord[] cords = Cord.TilesToCords(allTiles.ToArray());
        WallCord[] walls = WallCord.WallsToCords(allWalls.ToArray());
        while(doomsDayCount > doomsDays.Count)
        {
            Cord target = GetRandomTile().cord;
            Cord[] dooms = Cord.TilesToCords(doomsDays.ToArray());
            if(Pathing.DoomsDayGeneratorCheck(target, cords, walls, dooms))
            {
                doomsDays.Add(GetTile(target));
            }

        }
        for (int i = 0; i < doomsDays.Count; i++)
        {
            doomsDays[i].SetToDoomsDay();
        }
    }
    public void ChooseGenerators()
    {
        Cord[] cords = Cord.TilesToCords(allTiles.ToArray());
        WallCord[] walls = WallCord.WallsToCords(allWalls.ToArray());
        while (generatorCount > generators.Count)
        {
            Cord target = GetRandomTile().cord;
            List<Tile> temp = new List<Tile>();
            temp.AddRange(doomsDays);
            temp.AddRange(generators);
            Cord[] dooms = Cord.TilesToCords(temp.ToArray());
            if (Pathing.DoomsDayGeneratorCheck(target, cords, walls, dooms))
            {
                generators.Add(GetTile(target));
            }

        }
        for (int i = 0; i < generators.Count; i++)
        {
            generators[i].SetToGenerator();
            generators[i].SetGeneratorCount(0);
        }
    }
    public void CreateTile(Cord c)
    {
        Transform t = Instantiate(tile) as Transform;
        t.SetParent(transform);
        t.transform.position = CordToVec(c);
        t.transform.localScale = Vector3.one;
        Tile til = t.GetComponent<Tile>();
        til.Setup(c);
        allTiles.Add(til);
    }
    public void AddWall(Wall g)
    {
        allWalls.Add(g);
        g.gameObject.SetActive(true);
    }
    public void AddPossbileWallToAllWalls(WallCord c)
    {
        for (int i = 0; i < allPossibleWalls.Count; i++)
        {
            if(allPossibleWalls[i].cord == c)
            {
                allPossibleWalls[i].gameObject.SetActive(true);
                allWalls.Add(allPossibleWalls[i]);
            }
        }
    }
    public void CreateWall(WallCord c)
    {
        Transform t = Instantiate(wall) as Transform;
        t.SetParent(transform, false);
        t.transform.position = CordToVec(c);
        Wall til = t.GetComponent<Wall>();
        til.Setup(c);
        allWalls.Add(til);
    }
    public void DestroyWall(Tile t, WallPos pos)
    {
        for (int i = 0; i < allWalls.Count; i++)
        {
            if(t.cord == allWalls[i].cord.cord && pos == allWalls[i].cord.pos)
            {
                allWalls[i].Broken();
                allWalls.RemoveAt(i);
                return;
            }
        }
    }
    public Vector3 CordToVec(Cord c)
    {
        float width = Screen.width;
        float height = Screen.height;
        float per = .14f;//.1455f;
        return new Vector3(width * 0.5f + (c.x - 2) * (height - 2 * height * per) * 0.25f, height * 0.5f + (c.y - 2) * (height - 2 * height * per) * 0.25f);
    }
    public Vector3 CordToVec(WallCord c)
    {
        Vector3 pos = CordToVec(c.cord);
        float height = Screen.height * .17f;
        if(c.pos == WallPos.Up)
        {
            pos.y += height;
            pos.x += height / 2;
        }
        else
        {
            pos.y += height / 2;
            pos.x += height;
        }
        return pos;
    }
    public Tile GetRandomTile()
    {
        return allTiles[UnityEngine.Random.Range(0, allTiles.Count)];
    }
    public Tile GetTile(Cord c)
    {
        for (int i = 0; i < allTiles.Count; i++)
        {
            if(allTiles[i].cord == c)
            {
                return allTiles[i];
            }
        }
        Debug.Log("REturned Null on Gettile");
        return null;
    }
    public WallCord[] AllWallCords()
    {
        WallCord[] w = new WallCord[allWalls.Count];
        for (int i = 0; i < w.Length; i++)
        {
            w[i] = allWalls[i].cord;
        }
        return w;
    }
    public Tile[] GetAllTilesAround(Tile t, bool dontIgnoreWalls)
    {
        Tile[] temp = new Tile[4];
        if (!dontIgnoreWalls)
        {
            temp[0] = GetLeftTile(t);
            temp[1] = GetRightTile(t);
            temp[2] = GetUpTile(t);
            temp[3] = GetDownTile(t);
        }
        else
        {
            if (Pathing.CheckLeft(allCords.ToArray(), AllWallCords(), t.cord))
            {
                temp[0] = GetLeftTile(t);
            }
            if (Pathing.CheckRight(allCords.ToArray(), AllWallCords(), t.cord))
            {
                temp[1] = GetRightTile(t);
            }
            if (Pathing.CheckDown(allCords.ToArray(), AllWallCords(), t.cord))
            {
                temp[2] = GetDownTile(t);
            }
            if (Pathing.CheckUp(allCords.ToArray(), AllWallCords(), t.cord))
            {
                temp[3] = GetUpTile(t);
            }
        }
        return temp;
    }
    public Tile[] GetAll9Tiles(Tile t)
    {
        List<Tile> temp = new List<Tile>();
        int yOffset = -1;
        int xOffset = -1;
        for (int i = 0; i < 8; i++)
        {
            xOffset = -1;
            for (int k = 0; k < 3; k++)
            {
                if (yOffset == 0 && xOffset == 0) continue;
                Tile tile = GetTile(new Cord(t.cord.x + xOffset, t.cord.y + yOffset));
                if (tile != null)
                {
                    temp.Add(tile);
                }
                xOffset++;
            }
            yOffset++;
        }
        return temp.ToArray();
    }
    public Tile GetLeftTile(Tile t)
    {
        Cord c = new Cord(t.cord.x - 1, t.cord.y);
        return FindTile(c);
    }
    public Tile GetRightTile(Tile t)
    {
        Cord c = new Cord(t.cord.x + 1, t.cord.y);
        return FindTile(c);
    }
    public Tile GetUpTile(Tile t)
    {
        Cord c = new Cord(t.cord.x, t.cord.y + 1);
        return FindTile(c);
    }
    public Tile GetDownTile(Tile t)
    {
        Cord c = new Cord(t.cord.x, t.cord.y - 1);
        return FindTile(c);
    }
    public Tile FindTile(Cord c)
    {
        for (int i = 0; i < allTiles.Count; i++)
        {
            if (c.x == allTiles[i].cord.x && c.y == allTiles[i].cord.y)
            {
                return allTiles[i];
            }
        }
        return null;
    }
    public Tile[] GetAllTilesWithLockDoors()
    {
        List<Tile> allTiles = new List<Tile>();
        allTiles.AddRange(GridMaster.instnace.allTiles);
        for (int i = 0; i < LockDoorMaster.instnace.lockedList.Count; i++)
        {
            allTiles.Remove(LockDoorMaster.instnace.lockedList[i]);
        }
        return allTiles.ToArray();
    }
    public Cord[] TilesToCord(Tile[] t)
    {
        List<Cord> c = new List<Cord>();
        for (int i = 0; i < t.Length; i++)
        {
            c.Add(t[i].cord);
        }
        return c.ToArray();
    }
}
