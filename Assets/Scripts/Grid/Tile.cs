﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class Tile : MonoBehaviour {
    public Cord cord;
    public System.Action Pressed;
    public System.Action<Tile> PressedTile;
    public string letter;
    [SerializeField]
    Image hightLight;
    private void Awake()
    {
        GetComponent<Button>().onClick.AddListener(Clicked);
    }
    public void Setup(Cord c)
    {
        cord = c;
        letter = c.CordToLetter();
    }
    [SerializeField]
    Transform generatorParent;
    [SerializeField]
    Transform[] generatorCubes;
    public void SetGeneratorCount(int count)
    {
        for (int i = 0; i < generatorCubes.Length; i++)
        {
            if(i < count)
            {
                generatorCubes[i].gameObject.SetActive(true);
            }
            else
            {

                generatorCubes[i].gameObject.SetActive(false);
            }
        }
    }
    public void SetToGenerator() => generatorParent.gameObject.SetActive(true);
    [SerializeField]
    Transform doomsDayParent;
    [SerializeField]
    Transform[] doomsDayCubes;
    public void SetToDoomsDay() => doomsDayParent.gameObject.SetActive(true);
    int currentDoomCount = 4;
    public void TakeDoomsDayCount()
    {
        currentDoomCount--;
        for (int i = 0; i < doomsDayCubes.Length; i++)
        {
            if(i < currentDoomCount)
            {
                doomsDayCubes[i].gameObject.SetActive(true);

            }
            else
            {
                doomsDayCubes[i].gameObject.SetActive(false);

            }
        }
    }

    internal void ResetTile()
    {
        doomsDayParent.gameObject.SetActive(false);
        generatorParent.gameObject.SetActive(false);
        currentDoomCount = 4;
    }

    public void Highlight(Color col)
    {
        hightLight.gameObject.SetActive(true);
        hightLight.color = col;
    }
    public void ClearHightlight()
    {
        hightLight.gameObject.SetActive(false);
    }
    public void SetPressed(System.Action<Tile> presseed)
    {
        GetComponent<Button>().enabled = presseed != null;
        PressedTile = presseed;
    }
    public void Clicked()
    {
        Pressed?.Invoke();
        PressedTile?.Invoke(this);
    }
}
