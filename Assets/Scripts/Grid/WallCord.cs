﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum WallPos
{
    Up,
    Right
}
[System.Serializable]
public struct WallCord {
    public Cord cord;
    public WallPos pos;
    public WallCord(Cord c, WallPos p)
    {
        cord = c;
        pos = p;
    }
    public static WallCord WallsToCords(Wall w)
    {
        return w.cord;
    }
    public static WallCord[] WallsToCords(Wall[] w)
    {
        WallCord[] walls = new WallCord[w.Length];
        for (int i = 0; i < walls.Length; i++)
        {
            walls[i] = WallsToCords(w[i]);
        }
        return walls;
    }
    public static bool operator ==(WallCord c1, WallCord c2)
    {
        return c1.cord == c2.cord && c1.pos == c2.pos;
    }
    public static bool operator !=(WallCord c1, WallCord c2)
    {
        return !(c1 == c2);
    }
}
