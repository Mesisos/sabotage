﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wall : MonoBehaviour {
    public WallCord cord;
    [SerializeField]
    Sprite broken;
    public void Setup(WallCord c)
    {
        cord = c;
        if(c.pos == WallPos.Up)
        {
            transform.rotation = Quaternion.Euler(0, 0, 90);
        }
    }
    public void Broken()
    {
        GetComponent<Image>().enabled = false;
    }

    internal void ResetWall()
    {
        gameObject.SetActive(false);
    }
}
