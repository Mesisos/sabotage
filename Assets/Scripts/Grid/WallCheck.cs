﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallCheck : MonoBehaviour {
    
    public static WallCord[] ConvertWallToWallCord(Wall[] w)
    {
        List<WallCord> temp = new List<WallCord>();
        for (int i = 0; i < w.Length; i++)
        {
            temp.Add(w[i].cord);
        }
        return temp.ToArray();
    }
    public static WallCord GetWallCord(Cord[] allCords, WallCord[] wallCords, int top, int right)
    {
        WallCord[] possibleSpots = GetAllPossibleWalls(allCords, wallCords, top, right);
        if(possibleSpots.Length < 1)
        {
            Debug.Log("No Possible Walls Found");
            return new WallCord();
        }
        List<WallCord> walls = new List<WallCord>();
        walls.AddRange(wallCords);
        WallCord chosen = possibleSpots[UnityEngine.Random.Range(0, possibleSpots.Length)];
        walls.Add(chosen);
        int infinty = 1000;
        for (int i = 0; i < 1002; i++)
        {
            if(PathingCheck(allCords, walls.ToArray()))
            {
                break;
            }
            infinty--;
            if (infinty < 1)
            {
                Debug.Log("Infinity Break At Walls Pathing");
                break;
            }
            walls.Remove(chosen);
            chosen = possibleSpots[UnityEngine.Random.Range(0, possibleSpots.Length)];
            walls.Add(chosen);
        }
        return chosen;
    }
    static WallCord[] GetAllPossibleWalls(Cord[] cords, WallCord[] wall, int top, int right)
    {
        List<WallCord> walls = new List<WallCord>();
        for (int i = 0; i < cords.Length; i++)
        {
            bool checkUp = false;
            bool checkRight = false;
            for (int k = 0; k < wall.Length; k++)
            {
                if(cords[i] == wall[k].cord)
                {
                    if(wall[k].pos == WallPos.Up)
                    {
                        checkUp = true;
                    }
                    if(wall[k].pos == WallPos.Right)
                    {
                        checkRight = true;
                    }
                }
            }
            if (!checkUp && cords[i].y < top)
            {
                walls.Add(new WallCord(cords[i], WallPos.Up));
            }
            if(!checkRight && cords[i].x < right)
            {
                walls.Add(new WallCord(cords[i], WallPos.Right));
            }
        }
        return walls.ToArray();
    }
    public static bool PathingCheck(Cord[] allCords, WallCord[] allWalls)
    {
        List<Cord> openQueue = new List<Cord>();
        List<Cord> closed = new List<Cord>();
        openQueue.Add(allCords[0]);
        while(openQueue.Count > 0)
        {
            Cord c = openQueue[0];
            openQueue.RemoveAt(0);
            closed.Add(c);
            Cord[] neighbors = GetNeighbors(c, allWalls, allCords);
            for (int i = 0; i < neighbors.Length; i++)
            {
                bool check = false;
                for (int k = 0; k < closed.Count; k++)
                {
                    if(closed[k] == neighbors[i])
                    {
                        check = true;
                        break;
                    }
                }
                for (int k = 0; k < openQueue.Count; k++)
                {
                    if(openQueue[k] == neighbors[i])
                    {
                        check = true;
                        break;
                    }
                }
                if(!check)
                {
                    openQueue.Add(neighbors[i]);
                }
            }
        }
        return closed.Count == allCords.Length;
    }
    static Cord[] GetNeighbors(Cord c, WallCord[] allWalls, Cord[] allCords)
    {
        List<Cord> list = new List<Cord>();
        bool right = false;
        bool left = false;
        bool up = false;
        bool down = false;
        Cord leftCord = new Cord(c.x - 1, c.y);
        Cord downCord = new Cord(c.x, c.y - 1);
        Cord rightCord = new Cord(c.x + 1, c.y);
        Cord upCord = new Cord(c.x, c.y + 1);
        for (int i = 0; i < allWalls.Length; i++)
        {
            if(allWalls[i].cord == c && allWalls[i].pos == WallPos.Right)
            {
                right = true;
            }
            if (allWalls[i].cord == leftCord && allWalls[i].pos == WallPos.Right)
            {
                left = true;
            }
            if (allWalls[i].cord == c && allWalls[i].pos == WallPos.Up)
            {
                up = true;
            }
            if (allWalls[i].cord == downCord && allWalls[i].pos == WallPos.Up)
            {
                down = true;
            }
        }
        for (int i = 0; i < allCords.Length; i++)
        {
            if(!right && allCords[i] == rightCord)
            {
                right = true;
                list.Add(rightCord);
            }
            if(!left && allCords[i] == leftCord)
            {
                left = true;
                list.Add(leftCord);
            }
            if(!up && allCords[i] == upCord)
            {
                up = true;
                list.Add(upCord);
            }
            if(!down && allCords[i] == downCord)
            {
                down = true;
                list.Add(downCord);
            }
        }
        return list.ToArray();
    }
}
