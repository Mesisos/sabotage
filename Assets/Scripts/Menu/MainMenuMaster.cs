﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Doozy;
using Doozy.Engine.SceneManagement;

public class MainMenuMaster : MonoBehaviour {
    SceneLoader l;
    private void Start()
    {
        l = SceneDirector.LoadSceneAsync(1, LoadSceneMode.Single);
        l.AllowSceneActivation = false;
    }

    public void LoadGame()
    {
        l.AllowSceneActivation = true;
        l.ActivateLoadedScene();
        //SceneManager.LoadScene("Main");
    }
}
