﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C4Master : MonoBehaviour
{
    public static C4Master instance;
    private void Awake()
    {
        instance = this;
    }

    public Tile c4Pos;

    public void AddC4(Tile t)
    {
        c4Pos = t;
    }
    public void ClearC4()
    {
        c4Pos = null;
    }
}
