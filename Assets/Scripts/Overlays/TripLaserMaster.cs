﻿using Doozy.Engine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TripLaserMaster : MonoBehaviour
{
    public static TripLaserMaster instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        ScanMaster.instance.EventScan += CheckTrip;
    }
    public List<Tile> tripList = new List<Tile>();
    public void AddTripLaser(Tile t)
    {
        tripList.Add(t);
    }
    void CheckTrip(Tile[] t)
    {
        for (int i = 0; i < t.Length; i++)
        {
            if(tripList.Contains(t[i]))
            {
                popupQueue.Enqueue(t[i]);
                CreatePopup(t[i]);
            }
        }
    }
    Queue<Tile> popupQueue = new Queue<Tile>();
    UIPopup ui;
    void CreatePopup(Tile t)
    {
        popupQueue.Enqueue(t);
        ui = UIPopupManager.GetPopup("Trip");
        ui.Data.SetLabelsTexts("Trip Laser at " + t.letter+". Are you in that room?");
        ui.Data.SetButtonsCallbacks(Confirm, Cancel);
        UIPopupManager.AddToQueue(ui);
    }
    void Confirm()
    {
        Tile t = popupQueue.Dequeue();
        ScanFinder.instance.PlayerHere(t, PlayerMaster.instnace.GetCurrentPlayer().playerID);
        ui.Hide();
    }
    void Cancel()
    {
        Tile t = popupQueue.Dequeue();
        ScanFinder.instance.PlayerNotHere(t, PlayerMaster.instnace.GetCurrentPlayer().playerID);
        ui.Hide();
    }
}
