﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverlayBase : MonoBehaviour {

    protected bool overlayShown;
    public virtual void ClearOverlay()
    {
        overlayShown = false;
    }
    public virtual void Setup()
    {
        PlayerMaster.instnace.ResetAllOverlays();
        overlayShown = true;
    }

    [SerializeField]
    AudioClip sound;
    protected void PlaySound()
    {
        if(sound == null)
        {
            Debug.Log("Sound is Null");
            return;
        }
        MusicPlayer.instance.PlaySound(sound);
    }
}
