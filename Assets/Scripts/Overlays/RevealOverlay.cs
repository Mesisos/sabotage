﻿using Doozy.Engine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RevealOverlay : MonoBehaviour {
    public static RevealOverlay instance;

    private void Awake()
    {
        instance = this;
    }
    [SerializeField]
    GameObject parent;
    [SerializeField]
    RevealButton button;
    List<Tile> currentTiles = new List<Tile>();
    System.Action call;
    [SerializeField]
    TextMeshProUGUI mainText;
    [SerializeField]
    Image revealImage;
    [SerializeField]
    TwoClickButton closeButton;
    [SerializeField]
    UIView view;
    [SerializeField]
    UIView basicView;
    public List<RevealButton> list = new List<RevealButton>();
    public void RevealedTiles(Tile[] tiles, System.Action callBack, string name)
    {

        revealImage.sprite = IconMaster.instnace.ItemNameToSprite(name);
        MusicPlayer.instance.PlaySound(IconMaster.instnace.ItemNameToSound(name));
        mainText.text = name;
        closeButton.gameObject.SetActive(true);
        basicView.Show();
        currentTiles.Clear();
        currentTiles.AddRange(tiles);
        call = callBack;
        for (int i = 0; i < tiles.Length; i++)
        {
            SetupButtons(tiles[i]);
        }
        RevealViewMaster.instnce.ClearButtons();
    }
    void SetupButtons(Tile t)
    {
        t.SetPressed(TilePressed);
        t.Highlight(Color.yellow);
    }
    void CreateButton(Tile tile)

    {
        Transform t = Instantiate(button.transform) as Transform;
        t.SetParent(parent.transform, false);
        list.Add(t.GetComponent<RevealButton>());
        List<string> temp = new List<string>();
        temp.Add(PlayerMaster.instnace.players[0].className);
        temp.Add(PlayerMaster.instnace.players[1].className);
        if (PlayerMaster.instnace.players[0].revealableItem != null)
        {
            temp.Add(PlayerMaster.instnace.players[0].revealableItem);
        }
        if (PlayerMaster.instnace.players[1].revealableItem != null)
        {
            temp.Add(PlayerMaster.instnace.players[1].revealableItem);
        }
        list[list.Count - 1].Setup(tile, PlayerHere, temp.ToArray(), "Revealing on " + tile.letter);
    }
    void Clear()
    {
        RevealButton[] temp = list.ToArray();
        for (int i = 0; i < temp.Length; i++)
        {
            Destroy(temp[i].gameObject);
        }
        list.Clear();
    }
    public void CloseOverlay()
    {
        view.Hide();
        RevealViewMaster.instnce.ClearButtons();
        closeButton.ResetConfirm(null);
        closeButton.gameObject.SetActive(false);
        basicView.Hide();
        for (int i = 0; i < currentTiles.Count; i++)
        {
            if (!somthingHere.Contains(currentTiles[i]))
            {
                ScanFinder.instance.RemoveTile(currentTiles[i]);
            }
            currentTiles[i].ClearHightlight();
            currentTiles[i].SetPressed(null);
        }
        Clear();
        if (call != null)
        {
            call();
        }
    }
    Tile selectedTile;
    public void TilePressed(Tile t)
    {
        view.Show();
        selectedTile = t;
    }
    List<Tile> somthingHere = new List<Tile>();
    public void PlayerHere(int player)
    {
        //for Virus
        if (player == 2)
        {
            switch (PlayerMaster.instnace.players[0].className)
            {
                case "Hacker":
                case "Acrobat":
                    ScanFinder.instance.PlayerHere(selectedTile, PlayerMaster.instnace.players[0].playerID);
                    somthingHere.Add(selectedTile);
                    break;
                default:
                    break;
            }
        }
        else if (player == 3)
        {
            switch (PlayerMaster.instnace.players[1].className)
            {
                case "Hacker":
                case "Acrobat":
                    ScanFinder.instance.PlayerHere(selectedTile, PlayerMaster.instnace.players[1].playerID);
                    somthingHere.Add(selectedTile);
                    break;
                default:
                    break;
            }
        }
        else
        {
            ScanFinder.instance.PlayerHere(selectedTile, PlayerMaster.instnace.players[player].playerID);
            somthingHere.Add(selectedTile);
        }
    }
    public void PlayerHere(Tile t, int player)
    {
        currentTiles.Remove(t);
        //for Virus
        if (player == 2)
        {
            if (PlayerMaster.instnace.players[0].className == "Acrobat")
            {
                ScanFinder.instance.PlayerHere(t, PlayerMaster.instnace.players[0].playerID);
            }
        }
        else if (player == 3)
        {
            if (PlayerMaster.instnace.players[1].className == "Acrobat")
            {
                ScanFinder.instance.PlayerHere(t, PlayerMaster.instnace.players[1].playerID);
            }
        }
        else
        {
            ScanFinder.instance.PlayerHere(t, PlayerMaster.instnace.players[player].playerID);
        }
    }
}
