﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Doozy.Engine.UI;

public class RevealViewMaster : MonoBehaviour
{
    public static RevealViewMaster instnce;

    private void Awake()
    {
        instnce = this;
    }
    public void ClearButtons()
    {
        SetupButtons();
    }
    public void ResetConfirmButtons()
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].isOn = false;
        }
    }
    void SetupButtons()
    {
        popupShown = false;
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].isOn = false;
        }
        buttons[1].gameObject.SetActive(true);
        buttons[0].gameObject.SetActive(true);
        if (!PlayerMaster.instnace.players[0].killed)
        {
            buttons[0].GetComponentInChildren<TextMeshProUGUI>().text = PlayerMaster.instnace.players[0].className;
        }
        else
        {
            buttons[0].gameObject.SetActive(false);
        }
        if (!PlayerMaster.instnace.players[1].killed)
        {
            buttons[1].GetComponentInChildren<TextMeshProUGUI>().text = PlayerMaster.instnace.players[1].className;
        }
        else
        {
            buttons[1].gameObject.SetActive(false);
        }
        if (PlayerMaster.instnace.players[0].revealableItem != null && GameMaster.instance.currentUnlock != GameUnlock.Tier0)
        {
            buttons[2].GetComponentInChildren<TextMeshProUGUI>().text = PlayerMaster.instnace.players[0].revealableItem;
            buttons[2].gameObject.SetActive(true);
        }
        else
        {
            buttons[2].gameObject.SetActive(false);
        }
        if (PlayerMaster.instnace.players[1].revealableItem != null && GameMaster.instance.currentUnlock != GameUnlock.Tier0)
        {
            buttons[3].GetComponentInChildren<TextMeshProUGUI>().text = PlayerMaster.instnace.players[1].revealableItem;
            buttons[3].gameObject.SetActive(true);
        }
        else
        {
            buttons[3].gameObject.SetActive(false);
        }
    }
    [SerializeField]
    Toggle[] buttons;
    [SerializeField]
    AudioClip revealPlayer;
    bool popupShown;
    public void ButtonPressed(int player)
    {
        RevealOverlay.instance.PlayerHere(player);
        buttons[player].gameObject.SetActive(false);
        if(!popupShown)
        {
            popupShown = true;
            UIPopup u = UIPopupManager.GetPopup("Revealed");
            UIPopupManager.AddToQueue(u);
        }
        if (revealPlayer != null)
        {
            MusicPlayer.instance.PlaySound(revealPlayer);
        }
    }
    public void ViewHidden()
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            if(buttons[i].isOn)
            {
                ButtonPressed(i);
            }
        }
        ResetConfirmButtons();
    }
}
