﻿using Doozy.Engine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class FlashBangMaster : OverlayBase
{
    public static FlashBangMaster instnace;

    private void Awake()
    {
        instnace = this;
    }
    [SerializeField]
    TwoClickButton pre;
    [SerializeField]
    Button mainButton;
    public List<TwoClickButton> list = new List<TwoClickButton>();
    [SerializeField]
    UIView stunView;
    [SerializeField]
    UIView playerView;
    public override void Setup()
    {
        base.Setup();
        mainButton.GetComponentInChildren<TextMeshProUGUI>().text = "Cancel";
        CreateButtons();
        stunView.Show();
        playerView.Hide();
        overlayShown = true;
    }
    public void ButtonPressed()
    {
        if (list.Count > 0)
        {
            ClearOverlay();
        }
        else
        {
            Setup();
        }
    }
    void CreateButtons()
    {
        if (firstTarget == null)
        {
            for (int i = 0; i < GridMaster.instnace.allTiles.Count; i++)
            {
                CreateButton(GridMaster.instnace.allTiles[i]);
            }
        }
        else
        {
            CreateButton(firstTarget, "Finish");
            Tile up = GridMaster.instnace.FindTile(new Cord(firstTarget.cord.x, firstTarget.cord.y + 1));
            if (up != null)
            {
                if (Pathing.CheckUp(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), firstTarget.cord))
                {
                    CreateButton(up);
                }
            }
            Tile down = GridMaster.instnace.FindTile(new Cord(firstTarget.cord.x, firstTarget.cord.y - 1));
            if (down != null)
            {
                if (Pathing.CheckDown(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), firstTarget.cord))
                {
                    CreateButton(down);
                }
            }
            Tile left = GridMaster.instnace.FindTile(new Cord(firstTarget.cord.x - 1, firstTarget.cord.y));
            if (left != null)
            {
                if (Pathing.CheckLeft(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), firstTarget.cord))
                {
                    CreateButton(left);
                }
            }
            Tile right = GridMaster.instnace.FindTile(new Cord(firstTarget.cord.x + 1, firstTarget.cord.y));
            if (right != null)
            {
                if (Pathing.CheckRight(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), firstTarget.cord))
                {
                    CreateButton(right);
                }
            }
        }
        for (int i = 0; i < list.Count; i++)
        {
            list[0].pairedList.Add(list[i]);
        }
    }
    void CreateButton(Tile til, string text = "Stun")
    {
        Transform t = Instantiate(pre.transform) as Transform;
        TwoClickButton b = t.GetComponent<TwoClickButton>();
        Vector3 tilePos = til.transform.position;
        float amount = til.GetComponent<RectTransform>().sizeDelta.x / 2;
        tilePos.y += amount;
        tilePos.x += amount;
        t.SetParent(transform, false);
        t.transform.position = tilePos;
        b.Setup(ButtonPressed, text, til);
        list.Add(b);
    }
    public override void ClearOverlay()
    {
        if (overlayShown)
        {
            stunView.Hide();
            playerView.Show();
        }
        base.ClearOverlay();
        ClearButtons();
        mainButton.GetComponentInChildren<TextMeshProUGUI>().text = "Flashbang";
        overlayShown = false;
        firstTarget = null;
    }
    void ClearButtons()
    {
        TwoClickButton[] temp = list.ToArray();
        for (int i = 0; i < temp.Length; i++)
        {
            Destroy(temp[i].gameObject);
        }
        list.Clear();
    }
    Tile firstTarget;
    public void ButtonPressed(Tile t)
    {
        if (firstTarget == null)
        {
            firstTarget = t;
            ClearButtons();
            CreateButtons();
        }
        else
        {
            Debug.Log(t.cord.ToString() + " Stunned by Flashbang");
            bool stun = false;
            for (int i = 0; i < GuardMaster.instance.guard.Length; i++)
            {
                if (GuardMaster.instance.guard[i].tile.cord == t.cord)
                {
                    GuardMaster.instance.guard[i].stunned = true;
                    stun = true;
                }
                if(GuardMaster.instance.guard[i].tile.cord == firstTarget.cord)
                {
                    GuardMaster.instance.guard[i].stunned = true;
                    stun = true;
                }
            }
            if(stun)
            {
                GuardStunned();
            }
            mainButton.gameObject.SetActive(false);
            ClearOverlay();
        }
    }
    void GuardStunned()
    {
        UIPopup u = UIPopupManager.GetPopup("GuardStunned");
        u.Data.SetLabelsTexts(
            "Guard Stunned"
        );
        UIPopupManager.AddToQueue(u);
    }
}
