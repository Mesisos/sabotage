﻿using Doozy.Engine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoneyPotMaster : MonoBehaviour
{
    public static HoneyPotMaster instance;

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        ScanMaster.instance.EventScan += Scan;
    }
    public List<Tile> honeyPotLocations = new List<Tile>();
    public void AddHoneyPot(Tile c)
    {
        honeyPotLocations.Clear();
        honeyPotLocations.Add(c);
    }
    void Scan(Tile[] t)
    {
        for (int i = 0; i < t.Length; i++)
        {
            if (honeyPotLocations.Contains(t[i]))
            {
                CreatePopup(t[i]);
                honeyPotLocations.Remove(t[i]);
                ScanFinder.instance.PlayerHere(t[i], PlayerMaster.instnace.GetCurrentPlayer().playerID);
                break;
            }
        }
    }
    void CreatePopup(Tile t)
    {
        UIPopup pop = UIPopupManager.GetPopup("HoneyPot");
        pop.Data.SetLabelsTexts(new string[]
        {
            "Honey Pot at " + t.letter + " Move " + PlayerMaster.instnace.GetCurrentPlayerName() + " to " + t.letter + " and they gain 1 Unlock",
        });
        UIPopupManager.AddToQueue(pop);
    }
}
