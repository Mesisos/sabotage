﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Doozy.Engine.UI;
using TMPro;
public class FireMaster : MonoBehaviour {
    public static FireMaster instnace;

    private void Awake()
    {
        instnace = this;
    }
    [SerializeField]
    UIView overlay;
    [SerializeField]
    GameObject parent;
    [SerializeField]
    RevealButton button;
    List<Tile> currentTiles = new List<Tile>();
    [SerializeField]
    TextMeshProUGUI mainText;
    [SerializeField]
    TextMeshProUGUI fireText;
    [SerializeField]
    Image fireImge;
    [SerializeField]
    Toggle[] allButtons;
    public TextMeshProUGUI hitText;
    System.Action call;
    public int hits;
    public List<RevealButton> list = new List<RevealButton>();
    public void FireTiles(Tile[] tiles, System.Action callBack, string name, string fireText = "Firing")
    {
        this.fireText.text = fireText;
        fireImge.sprite = IconMaster.instnace.ItemNameToSprite(name);
        MusicPlayer.instance.PlaySound(IconMaster.instnace.ItemNameToSound(name));
        mainText.text = name;
        overlay.Show();
        currentTiles.Clear();
        currentTiles.AddRange(tiles);
        call = callBack;
        for (int i = 0; i < tiles.Length; i++)
        {
            tiles[i].Highlight(Color.red);
        }
        SetupButtons();
    }
    void SetupButtons()
    {
        for (int i = 0; i < allButtons.Length; i++)
        {
            allButtons[i].gameObject.SetActive(true);
        }
        PlayerMaster.PlayerInfo[] p = PlayerMaster.instnace.players;
        if (!p[0].killed)
        {
            allButtons[0].GetComponentInChildren<TextMeshProUGUI>().text = p[0].className;
        }
        else
        {
            allButtons[0].gameObject.SetActive(false);
        }
        if (!p[1].killed)
        {
            allButtons[1].GetComponentInChildren<TextMeshProUGUI>().text = p[1].className;
        }
        else
        {
            allButtons[1].gameObject.SetActive(false);
        }
        if (p[0].shootableItem != null && p[0].shootableItem != "" && GameMaster.instance.currentUnlock != GameUnlock.Tier0)
        {
            allButtons[2].GetComponentInChildren<TextMeshProUGUI>().text = p[0].shootableItem;
        }
        else
        {
            allButtons[2].gameObject.SetActive(false);
        }
        if(p[1].shootableItem != null && p[1].shootableItem != "" && GameMaster.instance.currentUnlock != GameUnlock.Tier0)
        {
            allButtons[3].GetComponentInChildren<TextMeshProUGUI>().text = p[1].shootableItem;
        }
        else
        {
            allButtons[3].gameObject.SetActive(false);
        }

    }
    void CreateButton(Tile tile)

    {
        Transform t = Instantiate(button.transform) as Transform;
        t.SetParent(parent.transform, false);
        list.Add(t.GetComponent<RevealButton>());
        List<string> temp = new List<string>();
        temp.Add(PlayerMaster.instnace.players[0].className);
        temp.Add(PlayerMaster.instnace.players[1].className);
        if (PlayerMaster.instnace.players[0].shootableItem != null)
        {
            temp.Add(PlayerMaster.instnace.players[0].shootableItem);
        }
        if (PlayerMaster.instnace.players[1].shootableItem != null)
        {
            temp.Add(PlayerMaster.instnace.players[1].shootableItem);
        }
        //list[list.Count - 1].Setup(tile, PlayerHere, temp.ToArray(), "Firing on " + tile.letter);
    }
    void Clear()
    {
        RevealButton[] temp = list.ToArray();
        for (int i = 0; i < temp.Length; i++)
        {
            Destroy(temp[i].gameObject);
        }
        list.Clear();
    }
    public void CloseOverlay()
    {
        overlay.Hide();
        for (int i = 0; i < currentTiles.Count; i++)
        {
            ScanFinder.instance.RemoveTile(currentTiles[i]);
            currentTiles[i].ClearHightlight();
        }
        for (int i = 0; i < allButtons.Length; i++)
        {
            if (allButtons[i].isOn)
            {
                if (i < 2)
                {
                    PlayerHere(i);
                }
                else
                {
                    BuildableHit(i);
                }
            }
        }
        for (int i = 0; i < allButtons.Length; i++)
        {
            allButtons[i].isOn = false;
        }
        Clear();
        if (call != null)
        {
            call();
        }
    }
    [SerializeField]
    AudioClip hitSound;
    [SerializeField]
    AudioClip hitSoundFem;
    public void AddHit()
    {
        hits++;
        hitText.text = "Hits: " + hits + "/" + ((GameMaster.instance.currentUnlock == GameUnlock.Tier0 ? 1 : 3) + 2);
        if (hits >= (GameMaster.instance.currentUnlock == GameUnlock.Tier0 ? 1 : 3) + 2)
        {
            GameOver.instance.GameFinished(false);
        }
    }
    public void PlayerHere(int player)
    {
        AddHit();
        if(PlayerMaster.instnace.players[player].className == "Hacker" || PlayerMaster.instnace.players[player].className == "Acrobat")
        {
            MusicPlayer.instance.PlaySound(hitSound);
        }
        else
        {
            MusicPlayer.instance.PlaySound(hitSoundFem);
        }
        ScanFinder.instance.PlayerShot(player);
        PlayerMaster.instnace.PlayerShot(player);
    }
    [SerializeField]
    AudioClip reflector;
    [SerializeField]
    AudioClip hologram;
    public void BuildableHit(int player)
    {
        if(PlayerMaster.instnace.players[player == 2 ? 0 : 1].className == "Juicer")
        {
            GuardMaster.instance.GetCurrentGuard().stunned = true;
            MusicPlayer.instance.PlaySound(reflector);
        }
        if (PlayerMaster.instnace.players[player == 2 ? 0 : 1].className == "Hacker")
        {
            MusicPlayer.instance.PlaySound(hologram);
        }
    }
}
