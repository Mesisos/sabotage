﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Doozy.Engine.UI;

public class MotionMaster : MonoBehaviour
{
    public static MotionMaster instnace;

    private void Awake()
    {
        instnace = this;
    }
    System.Action action;
    Tile[] tiles;
    [SerializeField]
    UIView view;
    public void Setup(System.Action call, Tile[] tiles)
    {
        view.Show();
        action = call;
        this.tiles = tiles;
        for (int i = 0; i < tiles.Length; i++)
        {
            tiles[i].Highlight(Color.cyan);
        }
    }
    public void TwoSpies()
    {
        ScanFinder.instance.MotionTwo(tiles);
        Clear();
    }
    public void OneSpy()
    {
        ScanFinder.instance.MotionOne(tiles);
        Clear();
    }
    public void NoSpies()
    {
        ScanFinder.instance.MotionNone(tiles);
        Clear();
    }
    [SerializeField]
    TwoClickButton[] buttons;
    void ClearButtons()
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            buttons[i].ResetConfirm(null);
        }
    }
    void Clear()
    {
        //Order
        for (int i = 0; i < tiles.Length; i++)
        {
            tiles[i].ClearHightlight();
        }
        ClearButtons();
        view.Hide();
        if (action != null)
        {
            action();
        }
    }
}
