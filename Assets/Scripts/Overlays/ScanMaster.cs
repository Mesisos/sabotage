﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Doozy;
using Doozy.Engine.UI;
using TMPro;
public enum ScanType
{
    Row,
    Colume,
    Quad
}
public class ScanMaster : OverlayBase {
    public static ScanMaster instance;

    private void Awake()
    {
        instance = this;
    }
    [SerializeField]
    Button scanButtons;
    [SerializeField]
    Button mainButton;
    public System.Action<Tile[]> EventScan;
    [SerializeField]
    UIView uIView;
    [SerializeField]
    ScanButton[] preSetButtons;
    [SerializeField]
    GameObject resetButton;
    [SerializeField]
    UIView playerView;
    bool blockDoubleClick;
    public override void Setup()
    {
        base.Setup();
        mainButton.GetComponentInChildren<TextMeshProUGUI>().text = "Cancel";
        uIView.Show();
        blockDoubleClick = false;
        //CreateButtons();
        playerView.Hide();
    }
    public void ButtonPressed()
    {
        Setup();
    }
    public override void ClearOverlay()
    {
        if(overlayShown)
        {
            uIView.Hide();
            playerView.Show();
        }
        base.ClearOverlay();
        resetButton.SetActive(false);
        ClearHightlights();
        ResetAllButtons();
        mainButton.GetComponentInChildren<TextMeshProUGUI>().text = "Scan";
    }
    void CreateButtons()
    {
        string[] rows = new string[]
        {
            "M Row",
            "I Row",
            "E Row",
            "A Row",
        };
        string[] columes = new string[]
        {
            "A Column",
            "B Column",
            "C Column",
            "D Column",
        };
        int rowCount = 0;
        int columeCount = 0;
        for (int i = 0; i < GridMaster.instnace.allTiles.Count; i++)
        {
            Tile t = GridMaster.instnace.allTiles[i];
            if (t.cord.x == 0)
            {
                CreateButton(t, ScanType.Row, rows[rowCount]);
                rowCount++;
            }
            if(t.cord.y == GridMaster.instnace.gridSizeY - 1)
            {
                CreateButton(t, ScanType.Colume, columes[columeCount]);
                columeCount++;
            }
        }
        CreateQuadButtons();
    }
    void CreateQuadButtons()
    {
        CreateButton(GridMaster.instnace.GetTile(new Cord(0, 0)), ScanType.Quad, "Blue");
        CreateButton(GridMaster.instnace.GetTile(new Cord(0, 3)), ScanType.Quad, "Yellow");
        CreateButton(GridMaster.instnace.GetTile(new Cord(3, 3)), ScanType.Quad, "Red");
        CreateButton(GridMaster.instnace.GetTile(new Cord(3, 0)), ScanType.Quad, "Green");
    }
    void CreateButton(Tile pos, ScanType s, string subText)
    {
        Transform t = Instantiate(scanButtons.transform) as Transform;
        t.SetParent(transform, false);
        ScanButton b = t.GetComponent<ScanButton>();
        b.Setup(pos, s, HighlightTiles, ButtonPressed, subText);
    }
    public void ClearAllButtons()
    {
        for (int i = 0; i < preSetButtons.Length; i++)
        {
            preSetButtons[i].Clear();
        }
    }
    public void ResetAllButtonExcept(Tile t)
    {
        for (int i = 0; i < preSetButtons.Length; i++)
        {
            if(preSetButtons[i].tile.cord != t.cord)
            {
                preSetButtons[i].Clear();
            }
        }
    }
    public void ResetAllButtons()
    {
        for (int i = 0; i < preSetButtons.Length; i++)
        {
            preSetButtons[i].Clear();
        }
    }
    public void ResetOverlay()
    {
        ClearHightlights();
        ResetAllButtons();
        resetButton.SetActive(false);
    }
    public void HighlightTiles(Tile t, ScanType type)
    {
        if (blockDoubleClick) return;
        resetButton.SetActive(true);
        ResetAllButtonExcept(t);
        ClearHightlights();
        HideOtherButtons(t, type);
        Tile[] tiles = GetTilesScanned(t, type);
        for (int i = 0; i < tiles.Length; i++)
        {
            tiles[i].Highlight(Color.yellow);
        }
    }
    void HideOtherButtons(Tile t, ScanType type)
    {
        for (int i = 0; i < preSetButtons.Length; i++)
        {
            if(preSetButtons[i].tile == t && preSetButtons[i].type == type)
            {
                preSetButtons[i].gameObject.SetActive(true);
            }
            else
            {
                preSetButtons[i].gameObject.SetActive(false);
            }
        }
    }
    void ClearHightlights()
    {
        for (int i = 0; i < GridMaster.instnace.allTiles.Count; i++)
        {
            GridMaster.instnace.allTiles[i].ClearHightlight();
        }
        for (int i = 0; i < preSetButtons.Length; i++)
        {
            preSetButtons[i].gameObject.SetActive(true);
        }
    }
    public Tile[] GetTilesScanned(Tile t, ScanType type)
    {
        List<Tile> list = new List<Tile>();
        for (int i = 0; i < GridMaster.instnace.allTiles.Count; i++)
        {
            Tile temp = GridMaster.instnace.allTiles[i];
            if (type == ScanType.Colume && t.cord.x == temp.cord.x)
            {
                list.Add(temp);
            }
            if (type == ScanType.Row && t.cord.y == temp.cord.y)
            {
                list.Add(temp);
            }
        }
        if(type == ScanType.Quad)
        {
            if(t.cord == new Cord(0,0))
            {
                list.Add(GridMaster.instnace.GetTile(new Cord(0, 0)));
                list.Add(GridMaster.instnace.GetTile(new Cord(0, 1)));
                list.Add(GridMaster.instnace.GetTile(new Cord(1, 0)));
                list.Add(GridMaster.instnace.GetTile(new Cord(1, 1)));
            }
            if(t.cord == new Cord(0,3))
            {
                list.Add(GridMaster.instnace.GetTile(new Cord(0, 3)));
                list.Add(GridMaster.instnace.GetTile(new Cord(0, 2)));
                list.Add(GridMaster.instnace.GetTile(new Cord(1, 3)));
                list.Add(GridMaster.instnace.GetTile(new Cord(1, 2)));
            }
            if (t.cord == new Cord(3, 3))
            {
                list.Add(GridMaster.instnace.GetTile(new Cord(3, 3)));
                list.Add(GridMaster.instnace.GetTile(new Cord(3, 2)));
                list.Add(GridMaster.instnace.GetTile(new Cord(2, 3)));
                list.Add(GridMaster.instnace.GetTile(new Cord(2, 2)));
            }
            if (t.cord == new Cord(3, 0))
            {
                list.Add(GridMaster.instnace.GetTile(new Cord(3, 0)));
                list.Add(GridMaster.instnace.GetTile(new Cord(3, 1)));
                list.Add(GridMaster.instnace.GetTile(new Cord(2, 0)));
                list.Add(GridMaster.instnace.GetTile(new Cord(2, 1)));
            }
        }
        return list.ToArray();
    }
    public void ButtonPressed(ScanButton s)
    {
        if (blockDoubleClick) return;
        blockDoubleClick = true;
        ClearOverlay();
        ScanFinder.instance.Scanned(GetTilesScanned(s.tile, s.type));
        Tile[] t = GetTilesScanned(s.tile, s.type);
        GeneratorMaster.instance.Scan(t);
        int guardsSpotted = 0;
        for (int i = 0; i < GuardMaster.instance.guard.Length; i++)
        {
            guardsSpotted += GuardMaster.instance.guard[i].CheckRevealable(t) ? 1 : 0;
        }
        if(EventScan != null)
        {
            EventScan(t);
        }
        if(guardsSpotted > 0)
        {
            UIPopup u = UIPopupManager.GetPopup("GuardFound");
            u.Data.SetLabelsTexts(
                guardsSpotted + " Guard" + (guardsSpotted > 1 ? "s" : "") + " Found." + 
                " Both Spies gain " + guardsSpotted + " Unlock Cube"+ (guardsSpotted > 1 ? "s" : "")
            );
            UIPopupManager.AddToQueue(u);
            //guardsSpottedText.text = guardsSpotted + " Guard" + (guardsSpotted > 1 ? "s" : "") + " Spotted!";
            //guardsSpottedText.gameObject.SetActive(false);
            //guardsSpottedText.gameObject.SetActive(true);
        }
    }
}
