﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy;
using Doozy.Engine.UI;

public class FreezeMasterOverlay : MonoBehaviour
{
    public static FreezeMasterOverlay instance;

    private void Awake()
    {
        instance = this;
    }

    public void Setup(Tile[] t)
    {
        UIPopup u = UIPopupManager.GetPopup("FreezeRay");
        string tileString = "";
        for (int i = 0; i < t.Length; i++)
        {
            tileString += " " + t[i].letter;
        }
        u.Data.SetLabelsTexts(new string[]
        {
                "Freeze Ray on rooms:" + tileString+". Players cannot leave these rooms this turn.",
        });
        UIPopupManager.AddToQueue(u);
    }

}
