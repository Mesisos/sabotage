﻿using Doozy.Engine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TearGasMaster : MonoBehaviour
{
    public static TearGasMaster instance;

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        PhaseMaster.instance.EventNewRound += ClearGas;
    }
    void ClearGas()
    {
        tearGasedTiles.Clear();
        tearGasImg.gameObject.SetActive(false);
    }
    public List<Tile> tearGasedTiles = new List<Tile>();
    [SerializeField]
    Transform tearGasImg;
    public void AddTearGasToTile(Tile t)
    {
        tearGasedTiles.Add(t);
        UIPopup ui = UIPopupManager.GetPopup("TearGas");
        ui.Data.SetLabelsTexts("Tear Gas at " + t.letter);
        UIPopupManager.AddToQueue(ui);
        tearGasImg.position = t.transform.position;
        tearGasImg.gameObject.SetActive(true);
    }
}
