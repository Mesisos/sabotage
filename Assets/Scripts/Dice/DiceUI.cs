﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Doozy;
using Doozy.Engine.UI;

public class DiceUI : OverlayBase {


    [SerializeField]
    Image[] diceImages;

    public override void Setup()
    {
        base.Setup();
        GetComponent<UIView>().Show();
        for (int i = 0; i < diceImages.Length; i++)
        {
            diceImages[i].sprite =IconMaster.instnace.dice[DiceMaster.instnace.diceValues[i] - 1];
        }
    }
    public override void ClearOverlay()
    {
        base.ClearOverlay();
        GetComponent<UIView>().Hide();
    }
}
