﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class DiceMaster : PhaseBase {
    public static DiceMaster instnace;

    private void Awake()
    {
        instnace = this;
        diceValues = new int[4];
    }
    public int[] diceValues;
    public System.Action DiceRolled;
    [SerializeField]
    DiceUI diceUI;
    public override void StartPhase()
    {
        base.StartPhase();
        RollDice();
    }
    public override void EndPhase()
    {
        if (GameMaster.instance.currentPhase != Phase.Dice) return;
        diceUI.ClearOverlay();
        base.EndPhase();
    }
    [ContextMenu("Roll Dice")]
    public void RollDice()
    {
        for (int i = 0; i < diceValues.Length; i++)
        {
            diceValues[i] = UnityEngine.Random.Range(1, 7);
        }
        diceValues = diceValues.OrderBy(i => i).ToArray();
        if(DiceRolled != null)
        {
            DiceRolled();
        }
        diceUI.Setup();
    }
}
