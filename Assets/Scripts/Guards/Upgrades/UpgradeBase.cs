﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum UpgradeType
{
    Reveal,
    Shooting,
    Other,
    Passive,
}
public class UpgradeBase : MonoBehaviour {
    public string upgradeName;
    public UpgradeType type;
    public System.Action call;
    protected Guard gaurd;
    public int[] diceCost;
    public bool[] dicePaid;
    public int useCost;
    public bool unlocked;
    public virtual void Upgrade(Guard g)
    {
        for (int i = 0; i < diceCost.Length; i++)
        {
            if(!dicePaid[i])
            {
                if(g.UseDice(diceCost[i]))
                {
                    dicePaid[i] = true;
                    break;
                }
            }
        }
        Debug.Log("Unlocked Part of " + upgradeName);
        for (int i = 0; i < dicePaid.Length; i++)
        {
            if(!dicePaid[i])
            {
                return;
            }
        }
        unlocked = true;
    }
    public virtual bool CheckUpgradeDice(int[] dice)
    {
        if(diceCost.Length != dicePaid.Length)
        {
            dicePaid = new bool[diceCost.Length];
        }
        for (int i = 0; i < dice.Length; i++)
        {
            for (int k = 0; k < diceCost.Length; k++)
            {
                if (dicePaid[k]) continue;
                if (dice[i] == diceCost[k])
                {
                    return true;
                }
            }
        }
        return false;
    }
    public virtual bool CheckUseDice(int[] dice)
    {
        Debug.LogError("Failed to check dice on " + upgradeName);
        return false;
    }
    public virtual void UseAbility(Guard g, System.Action finishCall)
    {
        GuardMaster.activeGuardActions++;
        Debug.Log("!Action Started!: " + name);
        call = finishCall;
        gaurd = g;
    }
    protected virtual void Finish()
    {
        Debug.Log("!Action Ended!: " + name);
        GuardMaster.activeGuardActions--;
        if(call != null)
        {
            call();
        }
    }
}
