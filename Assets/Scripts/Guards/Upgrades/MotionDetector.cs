﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionDetector : UpgradeBase {
    public override void UseAbility(Guard g, Action finishCall)
    {
        base.UseAbility(g, finishCall);
        List<Tile[]> tiles = new List<Tile[]>();
        List<int> scores = new List<int>();
        tiles.Add(ScanMaster.instance.GetTilesScanned(GridMaster.instnace.GetTile(new Cord(0, 3)), ScanType.Colume));
        tiles.Add(ScanMaster.instance.GetTilesScanned(GridMaster.instnace.GetTile(new Cord(1, 3)), ScanType.Colume));
        tiles.Add(ScanMaster.instance.GetTilesScanned(GridMaster.instnace.GetTile(new Cord(2, 3)), ScanType.Colume));
        tiles.Add(ScanMaster.instance.GetTilesScanned(GridMaster.instnace.GetTile(new Cord(3, 3)), ScanType.Colume));
        tiles.Add(ScanMaster.instance.GetTilesScanned(GridMaster.instnace.GetTile(new Cord(0, 0)), ScanType.Row));
        tiles.Add(ScanMaster.instance.GetTilesScanned(GridMaster.instnace.GetTile(new Cord(0, 1)), ScanType.Row));
        tiles.Add(ScanMaster.instance.GetTilesScanned(GridMaster.instnace.GetTile(new Cord(0, 2)), ScanType.Row));
        tiles.Add(ScanMaster.instance.GetTilesScanned(GridMaster.instnace.GetTile(new Cord(0, 3)), ScanType.Row));
        tiles.Add(ScanMaster.instance.GetTilesScanned(GridMaster.instnace.GetTile(new Cord(0, 0)), ScanType.Quad));
        tiles.Add(ScanMaster.instance.GetTilesScanned(GridMaster.instnace.GetTile(new Cord(0, 3)), ScanType.Quad));
        tiles.Add(ScanMaster.instance.GetTilesScanned(GridMaster.instnace.GetTile(new Cord(3, 3)), ScanType.Quad));
        tiles.Add(ScanMaster.instance.GetTilesScanned(GridMaster.instnace.GetTile(new Cord(3, 0)), ScanType.Quad));
        int pos = 0;
        int highScore = int.MinValue;
        int second = int.MinValue;
        for (int i = 0; i < tiles.Count; i++)
        {
            int tempScore = 0;
            for (int k = 0; k < tiles[i].Length; k++)
            {
                tempScore += ScanFinder.instance.GetTileScore(tiles[i][k]);
            }
            scores.Add(tempScore);
            if(tempScore > highScore)
            {
                highScore = tempScore;
            }
            if(tempScore < highScore && tempScore > second)
            {
                pos = i;
                second = tempScore;
            }
        }
        g.UseDice(3);
        MotionMaster.instnace.Setup(Finish, tiles[pos]);
        Debug.Log("Motion Used");
    }
    public override bool CheckUpgradeDice(int[] dice)
    {
        for (int i = 0; i < dice.Length; i++)
        {
            if(dice[i] == 3)
            {
                return true;
            }
        }
        return false;
    }
    public override bool CheckUseDice(int[] dice)
    {
        for (int i = 0; i < dice.Length; i++)
        {
            if (dice[i] == 4)
            {
                return true;
            }
        }
        return false;
    }
}
