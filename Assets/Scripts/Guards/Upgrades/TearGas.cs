﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TearGas : UpgradeBase {
    public override void UseAbility(Guard g, Action finishCall)
    {
        base.UseAbility(g, finishCall);
        List<Tile> doomsDays = new List<Tile>();
        for (int i = 0; i < GridMaster.instnace.doomsDays.Count; i++)
        {
            if(GridMaster.instnace.doomsDays[i].cord.x == g.tile.cord.x || GridMaster.instnace.doomsDays[i].cord.y == g.tile.cord.y)
            {
                doomsDays.Add(GridMaster.instnace.doomsDays[i]);
            }
        }
        for (int i = 0; i < GridMaster.instnace.generators.Count; i++)
        {
            if (GridMaster.instnace.generators[i].cord.x == g.tile.cord.x || GridMaster.instnace.generators[i].cord.y == g.tile.cord.y)
            {
                doomsDays.Add(GridMaster.instnace.generators[i]);
            }
        }
        if(doomsDays.Count < 1)
        {
            Finish();
            return;
        }
        g.UseDiceSum(7);
        TearGasMaster.instance.AddTearGasToTile(ScanFinder.instance.GetBestTileChance(doomsDays.ToArray()));
        Debug.Log("Tear Gas");

    }
    public override bool CheckUseDice(int[] dice)
    {
        for (int i = 0; i < dice.Length; i++)
        {
            for (int k = 0; k < dice.Length; k++)
            {
                if (i == k) continue;
                if (dice[i] + dice[k] == 7)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
