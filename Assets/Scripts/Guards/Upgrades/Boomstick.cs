﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boomstick : UpgradeBase {
    public override void UseAbility(Guard g, Action finishCall)
    {
        base.UseAbility(g, finishCall);
        Cord[] temp = new Cord[4];
        temp[0] = new Cord(g.tile.cord.x - 1, g.tile.cord.y);
        temp[1] = new Cord(g.tile.cord.x + 1, g.tile.cord.y);
        temp[2] = new Cord(g.tile.cord.x, g.tile.cord.y + 1);
        temp[3] = new Cord(g.tile.cord.x, g.tile.cord.y - 1);
        List<Tile> list = new List<Tile>();
        for (int i = 0; i < temp.Length; i++)
        {
            Tile t = GridMaster.instnace.FindTile(temp[i]);
            if (t != null)
            {
                list.Add(t);
            }
        }
        for (int i = 0; i < list.Count; i++)
        {
            WallKiller.instnace.KillWall(g.tile, list[i]);
        }
        FireMaster.instnace.FireTiles(list.ToArray(), Finish, upgradeName);
    }
    public override bool CheckUseDice(int[] dice)
    {
        for (int i = 0; i < dice.Length; i++)
        {
            for (int k = 0; k < dice.Length; k++)
            {
                if (i == k) continue;
                if (dice[i] + dice[k] == 11)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
