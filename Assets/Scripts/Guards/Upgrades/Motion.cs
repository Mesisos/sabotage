﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motion : UpgradeBase {
    public override void UseAbility(Guard g, Action finishCall)
    {
        base.UseAbility(g, finishCall);
        Debug.Log("Motion");
        TripLaserMaster.instance.AddTripLaser(g.tile);
        g.UseDice(5);
        Finish();
    }
    public override bool CheckUseDice(int[] dice)
    {
        if (TripLaserMaster.instance.tripList.Count > 1)
        {
            return false;
        }
        for (int i = 0; i < dice.Length; i++)
        {
            if(dice[i] == 5)
            {
                return true;
            }
        }
        return false;
    }
}
