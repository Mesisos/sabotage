﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sniper : UpgradeBase {
    public override void UseAbility(Guard g, Action finishCall)
    {
        base.UseAbility(g, finishCall);
        Tile best = null;
        List<Cord> temp = new List<Cord>();
        bool left = false;
        bool right = false;
        bool up = false;
        bool down = false;
        for (int i = 0; i < 3; i++)
        {
            if (!left && Pathing.CheckLeft(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), new Cord(g.tile.cord.x - (1 + i), g.tile.cord.y)))
            {
                temp.Add(new Cord(g.tile.cord.x - (1 + i), g.tile.cord.y));
            }
            else
            {
                left = true;
            }
            if (!right && Pathing.CheckRight(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), new Cord(g.tile.cord.x + (1 + i), g.tile.cord.y)))
            {
                temp.Add(new Cord(g.tile.cord.x + (1 + i), g.tile.cord.y));
            }
            else
            {
                right = true;
            }
            if (!up && Pathing.CheckUp(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), new Cord(g.tile.cord.x, g.tile.cord.y + (i + 1))))
            {
                temp.Add(new Cord(g.tile.cord.x, g.tile.cord.y + (i + 1)));
            }
            else
            {
                up = true;
            }
            if (!down && Pathing.CheckDown(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), new Cord(g.tile.cord.x, g.tile.cord.y - (i + 1))))
            {
                temp.Add(new Cord(g.tile.cord.x, g.tile.cord.y - (i + 1)));
            }
            else
            {
                down = true;
            }
        }
        ScanTile[] scanTiles = ScanFinder.instance.GetCombined();
        List<ScanTile> list = new List<ScanTile>();
        for (int i = 0; i < scanTiles.Length; i++)
        {
            for (int k = 0; k < temp.Count; k++)
            {
                if (scanTiles[i].tile.cord == temp[k])
                {
                    list.Add(scanTiles[i]);
                }
            }
        }
        float chance = float.MinValue;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].chance > chance)
            {
                best = list[i].tile;
                chance = list[i].chance;
            }
        }
        if (best == null)
        {
            Finish();
            return;
        }
        if (!g.CheckDiceSum(9))
        {
            Finish();
            return;
        }
        g.currentActions -= useCost;
        g.UseDiceSum(9);
        Debug.Log("Fire on " + best.cord.ToString());
        FireMaster.instnace.FireTiles(new Tile[] { best }, Finish, upgradeName);
    }
    public override bool CheckUseDice(int[] dice)
    {
        for (int i = 0; i < dice.Length; i++)
        {
            for (int k = 0; k < dice.Length; k++)
            {
                if (i == k) continue;
                if (dice[i] + dice[k] == 9)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
