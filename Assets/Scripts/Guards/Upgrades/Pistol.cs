﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : UpgradeBase {
    public override void UseAbility(Guard g, Action finishCall)
    {
        base.UseAbility(g, finishCall);
        Tile best = null;
        Cord left = new Cord(g.tile.cord.x - 1, g.tile.cord.y);
        Cord right = new Cord(g.tile.cord.x + 1, g.tile.cord.y);
        Cord up = new Cord(g.tile.cord.x, g.tile.cord.y + 1);
        Cord down = new Cord(g.tile.cord.x, g.tile.cord.y - 1);
        List<Cord> temp = new List<Cord>();
        if (Pathing.CheckLeft(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), g.tile.cord))
        {
            temp.Add(left);
        }
        if (Pathing.CheckRight(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), g.tile.cord))
        {
            temp.Add(right);
        }
        if (Pathing.CheckUp(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), g.tile.cord))
        {
            temp.Add(up);
        }
        if (Pathing.CheckDown(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), g.tile.cord))
        {
            temp.Add(down);
        }
        ScanTile[] scanTiles = ScanFinder.instance.GetCombined();
        List<ScanTile> list = new List<ScanTile>();
        for (int i = 0; i < scanTiles.Length; i++)
        {
            for (int k = 0; k < temp.Count; k++)
            {
                if (scanTiles[i].tile.cord == temp[k])
                {
                    list.Add(scanTiles[i]);
                }
            }
        }
        float chance = float.MinValue;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].chance > chance)
            {
                best = list[i].tile;
                chance = list[i].chance;
            }
        }
        if (best == null)
        {
            Finish();
            return;
        }
        if (!g.CheckDice((x, y) => (x >= y ? 1 : 0), 5))
        {
            if (chance < 25 || GuardMaster.instance.modifiers < 1)
            {
                Finish();
                return;
            }
            if (!g.ModifyDice((x, y) => (x >= y ? 1 : 0), 5))
            {

                Finish();
                return;
            }
        }
        if (chance < 1)
        {
            Finish();
            return;
        }
        g.currentActions--;
        g.UseDice((x, y) => (x >= y ? 1 : 0), 5);
        Debug.Log("Fire on " + best.cord.ToString());
        FireMaster.instnace.FireTiles(new Tile[] { best }, Finish, upgradeName);
    }
    public override bool CheckUseDice(int[] dice)
    {
        for (int i = 0; i < dice.Length; i++)
        {
            if(dice[i] == 5 || dice[i] == 6)
            {
                return true;
            }
        }
        return false;
    }
}
