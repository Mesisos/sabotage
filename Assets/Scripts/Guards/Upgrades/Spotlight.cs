﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spotlight : UpgradeBase {
    public override void UseAbility(Guard g, Action finishCall)
    {
        base.UseAbility(g, finishCall);
        List<Tile[]> list = new List<Tile[]>();
        ScanTile[] allScanTile = ScanFinder.instance.GetCombined();
        List<Tile> chosen = new List<Tile>();
        float highScore = 0;
        for (int i = 0; i < allScanTile.Length; i++)
        {
            Tile[] temp = GridMaster.instnace.GetAll9Tiles(allScanTile[i].tile);
            float score = allScanTile[i].chance;
            for (int kl = 0; kl < temp.Length; kl++)
            {
                score += ScanFinder.instance.GetTileChance(temp[kl]);
            }
            if(score > highScore)
            {
                highScore = score;
                chosen.Clear();
                chosen.AddRange(temp);
                chosen.Add(allScanTile[i].tile);
            }
        }
        g.UseDiceSum(11);
        Debug.Log("Spotlight!!!");
        RevealOverlay.instance.RevealedTiles(chosen.ToArray(), Finish, "Spotlight");
    }
    public override bool CheckUseDice(int[] dice)
    {
        for (int i = 0; i < dice.Length; i++)
        {
            for (int k = 0; k < dice.Length; k++)
            {
                if (i == k) continue;
                if (dice[i] + dice[k] == 11)
                {
                    return true;
                }
            }
        }
        return false;
    }

}
