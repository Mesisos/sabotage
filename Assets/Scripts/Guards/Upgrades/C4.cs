﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C4 : UpgradeBase {
    int useChance = 50;
    public override void UseAbility(Guard g, Action finishCall)
    {
        base.UseAbility(g, finishCall);
        if(C4Master.instance.c4Pos != null)
        {
            int chance = 0;
            List<Tile> hitTiles = new List<Tile>();
            hitTiles.Add(C4Master.instance.c4Pos);
            Tile center = C4Master.instance.c4Pos;
            hitTiles.Add(GridMaster.instnace.GetTile(new Cord(center.cord.x, center.cord.y + 1)));
            hitTiles.Add(GridMaster.instnace.GetTile(new Cord(center.cord.x, center.cord.y - 1)));
            hitTiles.Add(GridMaster.instnace.GetTile(new Cord(center.cord.x + 1, center.cord.y)));
            hitTiles.Add(GridMaster.instnace.GetTile(new Cord(center.cord.x - 1, center.cord.y)));
            hitTiles.RemoveAll(x => x == null);
            for (int i = 0; i < hitTiles.Count; i++)
            {
                if (hitTiles[i] == null) continue;

                chance += ScanFinder.instance.GetTileScore(hitTiles[i]);
            }
            if(chance < useChance)
            {
                Finish();
                return;
            }
            Debug.Log("C4 Boom!");
            FireMaster.instnace.FireTiles(hitTiles.ToArray(), Finish, "Dynamite");
        }
        else
        {
            Debug.Log("Place C4 at " + g.tile.cord.ToString());
            C4Master.instance.AddC4(g.tile);
            g.UseDiceSum(11);
            Finish();
        }
    }
    public override bool CheckUseDice(int[] dice)
    {
        if(C4Master.instance.c4Pos != null)
        {
            return true;
        }
        for (int i = 0; i < dice.Length; i++)
        {
            for (int k = 0; k < dice.Length; k++)
            {
                if (i == k) continue;
                if (dice[i] + dice[k] == 11)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
