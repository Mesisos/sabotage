﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Strobe : UpgradeBase {
    public override void UseAbility(Guard g, Action finishCall)
    {
        base.UseAbility(g, finishCall);
        List<Tile> temp = new List<Tile>();
        bool left = false;
        bool right = false;
        bool up = false;
        bool down = false;
        for (int i = 0; i < 3; i++)
        {
            if (!left && Pathing.CheckLeft(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), new Cord(g.tile.cord.x - (i), g.tile.cord.y)))
            {
                temp.Add(GridMaster.instnace.FindTile(new Cord(g.tile.cord.x - (1 + i), g.tile.cord.y)));
            }
            else
            {
                left = true;
            }
            if (!right && Pathing.CheckRight(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), new Cord(g.tile.cord.x + (i), g.tile.cord.y)))
            {
                temp.Add(GridMaster.instnace.FindTile(new Cord(g.tile.cord.x + (1 + i), g.tile.cord.y)));
            }
            else
            {
                right = true;
            }
            if (!up && Pathing.CheckUp(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), new Cord(g.tile.cord.x, g.tile.cord.y + (i))))
            {
                temp.Add(GridMaster.instnace.FindTile(new Cord(g.tile.cord.x, g.tile.cord.y + (i + 1))));
            }
            else
            {
                up = true;
            }
            if (!down && Pathing.CheckDown(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), new Cord(g.tile.cord.x, g.tile.cord.y - (i))))
            {
                temp.Add(GridMaster.instnace.FindTile(new Cord(g.tile.cord.x, g.tile.cord.y - (i + 1))));
            }
            else
            {
                down = true;
            }
        }
        if (!g.CheckDiceSum(7))
        {
            Finish();
            return;
        }
        g.currentActions -= useCost;
        g.UseDiceSum(7);
        Debug.Log("Reveal with strobe");
        RevealOverlay.instance.RevealedTiles(temp.ToArray(), Finish, upgradeName);
    }
    public override bool CheckUseDice(int[] dice)
    {
        for (int i = 0; i < dice.Length; i++)
        {
            for (int k = 0; k < dice.Length; k++)
            {
                if (i == k) continue;
                if (dice[i] + dice[k] == 7)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
