﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doppler : UpgradeBase {
    public override void UseAbility(Guard g, Action finishCall)
    {
        base.UseAbility(g, finishCall);
        Debug.Log("Doppler");
        g.UseDiceSum(11);
        RevealOverlay.instance.RevealedTiles(GridMaster.instnace.allTiles.ToArray(), Finish, "Doppler");
    }
    public override bool CheckUseDice(int[] dice)
    {
        for (int i = 0; i < dice.Length; i++)
        {
            for (int k = 0; k < dice.Length; k++)
            {
                if (i == k) continue;
                if (dice[i] + dice[k] == 11)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
