﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineBase : UpgradeBase
{
    public Tile[] GetLine(Tile g, int distance, bool ignoreWalls)
    {
        List<Cord> temp = new List<Cord>();
        bool left = false;
        bool right = false;
        bool up = false;
        bool down = false;
        for (int i = 0; i < distance; i++)
        {
            Cord leftCord = new Cord(g.cord.x - (i), g.cord.y);
            if (!left && GridMaster.instnace.GetTile(leftCord) != null && (ignoreWalls || Pathing.CheckLeft(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), leftCord)))
            {
                temp.Add(new Cord(g.cord.x - (1 + i), g.cord.y));
            }
            else
            {
                left = true;
            }
            Cord rightCord = new Cord(g.cord.x + (i), g.cord.y);
            if (!right && GridMaster.instnace.GetTile(rightCord) != null && (ignoreWalls || Pathing.CheckRight(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), rightCord)))
            {
                temp.Add(new Cord(g.cord.x + (1 + i), g.cord.y));
            }
            else
            {
                right = true;
            }
            Cord upCord = new Cord(g.cord.x, g.cord.y + (i));
            if (!up && GridMaster.instnace.GetTile(upCord) != null && (ignoreWalls || Pathing.CheckUp(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), upCord)))
            {
                temp.Add(new Cord(g.cord.x, g.cord.y + (i + 1)));
            }
            else
            {
                up = true;
            }
            Cord downCord = new Cord(g.cord.x, g.cord.y - (i));
            if (!down && GridMaster.instnace.GetTile(downCord) != null &&(ignoreWalls || Pathing.CheckDown(GridMaster.instnace.allCords.ToArray(), GridMaster.instnace.AllWallCords(), downCord)))
            {
                temp.Add(new Cord(g.cord.x, g.cord.y - (i + 1)));
            }
            else
            {
                down = true;
            }
        }
        ScanTile[] scanTiles = ScanFinder.instance.GetCombined();
        List<ScanTile> list = new List<ScanTile>();
        for (int i = 0; i < temp.Count; i++)
        {
            bool check = false;
            for (int k = 0; k < scanTiles.Length; k++)
            {
                if (scanTiles[k].tile.cord == temp[i])
                {
                    list.Add(scanTiles[k]);
                    check = true;
                    break;
                }
            }
            if (!check)
            {
                list.Add(new ScanTile(0, GridMaster.instnace.GetTile(temp[i])));
            }
        }
        List<ScanTile> leftList = new List<ScanTile>();
        List<ScanTile> rightList = new List<ScanTile>();
        List<ScanTile> upList = new List<ScanTile>();
        List<ScanTile> downList = new List<ScanTile>();
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].tile.cord.x < g.cord.x && list[i].tile.cord.y == g.cord.y)
            {
                leftList.Add(list[i]);
                continue;
            }
            if (list[i].tile.cord.x > g.cord.x && list[i].tile.cord.y == g.cord.y)
            {
                rightList.Add(list[i]);
                continue;
            }
            if (list[i].tile.cord.x == g.cord.x && list[i].tile.cord.y < g.cord.y)
            {
                downList.Add(list[i]);
                continue;
            }
            if (list[i].tile.cord.x == g.cord.x && list[i].tile.cord.y > g.cord.y)
            {
                upList.Add(list[i]);
                continue;
            }
        }
        float chance = float.MinValue;
        ScanTile[] chosen = leftList.ToArray();
        float leftChance = 0;
        for (int i = 0; i < leftList.Count; i++)
        {
            leftChance += leftList[i].chance;
        }
        chance = leftChance;
        float rightChance = 0;
        for (int i = 0; i < rightList.Count; i++)
        {
            rightChance += rightList[i].chance;
        }
        if (rightChance > leftChance)
        {
            chance = rightChance;
            chosen = rightList.ToArray();
        }
        float upChance = 0;
        for (int i = 0; i < upList.Count; i++)
        {
            upChance += upList[i].chance;
        }
        if (upChance > chance)
        {
            chance = upChance;
            chosen = upList.ToArray();
        }
        float downChance = 0;
        for (int i = 0; i < downList.Count; i++)
        {
            downChance += downList[i].chance;
        }
        if (downChance > chance)
        {
            chosen = downList.ToArray();
        }
        if (chosen == null)
        {
            return null;
        }
        Tile[] chosenTiles = new Tile[chosen.Length];
        for (int i = 0; i < chosen.Length; i++)
        {
            chosenTiles[i] = chosen[i].tile;
        }
        return chosenTiles;
    }
}
