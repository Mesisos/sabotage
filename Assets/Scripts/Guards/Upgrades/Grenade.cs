﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grenade : UpgradeBase {
    public override void UseAbility(Guard g, Action finishCall)
    {
        base.UseAbility(g, finishCall);
        List<Cord> temp = new List<Cord>();
        for (int i = 0; i < 2; i++)
        {
            temp.Add(new Cord(g.tile.cord.x - (1 + i), g.tile.cord.y));
            temp.Add(new Cord(g.tile.cord.x + (1 + i), g.tile.cord.y));
            temp.Add(new Cord(g.tile.cord.x, g.tile.cord.y + (i + 1)));
            temp.Add(new Cord(g.tile.cord.x, g.tile.cord.y - (i + 1)));
        }
        ScanTile[] scanTiles = ScanFinder.instance.GetCombined();
        List<ScanTile> list = new List<ScanTile>();
        for (int i = 0; i < temp.Count; i++)
        {
            bool check = false;
            for (int k = 0; k < scanTiles.Length; k++)
            {
                if (scanTiles[k].tile.cord == temp[i])
                {
                    list.Add(scanTiles[k]);
                    check = true;
                    break;
                }
            }
            if (!check)
            {
                if (GridMaster.instnace.GetTile(temp[i]) != null)
                {
                    list.Add(new ScanTile(0, GridMaster.instnace.GetTile(temp[i])));
                }
            }
        }
        List<ScanTile> leftList = new List<ScanTile>();
        List<ScanTile> rightList = new List<ScanTile>();
        List<ScanTile> upList = new List<ScanTile>();
        List<ScanTile> downList = new List<ScanTile>();
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].tile.cord.x < g.tile.cord.x && list[i].tile.cord.y == g.tile.cord.y)
            {
                leftList.Add(list[i]);
                continue;
            }
            if (list[i].tile.cord.x > g.tile.cord.x && list[i].tile.cord.y == g.tile.cord.y)
            {
                rightList.Add(list[i]);
                continue;
            }
            if (list[i].tile.cord.x == g.tile.cord.x && list[i].tile.cord.y < g.tile.cord.y)
            {
                downList.Add(list[i]);
                continue;
            }
            if (list[i].tile.cord.x == g.tile.cord.x && list[i].tile.cord.y > g.tile.cord.y)
            {
                upList.Add(list[i]);
                continue;
            }
        }
        float chance = float.MinValue;
        ScanTile[] chosen = leftList.ToArray();
        float leftChance = 0;
        for (int i = 0; i < leftList.Count; i++)
        {
            leftChance += leftList[i].chance;
        }
        chance = leftChance;
        float rightChance = 0;
        for (int i = 0; i < rightList.Count; i++)
        {
            rightChance += rightList[i].chance;
        }
        if (rightChance > leftChance)
        {
            chance = rightChance;
            chosen = rightList.ToArray();
        }
        float upChance = 0;
        for (int i = 0; i < upList.Count; i++)
        {
            upChance += upList[i].chance;
        }
        if (upChance > chance)
        {
            chance = upChance;
            chosen = upList.ToArray();
        }
        float downChance = 0;
        for (int i = 0; i < downList.Count; i++)
        {
            downChance += downList[i].chance;
        }
        if (downChance > chance)
        {
            chosen = downList.ToArray();
        }
        if (chosen == null)
        {
            Finish();
            return;
        }
        Tile[] chosenTiles = new Tile[chosen.Length];
        for (int i = 0; i < chosen.Length; i++)
        {
            chosenTiles[i] = chosen[i].tile;
        }
        string flashlight = "";
        for (int i = 0; i < chosen.Length; i++)
        {
            flashlight += chosen[i].tile.letter + " ";
        }
        for (int i = 0; i < chosen.Length; i++)
        {
            if (chosen[i].tile.cord.x == g.tile.cord.x && chosen[i].tile.cord.y > g.tile.cord.y)
            {
                WallKiller.instnace.KillWall(chosen[i].tile, GridMaster.instnace.FindTile(new Cord(chosen[i].tile.cord.x, chosen[i].tile.cord.y - 1)));
            }
            if (chosen[i].tile.cord.x == g.tile.cord.x && chosen[i].tile.cord.y < g.tile.cord.y)
            {
                WallKiller.instnace.KillWall(chosen[i].tile, GridMaster.instnace.FindTile(new Cord(chosen[i].tile.cord.x, chosen[i].tile.cord.y + 1)));
            }
            if (chosen[i].tile.cord.x > g.tile.cord.x && chosen[i].tile.cord.y == g.tile.cord.y)
            {
                WallKiller.instnace.KillWall(chosen[i].tile, GridMaster.instnace.FindTile(new Cord(chosen[i].tile.cord.x - 1, chosen[i].tile.cord.y)));
            }
            if (chosen[i].tile.cord.x < g.tile.cord.x && chosen[i].tile.cord.y == g.tile.cord.y)
            {
                WallKiller.instnace.KillWall(chosen[i].tile, GridMaster.instnace.FindTile(new Cord(chosen[i].tile.cord.x + 1, chosen[i].tile.cord.y)));
            }
        }
        g.currentActions -= useCost;
        g.UseDiceSum(13);
        Debug.Log("Grenade on " + flashlight);
        FireMaster.instnace.FireTiles(chosenTiles, Finish, upgradeName);
    }
    public override bool CheckUseDice(int[] dice)
    {
        for (int i = 0; i < dice.Length; i++)
        {
            for (int k = 0; k < dice.Length; k++)
            {
                if (i == k) continue;
                for (int j = 0; j < dice.Length; j++)
                {
                    if (j == k || j == i) continue;
                    if (dice[i] + dice[k] + dice[j] == 13)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
