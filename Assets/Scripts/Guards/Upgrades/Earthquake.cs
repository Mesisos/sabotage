﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Earthquake : UpgradeBase {
    public override void UseAbility(Guard g, Action finishCall)
    {
        base.UseAbility(g, finishCall);
        g.UseDiceSum(11);
        Debug.Log("Earthquake!!");
    }
    public override bool CheckUseDice(int[] dice)
    {
        for (int i = 0; i < dice.Length; i++)
        {
            for (int k = 0; k < dice.Length; k++)
            {
                if (i == k) continue;
                for (int j = 0; j < dice.Length; j++)
                {
                    if (j == k || j == i) continue;
                    if (dice[i] + dice[k] + dice[j] == 11)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
