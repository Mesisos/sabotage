﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoneyPot : UpgradeBase {
    public override void UseAbility(Guard g, Action finishCall)
    {
        base.UseAbility(g, finishCall);
        HoneyPotMaster.instance.AddHoneyPot(g.tile);
        g.UseDiceSum(7);
        Debug.Log("Honey Pot");
        Finish();
    }
    public override bool CheckUseDice(int[] dice)
    {
        if(HoneyPotMaster.instance.honeyPotLocations.Count > 0)
        {
            return false;
        }
        for (int i = 0; i < dice.Length; i++)
        {
            for (int k = 0; k < dice.Length; k++)
            {
                if (i == k) continue;
                if (dice[i] + dice[k] == 7)
                {
                    return true;
                }
            }
        }
        return false;
    }
}
