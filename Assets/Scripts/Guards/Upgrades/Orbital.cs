﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbital : UpgradeBase {
    public override void UseAbility(Guard g, Action finishCall)
    {
        base.UseAbility(g, finishCall);
        List<ScanTile> list = new List<ScanTile>();
        ScanTile[] temp = ScanFinder.instance.GetCombined();
        if(temp == null)
        {
            Finish();
            return;
        }
        list.AddRange(temp);
        for (int i = 0; i < temp.Length; i++)
        {
            if(temp[i].tile.cord == g.tile.cord)
            {
                list.Remove(temp[i]);
            }
            if (temp[i].tile.cord.x == g.tile.cord.x && temp[i].tile.cord.y == g.tile.cord.y + 1)
            {
                list.Remove(temp[i]);
            }
            if (temp[i].tile.cord.x == g.tile.cord.x && temp[i].tile.cord.y == g.tile.cord.y - 1)
            {
                list.Remove(temp[i]);
            }
            if (temp[i].tile.cord.x == g.tile.cord.x + 1 && temp[i].tile.cord.y == g.tile.cord.y)
            {
                list.Remove(temp[i]);
            }
            if (temp[i].tile.cord.x == g.tile.cord.x - 1 && temp[i].tile.cord.y == g.tile.cord.y)
            {
                list.Remove(temp[i]);
            }
        }
        int pos = 0;
        float chance = -1;
        for (int i = 0; i < list.Count; i++)
        {
            if(list[i].chance > chance)
            {
                chance = list[i].chance;
                pos = i;
            }
        }
        g.currentActions -= useCost;
        g.UseDiceSum(14);
        Debug.Log("Orbital on " + list[pos].tile.letter);
        FireMaster.instnace.FireTiles(new Tile[] { list[pos].tile }, Finish, upgradeName);
    }
    public override bool CheckUseDice(int[] dice)
    {
        for (int i = 0; i < dice.Length; i++)
        {
            for (int k = 0; k < dice.Length; k++)
            {
                if (i == k) continue;
                for (int j = 0; j < dice.Length; j++)
                {
                    if (j == k || j == i) continue;
                    if (dice[i] + dice[k] + dice[j] == 13)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
