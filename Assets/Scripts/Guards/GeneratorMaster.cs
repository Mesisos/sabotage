﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class GeneratorMaster : MonoBehaviour {
    public static GeneratorMaster instance;

    private void Awake()
    {
        instance = this;
    }
    public void LoadGenerators()
    {
        generators = new Generator[2];
        generators[0] = new Generator(0);
        generators[1] = new Generator(0);
        generators[0].tile = GridMaster.instnace.generators[0];
        generators[1].tile = GridMaster.instnace.generators[1];
    }
    public Generator[] generators;
    public void CreateGenerators()
    {
        generators = new Generator[2];
        generators[0] = new Generator(0);
        generators[1] = new Generator(0);
        generators[0].tile = GridMaster.instnace.generators[0];
        generators[1].tile = GridMaster.instnace.generators[1];
    }
    public void PowerGenerator(Tile t)
    {
        for (int i = 0; i < generators.Length; i++)
        {
            if(generators[i].tile == t)
            {
                generators[i].AddPower();
                break;
            }
        }
    }
    public int GeneratorsOn()
    {
        int count = 0;
        for (int i = 0; i < generators.Length; i++)
        {
            if(generators[i].isOn)
            {
                count++;
            }
        }
        return count;
    }
    public Generator GetGenerator(Tile t)
    {
        for (int i = 0; i < generators.Length; i++)
        {
            if(generators[i].tile.cord == t.cord)
            {
                return generators[i];
            }
        }
        return null;
    }
    public void Scan(Tile[] t)
    {
        for (int i = 0; i < generators.Length; i++)
        {
            for (int k = 0; k < t.Length; k++)
            {
                if(generators[i].tile.cord == t[k].cord)
                {
                    generators[i].tile.SetGeneratorCount(generators[i].powerCount);
                }
            }
        }
    }
    public void Hack(Tile t)
    {
        for (int i = 0; i < generators.Length; i++)
        {
            if(generators[i].tile.cord == t.cord)
            {
                generators[i].powerCount -= 1;
                if(generators[i].powerCount < 0)
                {
                    generators[i].powerCount = 0;
                }
                generators[i].tile.SetGeneratorCount(generators[i].powerCount);
            }
        }
    }
    [Serializable]
    public class Generator
    {
        public Tile tile;
        public int powerCount;
        public bool isOn
        {
            get
            {
                return powerCount > 1;
            }
        }
        public Generator(int p)
        {
            powerCount = p;
        }
        public void AddPower(int amount = 1)
        {
            powerCount += amount;
            if(powerCount > 2)
            {
                powerCount = 2;
            }
        }
    }
}
