﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Guard : MonoBehaviour {
    public string guardName;
    public int maxActions;
    public int currentActions;
    public Tile tile;
    public bool stunned;
    [SerializeField]
    Image image;
    public List<int> dice = new List<int>();
    bool triedSomthingElse;
    bool shootingSomthing;
    public UpgradeBase[] tier0;
    public UpgradeBase[] tier1;
    public UpgradeBase[] tier2;
    public UpgradeBase[] tier3;
    public bool extraAction;
    public System.Action EventTurnStart;
    public List<System.Action> turnEvents = new List<System.Action>();
    public bool revealed;
    public void Reveal()
    {
        image.enabled = true;
    }
    public bool CheckRevealable(Tile[] tiles)
    {
        if (revealed) return false;
        if (image.enabled) return false;
        for (int i = 0; i < tiles.Length; i++)
        {
            if(tile.cord == tiles[i].cord)
            {
                revealed = true;
                if (CheckForCamo()) return true;
                Reveal();
                return true;
            }
        }
        return false;
    }
    bool CheckForCamo()
    {
        for (int i = 0; i < tier2.Length; i++)
        {
            if(tier2[i].unlocked && tier2[i].upgradeName == "Camo")
            {
                return true;
            }
        }
        return false;
    }
    public void StartTurn()
    {
        if(tile == null)
        {
            tile = GridMaster.instnace.GetRandomTile();
            while (GridMaster.instnace.generators.Contains(tile))
            {
                tile = GridMaster.instnace.GetRandomTile();
            }
            transform.position = tile.transform.position;
        }
        Debug.Log("Guard Turn Start " + guardName);
        dice.Clear();
        dice.AddRange(DiceMaster.instnace.diceValues);
        image.enabled = false;
        revealed = false;
        maxActions = 2 + GeneratorMaster.instance.GeneratorsOn();
        currentActions = maxActions;
        if(GameMaster.instance.difficulty == Difficulty.Easy)
        {
            currentActions --;
        }
        if(currentActions < 2)
        {
            currentActions = 2;
        }
        if(stunned)
        {
            stunned = false;
            currentActions = 1;
        }
        if(extraAction)
        {
            extraAction = false;
            currentActions++;
        }
        if(currentActions > 4)
        {
            currentActions = 4;
        }
        if(EventTurnStart != null)
        {
            EventTurnStart();
        }
        //Roll for Options
        triedSomthingElse = false;
        int genChance = GeneratorChance();
        int shootChance = ShootChance();
        turnEvents.Clear();
        turnEvents.Add(Generator);
        turnEvents.Add(Shooting);
        turnEvents.Add(Upgarding);
        if(genChance + UnityEngine.Random.Range(0, 20) > shootChance + UnityEngine.Random.Range(0,20) + HackMaster.instnace.numberOfHacks * 15)
        {
            System.Action next = turnEvents[0];
            turnEvents.RemoveAt(0);
            next();
        }
        else if(shootChance > 20)
        {
            System.Action next = turnEvents[1];
            turnEvents.Remove(next);
            next();
        }
        else
        {
            System.Action next = turnEvents[2];
            turnEvents.Remove(next);
            next();
        }
    }
    void TrySomthingElse()
    {
        if(currentActions < 1)
        {
            EndTurn();
            return;
        }
        if (turnEvents.Count > 0)
        {
            System.Action next = turnEvents[UnityEngine.Random.Range(0, turnEvents.Count)];
            turnEvents.Remove(next);
            next();
        }
        else
        {
            EndTurn();
        }
    }
    int GeneratorChance()
    {
        List<Tile> genSpots = new List<Tile>();
        for (int i = 0; i < GeneratorMaster.instance.generators.Length; i++)
        {
            if(!GeneratorMaster.instance.generators[i].isOn)
            {
                genSpots.Add(GeneratorMaster.instance.generators[i].tile);
            }
        }
        if(genSpots.Count < 1)
        {
            return 0;
        }
        List<Tile[]> paths = new List<Tile[]>();
        for (int i = 0; i < genSpots.Count; i++)
        {
            paths.Add(Pathing.PathFindReturned(tile, genSpots[i], GridMaster.instnace.allTiles.ToArray(), GridMaster.instnace.AllWallCords()));
        }
        int pos = 0;
        int dis = int.MaxValue;
        for (int i = 0; i < paths.Count; i++)
        {
            if(paths[i].Length < dis)
            {
                dis = paths[i].Length;
                pos = i;
            }
        }
        return 70 - (dis * 30);
    }
    int ShootChance()
    {
        TargetTiles[] targetPath = GetTargetTile();
        if (targetPath == null)
        {
            return 0;
        }
        TargetTiles best = FindBestTargetTile(targetPath);
        if(best == null || best.path == null)
        {
            return 0;
        }
        return best.score - (best.path.Length * 25);
    }
    void Generator()
    {
        Debug.Log("Trying Generators");
        List<Tile> genSpots = new List<Tile>();
        for (int i = 0; i < GeneratorMaster.instance.generators.Length; i++)
        {
            if (!GeneratorMaster.instance.generators[i].isOn)
            {
                genSpots.Add(GeneratorMaster.instance.generators[i].tile);
            }
        }
        if (genSpots.Count < 1)
        {
            TrySomthingElse();
            return;
        }
        List<Tile[]> paths = new List<Tile[]>();
        for (int i = 0; i < genSpots.Count; i++)
        {
            paths.Add(Pathing.PathFindReturned(tile, genSpots[i], GridMaster.instnace.allTiles.ToArray(), GridMaster.instnace.AllWallCords()));
        }
        int pos = 0;
        int dis = int.MaxValue;
        for (int i = 0; i < paths.Count; i++)
        {
            if(paths[i].Length < dis)
            {
                dis = paths[i].Length;
                pos = i;
            }
        }
        if(paths[pos] == null)
        {
            LockDoorKill();
            return;
        }
        Move(paths[pos], currentActions);
        PowerGenerator(tile);
    }
    void PowerGenerator(Tile t)
    {
        GeneratorMaster.Generator g = GeneratorMaster.instance.GetGenerator(t);
        if(g == null || currentActions < 1)
        {
            TrySomthingElse();
            return;
        }
        if(!CheckDice((x, y) => (x == y || x == y + 1) ? 1 : 0, 3) && ModifyDice((x, y) => (x == y || x == y + 1) ? 1 : 0, 3))
        {

        }
        if(currentActions > 0 && !GeneratorMaster.instance.GetGenerator(t).isOn && CheckDice((x, y) => (x == y || x == y + 1) ? 1 : 0, 3))
        {
            currentActions--;
            GeneratorMaster.instance.PowerGenerator(t);
            Debug.Log("Guard Add cube to Gen at " + t.cord.ToString());
            UseDice((x, y) => (x == y || x == y + 1) ? 1 : 0, 3);
        }
        /*
        while (currentActions > 0 && !GeneratorMaster.instance.GetGenerator(t).isOn && CheckDice((x, y) => (x == y || x == y + 1) ? 1 : 0, 3))
        {
            currentActions--;
            GeneratorMaster.instance.PowerGenerator(t);
            Debug.Log("Guard Add cube to Gen at " + t.cord.ToString());
            UseDice((x, y) => (x == y || x == y + 1) ? 1 : 0, 3);
        }
        */
        TrySomthingElse();
    }
    UpgradeBase shootUpgrade;
    UpgradeBase revealUpgrade;
    UpgradeBase otherUpgrade;
    void Shooting()
    {
        Debug.Log("Trying Shooting");
        TargetTiles[] targetPath = GetTargetTile();
        if (targetPath == null)
        {
            TrySomthingElse();
            return;
        }
        TargetTiles best = FindBestTargetTile(targetPath);
        if(best == null)
        {
            LockDoorKill();
            return;
        }
        shootUpgrade = FindBestUpgrade(UpgradeType.Shooting, currentActions);
        int allowedMove = currentActions;
        if(shootUpgrade != null)
        {
            allowedMove -= shootUpgrade.useCost;
        }
        Move(best.path, allowedMove);
        revealUpgrade = FindBestUpgrade(UpgradeType.Reveal, currentActions - (shootUpgrade != null ? shootUpgrade.useCost : 0));
        otherUpgrade = FindBestUpgrade(UpgradeType.Other, currentActions - (shootUpgrade != null ? shootUpgrade.useCost : 0) - (revealUpgrade != null ? revealUpgrade.useCost : 0));
        FinishReveal();
    }
    UpgradeBase FindBestUpgrade(UpgradeType type, int actions)
    {
        for (int i = 0; i < tier3.Length; i++)
        {
            if(tier3[i] != null && tier3[i].type == type && tier3[i].useCost <= actions && tier3[i].unlocked && tier3[i].CheckUseDice(dice.ToArray()))
            {
                return tier3[i];
            }
        }
        for (int i = 0; i < tier2.Length; i++)
        {
            if (tier2[i] != null && tier2[i].type == type && tier2[i].useCost <= actions && tier2[i].unlocked && tier2[i].CheckUseDice(dice.ToArray()))
            {
                return tier2[i];
            }
        }
        for (int i = 0; i < tier1.Length; i++)
        {
            if (tier1[i] != null && tier1[i].type == type && tier1[i].useCost <= actions && tier1[i].unlocked && tier1[i].CheckUseDice(dice.ToArray()))
            {
                return tier1[i];
            }
        }
        for (int i = 0; i < tier0.Length; i++)
        {
            if (tier0[i] != null && tier0[i].type == type && tier0[i].useCost <= actions && tier0[i].unlocked && tier0[i].CheckUseDice(dice.ToArray()))
            {
                return tier0[i];
            }
        }
        return null;
    }
    void Upgarding()
    {
        Debug.Log("Trying Upgrading");
        if(GameMaster.instance.currentUnlock == GameUnlock.Tier3 || GameMaster.instance.currentUnlock == GameUnlock.Tier2 || GameMaster.instance.currentUnlock == GameUnlock.Tier1)
        {
            //Tier 1
            bool unlockCheck = false;
            for (int i = 0; i < tier1.Length; i++)
            {
                if (tier1[i].unlocked)
                {
                    unlockCheck = true;
                    break;
                }
            }
            if (!unlockCheck)
            {
                UpgradeBase b = null;
                for (int i = 0; i < tier1.Length; i++)
                {
                    for (int k = 0; k < tier1[i].dicePaid.Length; k++)
                    {
                        if (tier1[i].dicePaid[k])
                        {
                            b = tier1[i];
                            break;
                        }
                    }
                    if (b != null) break;
                }
                if (b == null) b = tier1[UnityEngine.Random.Range(0, tier1.Length)];
                while (currentActions > 0 && !b.unlocked && b.CheckUpgradeDice(dice.ToArray()))
                {
                    b.Upgrade(this);
                    currentActions--;
                }
            }
            //Tier 2
            bool check = false;
            for (int q = 0; q < tier1.Length; q++)
            {
                if (tier1[q].unlocked)
                {
                    check = true;
                    break;
                }
            }
            if (!check)
            {
                TrySomthingElse();
                return;
            }
            unlockCheck = false;
            for (int i = 0; i < tier2.Length; i++)
            {
                if (tier2[i].unlocked)
                {
                    unlockCheck = true;
                    break;
                }
            }
            if (!unlockCheck)
            {
                UpgradeBase b = null;
                for (int i = 0; i < tier2.Length; i++)
                {
                    for (int k = 0; k < tier2[i].dicePaid.Length; k++)
                    {
                        if (tier2[i].dicePaid[k])
                        {
                            b = tier2[i];
                            break;
                        }
                    }
                    if (b != null) break;
                }
                if (b == null) b = tier2[UnityEngine.Random.Range(0, tier2.Length)];
                while (currentActions > 0 && !b.unlocked && b.CheckUpgradeDice(dice.ToArray()))
                {
                    b.Upgrade(this);
                    currentActions--;
                }
            }
            //Tier 3
            check = false;
            for (int q = 0; q < tier2.Length; q++)
            {
                if (tier2[q].unlocked)
                {
                    check = true;
                    break;
                }
            }
            if (!check)
            {
                TrySomthingElse();
                return;
            }
            unlockCheck = false;
            for (int i = 0; i < tier3.Length; i++)
            {
                if (tier3[i].unlocked)
                {
                    unlockCheck = true;
                    break;
                }
            }
            if (!unlockCheck)
            {
                UpgradeBase b = null;
                for (int i = 0; i < tier3.Length; i++)
                {
                    for (int k = 0; k < tier3[i].dicePaid.Length; k++)
                    {
                        if (tier3[i].dicePaid[k])
                        {
                            b = tier3[i];
                            break;
                        }
                    }
                    if (b != null) break;
                }
                if (b == null) b = tier3[UnityEngine.Random.Range(0, tier3.Length)];
                while (currentActions > 0 && !b.unlocked && b.CheckUpgradeDice(dice.ToArray()))
                {
                    b.Upgrade(this);
                    currentActions--;
                }
            }
        }
        TrySomthingElse();
    }
    TargetTiles FindBestTargetTile(TargetTiles[] t)
    {
        int pos = -1;
        int score = int.MinValue;
        for (int i = 0; i < t.Length; i++)
        {
            if (t[i].path == null) continue;
            if (t[i].path != null && t[i].path.Length > maxActions - 1) continue;
            if(t[i].score - (t[i].path != null ? t[i].path.Length * 25 : 0) > score)
            {
                pos = i;
                score = t[i].score - (t[i].path.Length * 25);
            }
        }
        if (pos < 0) return null;
        return t[pos];
    }
    TargetTiles[] GetTargetTile()
    {
        List<ScanTile> targets = new List<ScanTile>();
        targets.AddRange(ScanFinder.instance.previousPlayerTwo);
        targets.AddRange(ScanFinder.instance.previousPlayerOne);
        ScanTile[] t = targets.ToArray();
        if (t.Length < 1) return null;
        TargetTiles[] targetsTile = FindBestTiles(t);
        for (int i = 0; i < targetsTile.Length; i++)
        {
            targetsTile[i].path = Pathing.PathFindReturned(tile, targetsTile[i].tile, GridMaster.instnace.GetAllTilesWithLockDoors(), GridMaster.instnace.AllWallCords());
        }
        return targetsTile;
    }
    TargetTiles[] FindBestTiles(ScanTile[] tiles)
    {
        List<TargetTiles> temp = new List<TargetTiles>();
        Tile[] allTiles = GridMaster.instnace.allTiles.ToArray();
        List<TargetTiles> targets = new List<TargetTiles>();
        for (int i = 0; i < allTiles.Length; i++)
        {
            Tile[] aroundTiles = GridMaster.instnace.GetAllTilesAround(allTiles[i], true);
            int score = 0;
            List<ScanTile> scanTiles = new List<ScanTile>();
            for (int k = 0; k < aroundTiles.Length; k++)
            {
                if (aroundTiles[k] == null) continue;
                for (int l = 0; l < tiles.Length; l++)
                {
                    if(aroundTiles[k] == tiles[l].tile)
                    {
                        score += (int)tiles[l].chance;
                        scanTiles.Add(tiles[l]);
                    }
                }
            }
            if(score > 0)
            {
                targets.Add(new TargetTiles(score, allTiles[i], scanTiles.ToArray()));
            }
        }
        return targets.ToArray();
    }
    void LockDoorKill()
    {
        if(LockDoorMaster.instnace.lockedList.Count < 1)
        {
            TrySomthingElse();
            return;
        }
        List<Tile[]> paths = new List<Tile[]>();
        for (int i = 0; i < LockDoorMaster.instnace.lockedList.Count; i++)
        {
            paths.Add(Pathing.PathFindReturned(tile, LockDoorMaster.instnace.lockedList[i], GridMaster.instnace.GetAllTilesWithLockDoors(), GridMaster.instnace.AllWallCords()));
        }
        int pos = -1;
        int len = currentActions;
        for (int i = 0; i < paths.Count; i++)
        {
            if (paths[i] == null) continue;
            if(paths[i].Length < len)
            {
                len = paths[i].Length;
                pos = i;
            }
        }
        if(pos == -1)
        {
            TrySomthingElse();
            return;
        }
        List<Tile> tempPath = new List<Tile>();
        tempPath.AddRange(paths[pos]);
        if(tempPath.Count > 0)
        {
            tempPath.RemoveAt(tempPath.Count - 1);
            Move(tempPath.ToArray(), currentActions - 1);
        }
        FireMaster.instnace.FireTiles(new Tile[] { LockDoorMaster.instnace.lockedList[pos] }, TrySomthingElse, "Pistol");
    }
    void Move(Tile[] tiles, int allowedMovement)
    {
        if (tiles == null || tiles.Length < 1) return;
        Queue<Tile> path = new Queue<Tile>();
        for (int i = 0; i < tiles.Length; i++)
        {
            path.Enqueue(tiles[i]);
        }
        while(allowedMovement > 0 && path.Count > 0 && UseDice((x,y) => (x >= y ? 0 : 1), 5))
        {
            Tile moveTo = path.Dequeue();
            Debug.Log("Guard Move " + tile.cord.ToString() + " to " + moveTo.cord.ToString());
            tile = moveTo;
            allowedMovement--;
            currentActions--;
        }
        Vector3 offset = Vector3.right * (Screen.width * .02f);
        if(GuardMaster.instance.guardPos == 0)
        {
            offset = Vector3.left * (Screen.width * .02f);
        }
        Vector3 tilePos = tile.transform.position;
        float amount = tile.GetComponent<RectTransform>().sizeDelta.x / 2;
        tilePos.y += amount;
        tilePos.x += amount;
        transform.position = tilePos + offset;
    }
    public void Move(Tile t)
    {
        tile = t;
        Vector3 offset = Vector3.right * (Screen.width * .05f);
        if (GuardMaster.instance.guardPos == 0)
        {
            offset = Vector3.left * (Screen.width * .03f);
        }
        Vector3 tilePos = tile.transform.position;
        float amount = tile.GetComponent<RectTransform>().sizeDelta.x / 2;
        tilePos.y += amount;
        tilePos.x += amount;
        transform.position = tilePos + offset;
        image.enabled = false;
    }
    void FinishReveal()
    {
        if(revealUpgrade != null)
        {
            revealUpgrade.UseAbility(this, FinishFiring);
        }
        else
        {
            FinishFiring();
        }
    }
    void FinishFiring()
    {
        if (shootUpgrade != null)
        {
            shootUpgrade.UseAbility(this, FinishOther);
        }
        else
        {
            FinishOther();
        }
    }
    void FinishOther()
    {
        if(otherUpgrade != null)
        {
            otherUpgrade.UseAbility(this, TrySomthingElse);
        }
        else
        {
            TrySomthingElse();
        }
    }
    void EndTurn()
    {
        if(GuardMaster.activeGuardActions > 0)
        {
            Debug.LogError("Guard Actions Still in Process: " + GuardMaster.activeGuardActions);
            return;
        }
        GuardMaster.instance.NextGuard();
    }
    public bool CheckDiceSum(int value)
    {
        for (int i = 0; i < dice.Count; i++)
        {
            for (int k = 0; k < dice.Count; k++)
            {
                if (i == k) continue;
                if(dice[i] + dice[k] == value)
                {
                    return true;
                }
            }
        }
        return false;
    }
    public bool UseDiceSum(int value)
    {
        if (value > 12)
        {
            for (int i = 0; i < dice.Count; i++)
            {
                for (int k = 0; k < dice.Count; k++)
                {
                    if (i == k) continue;
                    for (int j = 0; j < dice.Count; j++)
                    {
                        if (j == i || j == k) continue;
                        if (dice[i] + dice[k] + dice[j] == value)
                        {
                            int number = dice[k];
                            int number2 = dice[j];
                            dice.Remove(dice[i]);
                            dice.Remove(number);
                            dice.Remove(number2);
                            return true;
                        }
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < dice.Count; i++)
            {
                for (int k = 0; k < dice.Count; k++)
                {
                    if (i == k) continue;
                    if (dice[i] + dice[k] == value)
                    {
                        int number = dice[k];
                        dice.Remove(dice[i]);
                        dice.Remove(number);
                        return true;
                    }
                }
            }
        }
        return false;
    }
    public bool CheckDice(System.Comparison<int> c, int value)
    {
        for (int i = 0; i < dice.Count; i++)
        {
            if (c(dice[i], value) > 0)
            { 
                return true;
            }
        }
        return false;
    }
    public bool UseDice(System.Comparison<int> c, int value)
    {
        for (int i = 0; i < dice.Count; i++)
        {
            if(c(dice[i], value) > 0)
            {
                dice.RemoveAt(i);
                return true;
            }
        }
        return false;
    }
    public bool UseDice(int die)
    {
        for (int i = 0; i < dice.Count; i++)
        {
            if(die == dice[i])
            {
                dice.RemoveAt(i);
                return true;
            }
        }
        return false;
    }
    public bool ModifyDice(System.Comparison<int> c, int value)
    {
        int[] dUp = dice.ToArray();
        int[] dDown = dice.ToArray();
        for (int i = 0; i < dUp.Length; i++)
        {
            dUp[i] += 1;
            if(dUp[i] > 6)
            {
                dUp[i] = 1;
            }
            dDown[i] -= 1;
            if(dDown[i] < 1)
            {
                dDown[i] = 6;
            }
        }
        for (int i = 0; i < dUp.Length; i++)
        {
            if(c(dUp[i], value) > 0)
            {
                dice[i]++;
                if(dice[i] > 6)
                {
                    dice[i] = 1;
                }
                GuardMaster.instance.modifiers--;
                Debug.Log("Used Modify");
                return true;
            }
            if (c(dDown[i], value) > 0)
            {
                dice[i]--;
                if (dice[i] < 1)
                {
                    dice[i] = 6;
                }
                GuardMaster.instance.modifiers--;
                Debug.Log("Used Modify");
                return true;
            }
        }
        return false;
    }
    public class TargetTiles
    {
        public int score;
        public Tile tile;
        public ScanTile[] targets;
        public Tile[] path;
        public TargetTiles(int s, Tile t, ScanTile[] tar)
        {
            score = s;
            tile = t;
            targets = tar;
        }
    }
}
