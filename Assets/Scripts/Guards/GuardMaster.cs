﻿using Doozy.Engine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuardMaster : PhaseBase {
    public static GuardMaster instance;
    public static int activeGuardActions;
    [SerializeField]
    List<Guard> allGuards = new List<Guard>();
    public Guard[] guard;
    public int guardPos;
    public int modifiers;
    int maxModify = 2;
    [SerializeField]
    UIView uIView;
    private void Awake()
    {
        instance = this;
        uIView.ShowBehavior.OnFinished.Action += DelayedGuardStart;
    }
    public void AddGuard(string name)
    {
        List<Guard> list = new List<Guard>();
        if(guard.Length > 0)
        {
            list.AddRange(guard);
        }
        switch(name)
        {
            case "Chantico":
                list.Add(allGuards[0]);
                break;
            case "Nadesh":
                list.Add(allGuards[1]);
                break;
            case "Ophidian":
                list.Add(allGuards[2]);
                break;
            case "Count V":
                list.Add(allGuards[3]);
                break;
        }
        guard = list.ToArray();
    }
    public override void StartPhase()
    {
        base.StartPhase();
        if (GeneratorMaster.instance.generators == null || GeneratorMaster.instance.generators.Length == 0)
        {
            GeneratorMaster.instance.CreateGenerators();
        }
        if (guard == null || guard.Length < 2)
        {
            guard = new Guard[2];
            for (int i = 0; i < guard.Length; i++)
            {
                int pos = UnityEngine.Random.Range(0, allGuards.Count);
                guard[i] = allGuards[pos];
                allGuards.RemoveAt(pos);
            }
        }
        uIView.Show();
    }
    void DelayedGuardStart(GameObject g)
    {
        Debug.Log("____GUARD TURN START_____");
        guardPos = 0;
        guard[guardPos].StartTurn();
    }
    public override void EndPhase()
    {
        Debug.Log("____GUARD TURN END_____");
        base.EndPhase();
        uIView.Hide();
    }
    public void NextGuard()
    {
        guardPos++;
        if(guard.Length <= guardPos)
        {
            EndPhase();
            return;
        }
        guard[guardPos].StartTurn();
    }
    public void AddModify()
    {
        modifiers++;
        maxModify++;
        return;
        if(maxModify % 4 == 0 || maxModify % 6 == 0 || maxModify % 8 == 0)
        {
            guard[0].extraAction = true;
        }
    }
    public Guard GetCurrentGuard()
    {
        return guard[guardPos];
    }
}
