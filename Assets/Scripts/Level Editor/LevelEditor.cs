﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy;
using Doozy.Engine.UI;
using TMPro;

public class LevelEditor : MonoBehaviour
{
    public static LevelEditor instnace;

    private void Awake()
    {
        instnace = this;
    }
    enum LevelPhase
    {
        Walls,
        Dooms,
        Gens,
    }
    LevelPhase CurrentPhase;
    [SerializeField]
    UIView view;
    [SerializeField]
    UIView main;
    [SerializeField]
    TextMeshProUGUI infoText;
    public void StartLevelEditor()
    {
        view.Show();
        main.Hide();
        CurrentPhase = LevelPhase.Walls;
        GridMaster.instnace.ClearMap();
        infoText.text = "Walls Placed: 0/" + GridMaster.instnace.wallCount;
        CreateWallButtons();
    }
    public void NextPhase()
    {
        if (CurrentPhase == LevelPhase.Gens)
        {
            Finish();
            return;
        }
        CurrentPhase = (LevelPhase)((int)CurrentPhase + 1);
        if (CurrentPhase == LevelPhase.Dooms)
        {
            infoText.text = "Dooms Days Placed: " + GridMaster.instnace.doomsDays.Count + "/" + GridMaster.instnace.doomsDayCount;
            CreateDoomsDayButtons();
        }
        if(CurrentPhase == LevelPhase.Gens)
        {
            infoText.text = "Generators Placed: " + GridMaster.instnace.generators.Count + "/" + GridMaster.instnace.generatorCount;
            CreateGenButtons();
        }
    }
    void Finish()
    {
        main.Show();
        view.Hide();
        GridMaster.instnace.SaveState();
        GeneratorMaster.instance.LoadGenerators();
    }
    [SerializeField]
    Transform butWallPre;
    [SerializeField]
    Transform butPre;
    List<GameObject> buttonList = new List<GameObject>();
    void ClearButtons()
    {
        GameObject[] temp = buttonList.ToArray();
        for (int i = 0; i < temp.Length; i++)
        {
            Destroy(temp[i]);
        }
        buttonList.Clear();
    }
    Wall[] GetAllWallPlaces()
    {
        List<Wall> wallList = new List<Wall>();
        List<Wall> allWalls = new List<Wall>();
        allWalls.AddRange(GridMaster.instnace.allPossibleWalls.ToArray());

        Wall[] currentWallsChosen = GridMaster.instnace.allWalls.ToArray();
        for (int i = 0; i < currentWallsChosen.Length; i++)
        {
            allWalls.Remove(currentWallsChosen[i]);
        }
        for (int i = 0; i < allWalls.Count; i++)
        {
            List<Wall> temp = new List<Wall>();
            temp.Add(allWalls[i]);
            if (currentWallsChosen != null && currentWallsChosen.Length > 0)
            {
                temp.AddRange(currentWallsChosen);
            }
            if (WallCheck.PathingCheck(GridMaster.instnace.allCords.ToArray(), WallCheck.ConvertWallToWallCord(temp.ToArray())))
            {
                wallList.Add(allWalls[i]);
            }
        }
        return wallList.ToArray();
    }
    void CreateWallButtons()
    {
        ClearButtons();
        Wall[] w = GetAllWallPlaces();
        for (int i = 0; i < w.Length; i++)
        {
            CreateWallButton(w[i]);
        }
    }
    void CreateWallButton(Wall g)
    {
        Transform t = Instantiate(butWallPre) as Transform;
        t.transform.position = g.transform.position;
        t.SetParent(view.transform);
        t.GetComponent<LevelButton>().piece = g.gameObject;
        buttonList.Add(t.gameObject);
    }
    Tile[] GetAllDoomsAndGenSpots()
    {
        List<Tile> list = new List<Tile>();
        list.AddRange(GridMaster.instnace.allTiles.ToArray());
        List<Tile> doomsDay = new List<Tile>();
        if(GridMaster.instnace.doomsDays.Count > 0)
        {
            doomsDay.AddRange(GridMaster.instnace.doomsDays);
        }
        if(GridMaster.instnace.generators.Count > 0)
        {
            doomsDay.AddRange(GridMaster.instnace.generators);
        }
        for (int i = 0; i < doomsDay.Count; i++)
        {
            list.Remove(doomsDay[i]);
            if (Pathing.CheckUp(GridMaster.instnace.allCords.ToArray(), WallCheck.ConvertWallToWallCord(GridMaster.instnace.allWalls.ToArray()), doomsDay[i].cord))
            {
                Tile t = GridMaster.instnace.FindTile(new Cord(doomsDay[i].cord.x, doomsDay[i].cord.y + 1));
                if (t != null)
                {
                    list.Remove(t);
                }
            }
            if (Pathing.CheckDown(GridMaster.instnace.allCords.ToArray(), WallCheck.ConvertWallToWallCord(GridMaster.instnace.allWalls.ToArray()), doomsDay[i].cord))
            {
                Tile t = GridMaster.instnace.FindTile(new Cord(doomsDay[i].cord.x, doomsDay[i].cord.y - 1));
                if (t != null)
                {
                    list.Remove(t);
                }
            }
            if (Pathing.CheckLeft(GridMaster.instnace.allCords.ToArray(), WallCheck.ConvertWallToWallCord(GridMaster.instnace.allWalls.ToArray()), doomsDay[i].cord))
            {
                Tile t = GridMaster.instnace.FindTile(new Cord(doomsDay[i].cord.x - 1, doomsDay[i].cord.y));
                if (t != null)
                {
                    list.Remove(t);
                }
            }
            if (Pathing.CheckRight(GridMaster.instnace.allCords.ToArray(), WallCheck.ConvertWallToWallCord(GridMaster.instnace.allWalls.ToArray()), doomsDay[i].cord))
            {
                Tile t = GridMaster.instnace.FindTile(new Cord(doomsDay[i].cord.x + 1, doomsDay[i].cord.y));
                if (t != null)
                {
                    list.Remove(t);
                }
            }
        }
        return list.ToArray();
    }
    void CreateDoomsDayButtons()
    {
        ClearButtons();
        Tile[] list = GetAllDoomsAndGenSpots();
        for (int i = 0; i < list.Length; i++)
        {
            CreateButton(list[i], "Place Dooms Day");
        }
    }
    void CreateGenButtons()
    {
        ClearButtons();
        Tile[] list = GetAllDoomsAndGenSpots();
        for (int i = 0; i < list.Length; i++)
        {
            CreateButton(list[i], "Place Generator");
        }
    }
    void CreateButton(Tile g, string text)
    {
        Transform t = Instantiate(butPre) as Transform;
        t.transform.position = g.transform.GetChild(0).transform.position;
        t.SetParent(view.transform);
        t.GetComponent<LevelButton>().piece = g.gameObject;
        t.GetComponentInChildren<TextMeshProUGUI>().text = text;
        buttonList.Add(t.gameObject);
    }
    public void ButtonClicked(GameObject g)
    {
        if(g.GetComponent<Wall>())
        {
            GridMaster.instnace.AddWall(g.GetComponent<Wall>());
            infoText.text = "Walls Placed: " + GridMaster.instnace.allWalls.Count + "/" + GridMaster.instnace.wallCount;
            if (GridMaster.instnace.allWalls.Count >= GridMaster.instnace.wallCount)
            {
                NextPhase();
                return;
            }
            else
            {
                CreateWallButtons();
            }
        }
        else
        {
            if(CurrentPhase == LevelPhase.Dooms)
            {
                GridMaster.instnace.AddDoomsDay(g.GetComponent<Tile>());
                infoText.text = "Dooms Days Placed: " + GridMaster.instnace.doomsDays.Count + "/" + GridMaster.instnace.doomsDayCount;
                if (GridMaster.instnace.doomsDayCount == GridMaster.instnace.doomsDays.Count)
                {
                    NextPhase();
                    return;
                }
                else
                {
                    CreateDoomsDayButtons();
                }
            }
            if(CurrentPhase == LevelPhase.Gens)
            {
                GridMaster.instnace.AddGen(g.GetComponent<Tile>());
                infoText.text = "Generators Placed: " + GridMaster.instnace.generators.Count + "/" + GridMaster.instnace.generatorCount;
                if (GridMaster.instnace.generatorCount == GridMaster.instnace.generators.Count)
                {
                    NextPhase();
                    return;
                }
                else
                {
                    CreateGenButtons();
                }
            }
        }
    }

    public void AutoComplete()
    {
        if(CurrentPhase == LevelPhase.Walls)
        {
            while(GridMaster.instnace.allWalls.Count < GridMaster.instnace.wallCount)
            {
                Wall[] w = GetAllWallPlaces();
                Wall target = w[UnityEngine.Random.Range(0, w.Length)];
                GridMaster.instnace.AddWall(target);
            }
            CurrentPhase = LevelPhase.Dooms;
        }
        if(CurrentPhase == LevelPhase.Dooms)
        {
            while(GridMaster.instnace.doomsDays.Count < GridMaster.instnace.doomsDayCount)
            {
                Tile[] t = GetAllDoomsAndGenSpots();
                Tile target = t[UnityEngine.Random.Range(0, t.Length)];
                GridMaster.instnace.AddDoomsDay(target);
            }
            CurrentPhase = LevelPhase.Gens;
        }
        if(CurrentPhase == LevelPhase.Gens)
        {
            while (GridMaster.instnace.generators.Count < GridMaster.instnace.generatorCount)
            {
                Tile[] t = GetAllDoomsAndGenSpots();
                Tile target = t[UnityEngine.Random.Range(0, t.Length)];
                GridMaster.instnace.AddGen(target);
            }
        }
        Finish();
    }
}
