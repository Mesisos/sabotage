﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ScanButton : MonoBehaviour {
    public Tile tile;
    public ScanType type;
    public Cord cord;
    public bool confirmClick;
    [SerializeField]
    TextMeshProUGUI subText;
    [SerializeField]
    TextMeshProUGUI mainText;
    [SerializeField]
    Image checkImage;
    public System.Action<Tile, ScanType> callBack;
    public System.Action<ScanButton> confirmBack;
    public void Setup(Tile t, ScanType type, System.Action<Tile,ScanType> call, System.Action<ScanButton> confirm, string subtext, string mainText = "Scan")
    {
        if (subtext != null)
        {
            subText.text = subtext;
        }
        this.mainText.text = mainText;
        callBack = call;
        confirmBack = confirm;
        tile = t;
        this.type = type;
        offset = Vector3.zero;
        float dis = t.GetComponent<RectTransform>().sizeDelta.x / 2 + GetComponent<RectTransform>().sizeDelta.x / 2;
        float per = Screen.height * .1f;
        switch (type)
        {
            case ScanType.Colume:
                offset = Vector3.up * (dis + per);
                break;
            case ScanType.Row:
                offset = Vector3.left * (dis + per);
                break;
            case ScanType.Quad:
                offset = Vector3.zero;
                break;
        }
        tilePos = t.transform.position;
        float amount = t.GetComponent<RectTransform>().sizeDelta.x / 2f;
        tilePos.y += amount;
        tilePos.x += amount;
        transform.position = tilePos + offset;
    }
    public void PlacedInit(System.Action<Tile, ScanType> call, System.Action<ScanButton> confirm)
    {
        callBack = call;
        confirmBack = confirm;
    }
    private void Start()
    {
        if(tile == null)
        {
            Invoke("DelayedStart", .2f);
        }
        SetupPairedCheck();
    }
    Vector3 offset;
    Vector3 tilePos;
    private void Update()
    {
        if (tile != null)
        {
            //transform.position = tilePos + offset;
        }
    }
    void DelayedStart()
    {
        if (confirmBack == null)
        {
            confirmBack = ScanMaster.instance.ButtonPressed;
        }
        if (callBack == null)
        {
            callBack = ScanMaster.instance.HighlightTiles;
        }
        tile = GridMaster.instnace.GetTile(cord);
        float dis = tile.GetComponent<RectTransform>().sizeDelta.x / 2 + GetComponent<RectTransform>().sizeDelta.x / 2;
        float per = Screen.height * .1f;
        switch (type)
        {
            case ScanType.Colume:
                offset = Vector3.up * (dis + per);
                break;
            case ScanType.Row:
                offset = Vector3.left * (dis + per);
                break;
            case ScanType.Quad:
                offset = Vector3.zero;
                break;
        }
        tilePos = tile.transform.position;
        float amount = tile.GetComponent<RectTransform>().sizeDelta.x / 2f;
        tilePos.y += amount;
        tilePos.x += amount;
        //transform.position = tilePos + offset;
    }
    public void Clear()
    {
        confirmClick = false;
        checkImage.gameObject.SetActive(false);
    }
    public List<ScanButton> pairedButtons = new List<ScanButton>();
    public System.Action<ScanButton> EventPairedCheck;
    void SetupPairedCheck()
    {
        for (int i = 0; i < pairedButtons.Count; i++)
        {
            pairedButtons[i].EventPairedCheck += PairedCheck;
        }
    }
    public void PairedCheck(ScanButton b)
    {
        for (int i = 0; i < pairedButtons.Count; i++)
        {
            if (b == pairedButtons[i]) continue;
            pairedButtons[i].Clear();
        }
    }
    public void Clicked()
    {
        if(!confirmClick)
        {
            if (callBack != null)
            {
                callBack(tile, type);
            }
            checkImage.gameObject.SetActive(true);
            confirmClick = true;
            EventPairedCheck?.Invoke(this);
            return;
        }
        checkImage.gameObject.SetActive(false);
        if (confirmBack != null)
        {
            confirmBack(this);
        }
    }
}
