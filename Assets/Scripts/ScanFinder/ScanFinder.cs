﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class ScanFinder : MonoBehaviour {
    public static ScanFinder instance;

    private void Awake()
    {
        instance = this;
    }
    int playerOneID;
    int playerTwoID;
    public List<ScanTile> previousPlayerOne = new List<ScanTile>();
    public List<ScanTile> previousPlayerTwo = new List<ScanTile>();
    float thermalDivde = 3;
    public ScanTile[] GetCombined()
    {
        List<ScanTile> list = new List<ScanTile>();
        for (int i = 0; i < previousPlayerOne.Count; i++)
        {
            list.Add(previousPlayerOne[i]);
        }
        for (int i = 0; i < previousPlayerTwo.Count; i++)
        {
            bool check = false;
            for (int k = 0; k < list.Count; k++)
            {
                if (list[k].tile.cord == previousPlayerTwo[i].tile.cord)
                {
                    list[k].chance += previousPlayerTwo[i].chance;
                    check = true;
                    break;
                }
            }
            if (!check)
            {
                list.Add(previousPlayerTwo[i]);
            }
        }
        return list.ToArray();
    }
    public void PlayerHere(Tile t, int player)
    {
        if (player == playerOneID)
        {
            List<ScanTile> temp = new List<ScanTile>();
            for (int i = 0; i < previousPlayerOne.Count; i++)
            {
                if (previousPlayerOne[i].chance == 100)
                {
                    temp.Add(previousPlayerOne[i]);
                }
            }
            previousPlayerOne.Clear();
            previousPlayerOne.AddRange(temp);
            previousPlayerOne.Add(new ScanTile(101, t));
        }
        else
        {
            List<ScanTile> temp = new List<ScanTile>();
            for (int i = 0; i < previousPlayerTwo.Count; i++)
            {
                if (previousPlayerTwo[i].chance == 100)
                {
                    temp.Add(previousPlayerTwo[i]);
                }
            }
            previousPlayerTwo.Clear();
            previousPlayerTwo.AddRange(temp);
            previousPlayerTwo.Add(new ScanTile(101, t));
        }
    }
    public void PlayerNotHere(Tile t, int player)
    {
        if (player == playerOneID)
        {
            
            for (int i = 0; i < previousPlayerOne.Count; i++)
            {
                if (previousPlayerOne[i].tile == t)
                {
                    previousPlayerOne.RemoveAt(i);
                    break;
                }
            }
        }
        else
        {
            for (int i = 0; i < previousPlayerTwo.Count; i++)
            {
                if (previousPlayerTwo[i].tile == t)
                {
                    previousPlayerTwo.RemoveAt(i);
                    break;
                }
            }
        }
    }
    public void PlayerShot(int player)
    {
        if (PlayerMaster.instnace.players[player].playerID == playerOneID)
        {
            previousPlayerOne.Clear();
        }
        else
        {
            previousPlayerTwo.Clear();
        }
    }
    public void RemoveTile(Tile t)
    {
        ScanTile[] tiles = previousPlayerOne.ToArray();
        previousPlayerOne.Clear();
        for (int i = 0; i < tiles.Length; i++)
        {
            if (tiles[i].tile.cord != t.cord)
            {
                previousPlayerOne.Add(tiles[i]);
            }
        }
        ScanTile[] tiles2 = previousPlayerTwo.ToArray();
        previousPlayerTwo.Clear();
        for (int i = 0; i < tiles2.Length; i++)
        {
            if (tiles2[i].tile.cord != t.cord)
            {
                previousPlayerTwo.Add(tiles2[i]);
            }
        }
    }
    public void Scanned(Tile[] tiles)
    {
        if (tiles.Length > 4)
        {
            ParachuteScan(tiles);
            return;
        }
        switch (GameMaster.instance.currentUnlock)
        {
            case GameUnlock.Tier0:
            case GameUnlock.Tier1:
                ThroughWallsScan(tiles);
                break;
            case GameUnlock.Tier2:
                string playerName = PlayerMaster.instnace.GetCurrentPlayerName();
                switch (playerName)
                {
                    case "Hawk":
                        if (UnityEngine.Random.Range(0, 100) > 90)
                        {
                            Debug.Log("Beacon Scan");
                            NormalScan(BeaconScan(tiles).ToArray());
                        }
                        if (UnityEngine.Random.Range(0, 100) > 70)
                        {
                            Debug.Log("Tunnel Scan");
                            NormalScan(TunnelScan(tiles).ToArray());
                        }
                        else
                        {
                            ThroughWallsScan(tiles);
                        }
                        break;
                    case "Jucier":
                        if (UnityEngine.Random.Range(0, 100) > 70)
                        {
                            Debug.Log("Zipline Scan");
                            NormalScan(ZiplineScan(tiles).ToArray());
                        }
                        else
                        {
                            ThroughWallsScan(tiles);
                        }
                        break;
                    default:
                        ThroughWallsScan(tiles);
                        break;
                }
                break;
            case GameUnlock.Tier3:
                playerName = PlayerMaster.instnace.GetCurrentPlayerName();
                switch (playerName)
                {
                    case "Hawk":
                        if (UnityEngine.Random.Range(0, 100) > 90)
                        {
                            Debug.Log("Beacon Scan");
                            NormalScan(BeaconScan(tiles).ToArray());
                        }
                        if (UnityEngine.Random.Range(0, 100) > 70)
                        {
                            Debug.Log("Tunnel Scan");
                            List<Tile> t = TunnelScan(tiles);
                            if (t == null || t.Count < 1)
                            {
                                if (PlayerMaster.instnace.playerOne)
                                {
                                    t = GetFirstPlayerTemp(tiles);
                                }
                                else
                                {
                                    t = GetSecondPlayerTemp(tiles);
                                }
                            }
                            NormalScan(t.ToArray());
                        }
                        else
                        {
                            ThroughWallsScan(tiles);
                        }
                        break;
                    case "Jucier":
                        if (UnityEngine.Random.Range(0, 100) > 70)
                        {
                            Debug.Log("Zipline Scan");
                            NormalScan(ZiplineScan(tiles).ToArray());
                        }
                        else
                        {
                            ThroughWallsScan(tiles);
                        }
                        break;
                    case "Acrobat":
                        if (UnityEngine.Random.Range(0, 100) > 90)
                        {
                            Debug.Log("Teleport Scan");
                            NormalScan(TeleportScan(tiles).ToArray());
                        }
                        else
                        {
                            ThroughWallsScan(tiles);
                        }
                        break;
                    default:
                        ThroughWallsScan(tiles);
                        break;
                }
                break;
        }
    }
    public void Hack(Tile t)
    {
        PlayerHere(t, PlayerMaster.instnace.GetCurrentPlayer().playerID);
    }
    public void Ear(int id)
    {
        if (playerOneID == id)
        {
            List<ScanTile> list = new List<ScanTile>();
            for (int i = 0; i < previousPlayerOne.Count; i++)
            {
                Tile[] t = GridMaster.instnace.GetAllTilesAround(previousPlayerOne[i].tile, true);
                for (int k = 0; k < t.Length; k++)
                {
                    if (t[k] == null) continue;
                    list.Add(new ScanTile(previousPlayerOne[i].chance, t[k]));
                }
            }
            for (int i = 0; i < list.Count; i++)
            {
                bool check = false;
                for (int k = 0; k < previousPlayerOne.Count; k++)
                {
                    if (previousPlayerOne[k].tile == list[i].tile)
                    {
                        previousPlayerOne[k].chance += list[i].chance;
                        check = true;
                        break;
                    }
                }
                if (!check)
                {
                    previousPlayerOne.Add(list[i]);
                }
            }
        }
        else if (playerTwoID == id)
        {
            List<ScanTile> list = new List<ScanTile>();
            for (int i = 0; i < previousPlayerTwo.Count; i++)
            {
                Tile[] t = GridMaster.instnace.GetAllTilesAround(previousPlayerTwo[i].tile, true);
                for (int k = 0; k < t.Length; k++)
                {
                    if (t[k] == null) continue;
                    list.Add(new ScanTile(previousPlayerTwo[i].chance, t[k]));
                }
            }
            for (int i = 0; i < list.Count; i++)
            {
                bool check = false;
                for (int k = 0; k < previousPlayerTwo.Count; k++)
                {
                    if (previousPlayerTwo[k].tile == list[i].tile)
                    {
                        previousPlayerTwo[k].chance += list[i].chance;
                        check = true;
                        break;
                    }
                }
                if (!check)
                {
                    previousPlayerTwo.Add(list[i]);
                }
            }
        }
    }
    public void MotionTwo(Tile[] t)
    {
        List<ScanTile> temp = new List<ScanTile>();
        for (int i = 0; i < previousPlayerOne.Count; i++)
        {
            for (int k = 0; k < t.Length; k++)
            {
                if (t[k].cord == previousPlayerOne[i].tile.cord)
                {
                    temp.Add(previousPlayerOne[i]);
                }
            }
        }
        previousPlayerOne.Clear();
        previousPlayerOne.AddRange(temp);
        List<ScanTile> temp2 = new List<ScanTile>();
        for (int i = 0; i < previousPlayerTwo.Count; i++)
        {
            for (int k = 0; k < t.Length; k++)
            {
                if (t[k].cord == previousPlayerTwo[i].tile.cord)
                {
                    temp2.Add(previousPlayerTwo[i]);
                }
            }
        }
        previousPlayerTwo.Clear();
        previousPlayerTwo.AddRange(temp2);
    }
    public void MotionOne(Tile[] t)
    {
        bool check = false;
        bool check2 = false;
        for (int i = 0; i < previousPlayerOne.Count; i++)
        {
            for (int k = 0; k < t.Length; k++)
            {
                if (previousPlayerOne[i].tile == t[k])
                {
                    check = true;
                    break;
                }
            }
            if (check) break;
        }
        for (int i = 0; i < previousPlayerTwo.Count; i++)
        {
            for (int k = 0; k < t.Length; k++)
            {
                if (previousPlayerTwo[i].tile == t[k])
                {
                    check2 = true;
                    break;
                }
            }
            if (check2) break;
        }
        if(!check)
        {
            List<ScanTile> temp = new List<ScanTile>();
            for (int i = 0; i < previousPlayerOne.Count; i++)
            {
                for (int k = 0; k < t.Length; k++)
                {
                    if(previousPlayerOne[i].tile == t[k])
                    {
                        temp.Add(previousPlayerOne[i]);
                        break;
                    }
                }
            }
            previousPlayerOne.Clear();
            previousPlayerOne.AddRange(temp);
        }else if(!check2)
        {
            List<ScanTile> temp = new List<ScanTile>();
            for (int i = 0; i < previousPlayerTwo.Count; i++)
            {
                for (int k = 0; k < t.Length; k++)
                {
                    if (previousPlayerTwo[i].tile == t[k])
                    {
                        temp.Add(previousPlayerTwo[i]);
                        break;
                    }
                }
            }
            previousPlayerTwo.Clear();
            previousPlayerTwo.AddRange(temp);
        }
    }
    public void MotionNone(Tile[] t)
    {
        for (int i = 0; i < t.Length; i++)
        {
            RemoveTile(t[i]);
        }
    }
    public int GetTileScore(Tile t)
    {
        int amount = 0;
        for (int i = 0; i < previousPlayerTwo.Count; i++)
        {
            if(previousPlayerTwo[i].tile == t)
            {
                amount += (int)previousPlayerTwo[i].chance;
            }
        }
        for (int i = 0; i < previousPlayerOne.Count; i++)
        {
            if (previousPlayerOne[i].tile == t)
            {
                amount += (int)previousPlayerOne[i].chance;
            }
        }
        return amount;
    }
    void ParachuteScan(Tile[] tiles)
    {
        NormalScan(tiles);
    }
    void NormalScan(Tile[] tiles)
    {
        SetPlayerIDs();
        int id = PlayerMaster.instnace.GetPlayersID();
        if(id == playerOneID)
        {
            ScanShrinkFinished(GetFirstPlayerTemp(tiles));
        }
        else
        {
            ScanShrinkFinished(GetSecondPlayerTemp(tiles));
        }
    }
    List<Tile> ZiplineScan(Tile[] tiles)
    {
        List<ScanTile> scanTile = GetPlayerScanTiles();
        List<Tile> zipLineList = new List<Tile>();
        if (scanTile.Count > 0)
        {
            for (int i = 0; i < scanTile.Count; i++)
            {
                if(scanTile[i].tile.cord.x - 1 < 0)
                {
                    for (int k = 0; k < tiles.Length; k++)
                    {
                        if(tiles[k].cord.x == 3)
                        {
                            zipLineList.Add(tiles[k]);
                        }
                    }
                }
                if (scanTile[i].tile.cord.x + 1 > 3)
                {
                    for (int k = 0; k < tiles.Length; k++)
                    {
                        if (tiles[k].cord.x == 0)
                        {
                            zipLineList.Add(tiles[k]);
                        }
                    }
                }
                if (scanTile[i].tile.cord.y - 1 < 0)
                {
                    for (int k = 0; k < tiles.Length; k++)
                    {
                        if (tiles[k].cord.y == 3)
                        {
                            zipLineList.Add(tiles[k]);
                        }
                    }
                }
                if (scanTile[i].tile.cord.y + 1 > 3)
                {
                    for (int k = 0; k < tiles.Length; k++)
                    {
                        if (tiles[k].cord.y == 0)
                        {
                            zipLineList.Add(tiles[k]);
                        }
                    }
                }
            }
            
        }
        return zipLineList;
    }
    List<Tile> TunnelScan(Tile[] tiles)
    {

        List<ScanTile> scanTile = GetPlayerScanTiles();
        List<Tile> temp = new List<Tile>();
        temp.AddRange(tiles);
        List<Tile> zipLineList = new List<Tile>();
        if (scanTile.Count > 0)
        {
            Cord[] allCords = new Cord[GridMaster.instnace.allTiles.Count];
            for (int i = 0; i < allCords.Length; i++)
            {
                allCords[i] = GridMaster.instnace.allTiles[i].cord;
            }
            WallCord[] allWalls = new WallCord[GridMaster.instnace.allWalls.Count];
            for (int i = 0; i < allWalls.Length; i++)
            {
                allWalls[i] = GridMaster.instnace.allWalls[i].cord;
            }
            for (int i = 0; i < tiles.Length; i++)
            {
                bool check = false;
                Cord left = new Cord(tiles[i].cord.x - 1, tiles[i].cord.y);
                Cord right = new Cord(tiles[i].cord.x + 1, tiles[i].cord.y);
                Cord up = new Cord(tiles[i].cord.x, tiles[i].cord.y + 1);
                Cord down = new Cord(tiles[i].cord.x, tiles[i].cord.y - 1);
                for (int k = 0; k < scanTile.Count; k++)
                {
                    if ((scanTile[k].tile.cord == left && !Pathing.CheckRight(allCords, allWalls, scanTile[k].tile.cord)))
                    {
                        check = true;
                        break;
                    }
                    if (scanTile[k].tile.cord == right && !Pathing.CheckLeft(allCords, allWalls, scanTile[k].tile.cord))
                    {
                        check = true;
                        break;
                    }
                    if (scanTile[k].tile.cord == up && !Pathing.CheckDown(allCords, allWalls, scanTile[k].tile.cord))
                    {
                        check = true;
                        break;
                    }
                    if (scanTile[k].tile.cord == down && !Pathing.CheckUp(allCords, allWalls, scanTile[k].tile.cord))
                    {
                        check = true;
                        break;
                    }
                }
                if (!check)
                {
                    temp.Remove(tiles[i]);
                }
            }

        }
        if(temp.Count < 1)
        {
            return new List<Tile>(tiles);
        }
        return temp;
    }
    List<Tile> TeleportScan(Tile[] tiles)
    {
        return new List<Tile>(tiles);
    }
    List<Tile> BeaconScan(Tile[] tiles)
    {
        return new List<Tile>(tiles);
    }
    void ThroughWallsScan(Tile[] tiles)
    {
        if (PlayerMaster.instnace.playerOne)
        {
            ScanShrinkFinished(GetFirstPlayerTemp(tiles));
        }
        else
        {
            ScanShrinkFinished(GetSecondPlayerTemp(tiles));
        }
    }
    void SetPlayerIDs()
    {
        if (playerTwoID == 0)
        {
            playerTwoID = PlayerMaster.instnace.players[1].playerID;
            playerOneID = PlayerMaster.instnace.players[0].playerID;
        }
    }
    void ScanShrinkFinished(List<Tile> temp)
    {
        SetPlayerIDs();
        if(PlayerMaster.instnace.playerOne)
        {
            if (PlayerMaster.instnace.players[0].playerID == playerOneID)
            {
                ScanTile[] thermalTiles = previousPlayerOne.ToArray();
                for (int i = 0; i < thermalTiles.Length; i++)
                {
                    if (!thermalTiles[i].thermalCheck)
                    {
                        previousPlayerOne.Remove(thermalTiles[i]);
                    }
                }
            }
            else if(PlayerMaster.instnace.players[0].playerID == playerTwoID)
            {
                ScanTile[] thermalTiles = previousPlayerOne.ToArray();
                for (int i = 0; i < thermalTiles.Length; i++)
                {
                    if (!thermalTiles[i].thermalCheck)
                    {
                        previousPlayerTwo.Remove(thermalTiles[i]);
                    }
                }
            }
        }
        else
        {
            if (PlayerMaster.instnace.players[1].playerID == playerOneID)
            {
                ScanTile[] thermalTiles = previousPlayerOne.ToArray();
                for (int i = 0; i < thermalTiles.Length; i++)
                {
                    if (!thermalTiles[i].thermalCheck)
                    {
                        previousPlayerOne.Remove(thermalTiles[i]);
                    }
                }
            }
            else if (PlayerMaster.instnace.players[1].playerID == playerTwoID)
            {
                ScanTile[] thermalTiles = previousPlayerOne.ToArray();
                for (int i = 0; i < thermalTiles.Length; i++)
                {
                    if (!thermalTiles[i].thermalCheck)
                    {
                        previousPlayerTwo.Remove(thermalTiles[i]);
                    }
                }
            }
        }
        float per = 100 / temp.Count;
        for (int i = 0; i < temp.Count; i++)
        {
            if (PlayerMaster.instnace.playerOne)
            {
                if (PlayerMaster.instnace.players[0].playerID == playerOneID)
                {
                    previousPlayerOne.Add(new ScanTile(per, temp[i]));
                }
                else if (PlayerMaster.instnace.players[0].playerID == playerTwoID)
                {
                    previousPlayerTwo.Add(new ScanTile(per, temp[i]));
                }
            }
            else
            {
                if (PlayerMaster.instnace.players[1].playerID == playerOneID)
                {
                    previousPlayerOne.Add(new ScanTile(per, temp[i]));
                }
                else if (PlayerMaster.instnace.players[1].playerID == playerTwoID)
                {
                    previousPlayerTwo.Add(new ScanTile(per, temp[i]));
                }
            }
            //Debug.Log("Tile: " + temp[i].cord.ToString() + " Chance: " + per);
        }
    }
    List<ScanTile> GetPlayerScanTiles()
    {
        if(PlayerMaster.instnace.playerOne)
        {
            return previousPlayerOne;
        }
        else
        {
            return previousPlayerTwo;
        }
    }
    List<Tile> GetFirstPlayerTemp(Tile[] tiles)
    {
        List<Tile> temp = new List<Tile>();
        temp.AddRange(tiles);
        if (previousPlayerOne.Count > 0)
        {
            if (GameMaster.instance.currentUnlock != GameUnlock.Tier0)
            {
                ScanTile[] tl = new ScanTile[0];
                tl = previousPlayerOne.ToArray();
                for (int i = 0; i < tl.Length; i++)
                {
                    if (tl[i].thermalCheck)
                    {
                        previousPlayerOne.Remove(tl[i]);
                    }
                }
                for (int i = 0; i < previousPlayerOne.Count; i++)
                {
                    previousPlayerOne[i].thermalCheck = true;
                    previousPlayerOne[i].chance /= thermalDivde;
                }
            }
            Cord[] allCords = new Cord[GridMaster.instnace.allTiles.Count];
            for (int i = 0; i < allCords.Length; i++)
            {
                allCords[i] = GridMaster.instnace.allTiles[i].cord;
            }
            WallCord[] allWalls = new WallCord[GridMaster.instnace.allWalls.Count];
            for (int i = 0; i < allWalls.Length; i++)
            {
                allWalls[i] = GridMaster.instnace.allWalls[i].cord;
            }
            for (int i = 0; i < tiles.Length; i++)
            {
                bool check = false;
                Cord left = new Cord(tiles[i].cord.x - 1, tiles[i].cord.y);
                Cord right = new Cord(tiles[i].cord.x + 1, tiles[i].cord.y);
                Cord up = new Cord(tiles[i].cord.x, tiles[i].cord.y + 1);
                Cord down = new Cord(tiles[i].cord.x, tiles[i].cord.y - 1);
                for (int k = 0; k < previousPlayerOne.Count; k++)
                {
                    if (previousPlayerOne[k].tile.cord == left && Pathing.CheckRight(allCords, allWalls, previousPlayerOne[k].tile.cord))
                    {
                        check = true;
                        break;
                    }
                    if (previousPlayerOne[k].tile.cord == right && Pathing.CheckLeft(allCords, allWalls, previousPlayerOne[k].tile.cord))
                    {
                        check = true;
                        break;
                    }
                    if (previousPlayerOne[k].tile.cord == up && Pathing.CheckDown(allCords, allWalls, previousPlayerOne[k].tile.cord))
                    {
                        check = true;
                        break;
                    }
                    if (previousPlayerOne[k].tile.cord == down && Pathing.CheckUp(allCords, allWalls, previousPlayerOne[k].tile.cord))
                    {
                        check = true;
                        break;
                    }
                }
                if (!check)
                {
                    temp.Remove(tiles[i]);
                }
            }
        }
        if (temp.Count == 0)
        {
            if (tiles.Length < 1) return null;
            return new List<Tile>(tiles);
        }
        return temp;
    }
    List<Tile> GetSecondPlayerTemp(Tile[] tiles)
    {
        List<Tile> temp = new List<Tile>();
        temp.AddRange(tiles);
        if (previousPlayerTwo.Count > 0)
        {
            if (GameMaster.instance.currentUnlock != GameUnlock.Tier0)
            {
                ScanTile[] tl = new ScanTile[0];
                tl = previousPlayerTwo.ToArray();
                for (int i = 0; i < tl.Length; i++)
                {
                    if (tl[i].thermalCheck)
                    {
                        previousPlayerTwo.Remove(tl[i]);
                    }
                }
                for (int i = 0; i < previousPlayerTwo.Count; i++)
                {
                    previousPlayerTwo[i].thermalCheck = true;
                    previousPlayerTwo[i].chance /= thermalDivde;
                }
            }
            Cord[] allCords = new Cord[GridMaster.instnace.allTiles.Count];
            for (int i = 0; i < allCords.Length; i++)
            {
                allCords[i] = GridMaster.instnace.allTiles[i].cord;
            }
            WallCord[] allWalls = new WallCord[GridMaster.instnace.allWalls.Count];
            for (int i = 0; i < allWalls.Length; i++)
            {
                allWalls[i] = GridMaster.instnace.allWalls[i].cord;
            }
            for (int i = 0; i < tiles.Length; i++)
            {
                bool check = false;
                Cord left = new Cord(tiles[i].cord.x - 1, tiles[i].cord.y);
                Cord right = new Cord(tiles[i].cord.x + 1, tiles[i].cord.y);
                Cord up = new Cord(tiles[i].cord.x, tiles[i].cord.y + 1);
                Cord down = new Cord(tiles[i].cord.x, tiles[i].cord.y - 1);
                for (int k = 0; k < previousPlayerTwo.Count; k++)
                {
                    if (previousPlayerTwo[k].tile.cord == left && Pathing.CheckRight(allCords, allWalls, previousPlayerTwo[k].tile.cord))
                    {
                        check = true;
                        break;
                    }
                    if (previousPlayerTwo[k].tile.cord == right && Pathing.CheckLeft(allCords, allWalls, previousPlayerTwo[k].tile.cord))
                    {
                        check = true;
                        break;
                    }
                    if (previousPlayerTwo[k].tile.cord == up && Pathing.CheckDown(allCords, allWalls, previousPlayerTwo[k].tile.cord))
                    {
                        check = true;
                        break;
                    }
                    if (previousPlayerTwo[k].tile.cord == down && Pathing.CheckUp(allCords, allWalls, previousPlayerTwo[k].tile.cord))
                    {
                        check = true;
                        break;
                    }
                }
                if (!check)
                {
                    temp.Remove(tiles[i]);
                }
            }
        }
        if (temp.Count == 0)
        {
            if (tiles.Length < 1) return null;
            return new List<Tile>(tiles);
        }
        return temp;
    }
    public float GetTileChance(Tile t)
    {
        float count = 0;
        for (int i = 0; i < previousPlayerOne.Count; i++)
        {
            if(previousPlayerOne[i].tile.cord == t.cord)
            {
                count += previousPlayerOne[i].chance;
            }
        }
        for (int i = 0; i < previousPlayerTwo.Count; i++)
        {
            if (previousPlayerTwo[i].tile.cord == t.cord)
            {
                count += previousPlayerTwo[i].chance;
            }
        }
        return count;
    }
    public Tile GetBestTileChance(Tile[] t)
    {
        float temp = float.MinValue;
        int pos = 0;
        for (int i = 0; i < t.Length; i++)
        {
            float per = GetTileChance(t[i]);
            if(per > temp)
            {
                temp = per;
                pos = i;
            }
        }
        return t[pos];
    }
}
[Serializable]
public class ScanTile
{
    public float chance;
    public Tile tile;
    public bool thermalCheck;

    public ScanTile(float c, Tile t)
    {
        chance = c;
        tile = t;
    }
}
