﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
public class ConfirmButtons : MonoBehaviour
{
    public UnityEvent unityEvent;
    public UnityEvent firstPressEvent;
    [SerializeField]
    Image img;
    [SerializeField]
    TextMeshProUGUI mainText;
    [SerializeField]
    Sprite confirmSpr;
    [SerializeField]
    Sprite oldSpr;
    public bool setup;
    public List<ConfirmButtons> pairedButtons = new List<ConfirmButtons>();
    public System.Action<ConfirmButtons> EventPairedButton;
    private void Awake()
    {
        Init();
    }
    private void Start()
    {
        oldSpr = img.sprite;
        SetupPaired();
    }
    void Init()
    {
        if (img == null)
        {
            img = GetComponent<Image>();
        }
        if (mainText == null)
        {
            mainText = GetComponentInChildren<TextMeshProUGUI>();
        }
        if(mainText == null)
        {
            Debug.Log("I dont have a TextMeshPro " + gameObject.name);
        }
    }
    public void Setup(UnityAction firstPress, UnityAction confirmPress, string text = "", Sprite spr = null)
    {
        firstPressEvent.RemoveAllListeners();
        unityEvent.RemoveAllListeners();
        firstPressEvent.AddListener(firstPress);
        unityEvent.AddListener(confirmPress);
        Init();
        if (text != "")
        {
            mainText.text = text;
        }
        if(spr != null)
        {
            confirmSpr = spr;
        }
        setup = true;
    }

    bool confirmed;
    public void ButtonPressed()
    {
        if(confirmed)
        {
            unityEvent.Invoke();
            ClearConfirm();
            return;
        }
        else
        {
            confirmed = true;
            if (firstPressEvent == null)
            {
                firstPressEvent.Invoke();
            }
            EventPairedButton?.Invoke(this);
            img.sprite = confirmSpr;
            mainText.enabled = false;
        }
    }
    public void ClearConfirm()
    {
        if (mainText == null)
        {
            Init();
        }
        confirmed = false;
        mainText.enabled = true;
        img.sprite = oldSpr;
    }
    void SetupPaired()
    {
        if (pairedButtons.Count > 0 && !pairedButtons.Contains(this))
        {
            pairedButtons.Add(this);
        }
        for (int i = 0; i < pairedButtons.Count; i++)
        {
            pairedButtons[i].EventPairedButton += PairedButtonsClear;
        }
    }
    public void PairedButtonsClear(ConfirmButtons b)
    {
        for (int i = 0; i < pairedButtons.Count; i++)
        {
            if(b != pairedButtons[i])
            {
                pairedButtons[i].ClearConfirm();
            }
        }
    }
}
