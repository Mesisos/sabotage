﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VillianDifficulty : MonoBehaviour
{
    [SerializeField]
    GameObject hard;
    [SerializeField]
    GameObject easy;

    public void Toggle(bool b)
    {
        hard.SetActive(!hard.activeInHierarchy);
        easy.SetActive(!easy.activeInHierarchy);
    }
}
