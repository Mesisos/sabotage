﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Phase
{
    Dice,
    Guard,
    Spy,
}
public enum GameUnlock
{
    Tier0,
    Tier1,
    Tier2,
    Tier3,
}
public enum Difficulty
{
    Easy,
    Hard
}
public class GameMaster : MonoBehaviour {
    public static GameMaster instance;

    private void Awake()
    {
        instance = this;
    }

    public Phase currentPhase;
    public GameUnlock currentUnlock;
    public Difficulty difficulty;
    public System.Action<Phase> PhaseChanged;

    public void ChangePhase(Phase p)
    {
        currentPhase = p;
        if(PhaseChanged != null)
        {
            PhaseChanged(p);
        }
    }
    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }
    public void SetDifficulty(bool b)
    {
        if(!b)
        {
            difficulty = Difficulty.Hard;
        }
        else
        {
            difficulty = Difficulty.Easy;
        }
    }
}
