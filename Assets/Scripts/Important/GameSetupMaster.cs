﻿using Doozy.Engine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSetupMaster : MonoBehaviour {
    public static GameSetupMaster instnace;

    private void Awake()
    {
        instnace = this;
    }


    [SerializeField]
    UIView tierParent;
    [SerializeField]
    UIView choosePlayersParent;
    [SerializeField]
    UIView chooseGuardParent;
    [SerializeField]
    GameObject startButton;
    UIView[] parents;
    [SerializeField]
    GameObject[] classButtons;
    [SerializeField]
    GameObject[] guardChooseButtons;
    private void Start()
    {
        parents = new UIView[3];
        parents[0] = tierParent;
        parents[1] = choosePlayersParent;
        parents[2] = chooseGuardParent;
        phase = -1;
    }
    public void StartSetup()
    {
        startButton.SetActive(false);
        NextPhase();
    }
    public void TierChoose(int pos)
    {
        GameMaster.instance.currentUnlock = (GameUnlock)pos;
        HackMaster.instnace.mainText.text = "Hacks: " + 0 + "/" + (5 + (int)GameMaster.instance.currentUnlock);
        FireMaster.instnace.hitText.text = "Hits: " + 0 + "/" + ((GameMaster.instance.currentUnlock == GameUnlock.Tier0 ? 1 : 3) + 2);
        NextPhase();
    }
    bool classChoose = false;
    public void ClassChosen(string name)
    {
        switch(name)
        {
            case "Hacker":
                classButtons[0].gameObject.SetActive(false);
                break;
            case "Hawk":
                classButtons[1].gameObject.SetActive(false);
                break;
            case "Acrobat":
                classButtons[2].gameObject.SetActive(false);
                break;
            case "Juicer":
                classButtons[3].gameObject.SetActive(false);
                break;
        }
        PlayerMaster.instnace.AddPlayer(name);
        if(!classChoose)
        {
            classChoose = true;
        }
        else
        {
            NextPhase();
        }
    }
    bool guardChosen;
    public void GuardChosen(string name)
    {
        switch (name)
        {
            case "Chantico":
                guardChooseButtons[0].gameObject.SetActive(false);
                break;
            case "Nadesh":
                guardChooseButtons[1].gameObject.SetActive(false);
                break;
            case "Ophidian":
                guardChooseButtons[2].gameObject.SetActive(false);
                break;
            case "Count V":
                guardChooseButtons[3].gameObject.SetActive(false);
                break;
        }
        GuardMaster.instance.AddGuard(name);
        if (!guardChosen)
        {
            guardChosen = true;
        }
        else
        {
            NextPhase();
        }
    }
    public void RandomGuard()
    {
        if(guardChosen)
        {
            List<string> otherGuards = new List<string>();
            otherGuards.Add("Chantico");
            otherGuards.Add("Ophidian");
            otherGuards.Add("Nadesh");
            otherGuards.Add("Count V");
            otherGuards.Remove(GuardMaster.instance.guard[0].guardName);
            GuardChosen(otherGuards[UnityEngine.Random.Range(0, otherGuards.Count)]);
        }
        else
        {
            string[] g = new []
            {
               "Chantico",
               "Nadesh",
               "Ophidian",
               "Count V"
            };
            GuardChosen(g[UnityEngine.Random.Range(0, g.Length)]);
        }
    }
    int phase;
    void NextPhase()
    {
        phase++;
        for (int i = 0; i < parents.Length; i++)
        {
            if(phase == i)
            {
                parents[i].Show();
            }
            else
            {
                parents[i].Hide();
            }
        }
        if (phase >= parents.Length)
        {
            PhaseMaster.instance.StartGame();
        }
    }
}
