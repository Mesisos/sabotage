﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine;
using Doozy.Engine.Soundy;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MusicPlayer : MonoBehaviour
{
    public static MusicPlayer instance;
    private void Awake()
    {
        instance = this;
    }
    [SerializeField]
    AudioClip[] music;
    SoundyController controller;
    [SerializeField]
    Toggle toggle;
    [SerializeField]
    Toggle toggleFX;
    [SerializeField]
    AudioMixerGroup fx;
    [SerializeField]
    AudioMixerGroup musicGroup;
    [SerializeField]
    Slider musicSlider;
    [SerializeField]
    Slider fxSlider;
    private void Start()
    {
        SaveSound s = SaveSound.Load();
        if (s == null) return;
        saveSound = s;
        toggle.isOn = s.mute;
        toggleFX.isOn = s.muteSoundEffects;
        fx.audioMixer.SetFloat("Volume", saveSound.fxVolume);
        musicGroup.audioMixer.SetFloat("VolumeMusic", saveSound.musicVolume);
        fxSlider.value = saveSound.fxVolume;
        musicSlider.value = saveSound.musicVolume;
        Mute(s.mute);
    }
    public void PlaySound(AudioClip c)
    {
        SaveSound s = SaveSound.Load();
        if(s != null  && s.muteSoundEffects)
        {
            return;
        }
        if (c == null)
        {
            Debug.Log("Null Sound to Play");
            return;
        }
        SoundyManager.Play(c,fx);
    }
    void PlayNextSong()
    {
        if (music.Length < 1) return;
         controller = SoundyManager.Play(music[UnityEngine.Random.Range(0, music.Length)], musicGroup);
        SaveSound s = SaveSound.Load();
        if(s != null)
        {
            if(s.mute)
            {
                controller.Mute();
                controller.Pause();
            }
        }
    }
    private void Update()
    {
        if(controller == null || controller.PlayProgress >= .99f)
        {
            PlayNextSong();
        }
    }
    public void MuteSoundFX(bool b)
    {
        SaveSound s = SaveSound.Load();
        if (s == null)
        {
            SaveSound.Save(new SaveSound(false, b));
        }
        else
        {
            s.muteSoundEffects = b;
            SaveSound.Save(s);
        }
    }
    public void Mute(bool b)
    {
        SaveSound s = SaveSound.Load();
        if (s == null)
        {
            SaveSound.Save(new SaveSound(b, false));
        }
        else
        {
            s.mute = b;
            SaveSound.Save(s);
        }
        if (controller == null) return;
        if(b)
        {
            controller.Mute();
            controller.Pause();
        }
        else
        {
            controller.Unmute();
            controller.Unpause();
        }
    }
    SaveSound saveSound;
    [SerializeField]
    AudioClip fxVolumeCheck;
    public void SetFXVolume(float f)
    {
        if (saveSound == null)
        {
            saveSound = new SaveSound(false, false);
        }
        if(f < -19)
        {
            f = -80;
        }
        fx.audioMixer.SetFloat("Volume", f);
        saveSound.fxVolume = f;
        SaveSound.Save(saveSound);
        PlaySound(fxVolumeCheck);
    }
    public void SetMusicVolume(float f)
    {
        if(saveSound == null)
        {
            saveSound = new SaveSound(false, false);
        }
        if (f < -19)
        {
            f = -80;
        }
        musicGroup.audioMixer.SetFloat("VolumeMusic", f);
        saveSound.musicVolume = f;
        SaveSound.Save(saveSound);
    }
    [System.Serializable]
    class SaveSound
    {
        [System.NonSerialized]
        static string SaveDir = "/Save";
        [System.NonSerialized]
        static string SavePath = "/MusicSave.sav";
        public bool mute;
        public bool muteSoundEffects;
        public float fxVolume;
        public float musicVolume;
        public SaveSound(bool b, bool soundFX)
        {
            mute = b;
            muteSoundEffects = soundFX;
        }
        public static void Save(SaveSound s)
        {
            if (!System.IO.Directory.Exists(Application.persistentDataPath + SaveDir))
            {
                System.IO.Directory.CreateDirectory(Application.persistentDataPath + SaveDir);
            }
            if (File.Exists(Application.persistentDataPath + SaveDir + SavePath))
            {
                File.Delete(Application.persistentDataPath + SaveDir + SavePath);
            }
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + SaveDir + SavePath);
            bf.Serialize(file, s);
            file.Close();
        }
        public static SaveSound Load(byte[] path)
        {
            MemoryStream memStream = new MemoryStream();
            BinaryFormatter binForm = new BinaryFormatter();

            memStream.Write(path, 0, path.Length);
            memStream.Seek(0, SeekOrigin.Begin);

            SaveSound obj = (SaveSound)binForm.Deserialize(memStream);

            return obj;
        }
        public static SaveSound Load()
        {
            if (File.Exists(Application.persistentDataPath + SaveDir + SavePath))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + SaveDir + SavePath, FileMode.Open);
                SaveSound s = (SaveSound)bf.Deserialize(file);
                file.Close();
                return s;
            }
            else
            {
                return null;
            }
        }
        public static void Delete()
        {
            if (File.Exists(Application.persistentDataPath + SaveDir + SavePath))
            {
                File.Delete(Application.persistentDataPath + SaveDir + SavePath);
            }
        }
    }
}
