﻿using Doozy.Engine.UI;
using UnityEngine;

public class WallKiller : MonoBehaviour {
    public static WallKiller instnace;

    private void Awake()
    {
        instnace = this;
    }

    public void KillWall(Tile start, Tile end)
    {
        if(start.cord.x == end.cord.x && start.cord.y > end.cord.y)
        {
            if(GridMaster.instnace.GetWall(end, WallPos.Up) == null)
            {
                return;
            }
            GridMaster.instnace.DestroyWall(end, WallPos.Up);
        }
        if(start.cord.x == end.cord.x && start.cord.y < end.cord.y)
        {
            if (GridMaster.instnace.GetWall(start, WallPos.Up) == null)
            {
                return;
            }
            GridMaster.instnace.DestroyWall(start, WallPos.Up);
        }
        if (start.cord.x < end.cord.x && start.cord.y == end.cord.y)
        {
            if (GridMaster.instnace.GetWall(start, WallPos.Right) == null)
            {
                return;
            }
            GridMaster.instnace.DestroyWall(start, WallPos.Right);
        }
        if (start.cord.x > end.cord.x && start.cord.y == end.cord.y)
        {
            if (GridMaster.instnace.GetWall(end, WallPos.Right) == null)
            {
                return;
            }
            GridMaster.instnace.DestroyWall(end, WallPos.Right);
        }
        CreatePopup("Wall Destroyed between " + start.letter + " and " + end.letter);
    }
    void CreatePopup(string g)
    {
        UIPopup ui = UIPopupManager.GetPopup("WallKiller");
        ui.Data.SetLabelsTexts(g);
        UIPopupManager.AddToQueue(ui);
    }
}
