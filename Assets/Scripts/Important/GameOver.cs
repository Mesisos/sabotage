﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameOver : MonoBehaviour {
    public static GameOver instance;

    private void Awake()
    {
        instance = this;
    }
    [SerializeField]
    GameObject parent;
    [SerializeField]
    TextMeshProUGUI endText;
    private void Start()
    {
        parent.gameObject.SetActive(false);
    }
    [SerializeField]
    AudioClip playerWin;
    [SerializeField]
    AudioClip villiansWin;
    public void GameFinished(bool playersWin)
    {
        PhaseMaster.instance.GameOver();
        PlayerMaster.instnace.EndPhase();
        GuardMaster.instance.EndPhase();
        DiceMaster.instnace.EndPhase();
        parent.gameObject.SetActive(true);
        MusicPlayer.instance.PlaySound(playersWin ? playerWin : villiansWin);
        endText.text = playersWin ? "Spies Win" : "Villians Win";
        for (int i = 0; i < GuardMaster.instance.guard.Length; i++)
        {
            GuardMaster.instance.guard[i].Reveal();
        }
    }
}
