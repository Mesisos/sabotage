﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhaseBase : MonoBehaviour {
    
    public virtual void StartPhase()
    {

    }
    public virtual void EndPhase()
    {
        PhaseMaster.instance.NextPhase();
    }
}
