﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconMaster : MonoBehaviour {
    public static IconMaster instnace;

    private void Awake()
    {
        instnace = this;
    }
    public Sprite doomTile;
    public Sprite genTile;
    public Sprite[] dice;
    public Sprite hawk;
    public Sprite acrobat;
    public Sprite hacker;
    public Sprite jucier;
    public Sprite cube;

    public Sprite taser;
    public Sprite flameThrower;
    public Sprite freezeRay;
    public Sprite orbital;
    public Sprite strobe;
    public Sprite flashlight;
    public Sprite miniGun;
    public Sprite boomStick;
    public Sprite c4;
    public Sprite doppler;
    public Sprite grenade;
    public Sprite spotlight;

    public Sprite PlayerNameToSprite(string player)
    {
        switch(player)
        {
            case "Juicer":
                return jucier;
            case "Hacker":
                return hacker;
            case "Hawk":
                return hawk;
            case "Acrobat":
                return acrobat;
        }
        Debug.Log("Failed to Find Player Sprite");
        return null;
    }
    public Sprite ItemNameToSprite(string name)
    {
        switch(name)
        {
            case "Stun Gun":
                return taser;
            case "Flame Thrower":
                return flameThrower;
            case "Freeze Ray":
                return freezeRay;
            case "Orbital Laser":
                return orbital;
            case "Flare":
                return strobe;
            case "Flashlight":
                return flashlight;
            case "Land Shark":
                return miniGun;
            case "Tentacles":
                return boomStick;
            case "T.N.T.":
                return c4;
            case "Doppler":
                return doppler;
            case "Rocket Launcher":
                return grenade;
            case "X-Ray Goggles":
                return spotlight;
        }
        Debug.LogError("Cannot find " + name + " Sprite");
        return null;
    }
    public AudioClip tasers;
    public AudioClip flameThrowers;
    public AudioClip freezeRays;
    public AudioClip orbitals;
    public AudioClip strobes;
    public AudioClip flashlights;
    public AudioClip miniGuns;
    public AudioClip boomSticks;
    public AudioClip c4s;
    public AudioClip dopplers;
    public AudioClip grenades;
    public AudioClip spotlights;

    public AudioClip ItemNameToSound(string name)
    {
        switch (name)
        {
            case "Stun Gun":
                return tasers;
            case "Flame Thrower":
                return flameThrowers;
            case "Freeze Ray":
                return freezeRays;
            case "Orbital Laser":
                return orbitals;
            case "Flare":
                return strobes;
            case "Flashlight":
                return flashlights;
            case "Land Shark":
                return miniGuns;
            case "Tentacles":
                return boomSticks;
            case "T.N.T.":
                return c4s;
            case "Doppler":
                return dopplers;
            case "Rocket Launcher":
                return grenades;
            case "X-Ray Goggles":
                return spotlights;
        }
        Debug.LogError("Cannot find " + name + " Sprite");
        return null;
    }

    public AudioClip buttonClick;
    public AudioClip buttonCancel;
}
