﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhaseMaster : MonoBehaviour {
    public static PhaseMaster instance;

    private void Awake()
    {
        instance = this;
    }
    public System.Action EventNewRound;
    [SerializeField]
    PhaseBase[] phaseBase;
    bool gameOver;
    public void StartGame()
    {
        StartPhase();
    }
    [ContextMenu("Next Phase")]
    public void NextPhase()
    {
        if (gameOver) return;
        Phase p = GameMaster.instance.currentPhase;
        if(p == Phase.Spy)
        {
            p = Phase.Dice;
            if(EventNewRound != null)
            {
                EventNewRound();
            }
        }
        else
        {
            p = (Phase)((int)p + 1);
        }
        GameMaster.instance.ChangePhase(p);
        StartPhase();
    }
    public void StartPhase()
    {
        phaseBase[(int)GameMaster.instance.currentPhase].StartPhase();
    }
    public void GameOver()
    {
        gameOver = true;
    }
}
