﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchMaster : MonoBehaviour {
    public static TouchMaster instance;

    public System.Action<TwoClickButton> ClearTwoClick;
    private void Awake()
    {
        instance = this;
    }
    private void LateUpdate()
    {
        if(Input.GetMouseButtonDown(0))
        {
            CheckTwoClick(GetMouseResults());
        }
        if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            CheckTwoClick(GetTouchResults(0));
        }
    }
    void CheckTwoClick(RaycastResult[] r)
    {
        for (int i = 0; i < r.Length; i++)
        {
            TwoClickButton t = r[i].gameObject.GetComponent<TwoClickButton>();
            if(t != null)
            {
                ClearClicks(t);
                return;
            }
        }
        ClearClicks(null);
    }
    RaycastResult[] GetMouseResults()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.ToArray();
    }
    /// <summary>
    /// Requires atleast one Touch to be Active.
    /// </summary>
    /// <param name="touchPos"></param>
    /// <returns></returns>
    RaycastResult[] GetTouchResults(int touchPos = 0)
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.GetTouch(touchPos).position.x, Input.GetTouch(touchPos).position.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.ToArray();
    }
    void ClearClicks(TwoClickButton p)
    {
        if(ClearTwoClick != null)
        {
            ClearTwoClick(p);
        }
    }
}
