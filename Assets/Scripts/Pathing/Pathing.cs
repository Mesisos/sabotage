﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathing {

    public static Tile[] FindClosestTile(Tile currentTile, Tile[] targets, Tile[] allTiles, WallCord[] walls)
    {
        List<Tile[]> list = new List<Tile[]>();
        for (int i = 0; i < targets.Length; i++)
        {
            list.Add(PathFindReturned(currentTile, targets[i], allTiles, walls));
        }
        int pos = 0;
        int amount = int.MaxValue;
        for (int i = 0; i < list.Count; i++)
        {
            if(list[i].Length > 0 && list[i].Length < amount)
            {
                pos = i;
                amount = list[i].Length;
            }
        }
        return list[pos];
    }
    public static Tile[] PathFindReturned(Tile currentTile, Tile targetTile, Tile[] tiles, WallCord[] walls)
    {
        List<ANode> openSet = new List<ANode>();
        HashSet<ANode> closeSet = new HashSet<ANode>();
        List<Tile> tileList = new List<Tile>();
        tileList.AddRange(tiles);
        List<ANode> nodeMap = new List<ANode>();
        ANode targetNode = null;
        ANode startNode = null;
        Cord[] cords = Cord.TilesToCords(tiles);
        for (int i = 0; i < tiles.Length; i++)
        {
            nodeMap.Add(new ANode(tiles[i].cord.x, tiles[i].cord.y));
            if (tiles[i].cord == targetTile.cord)
            {
                targetNode = nodeMap[nodeMap.Count - 1];
            }
            if (tiles[i].cord == currentTile.cord)
            {
                startNode = nodeMap[nodeMap.Count - 1];
            }
        }
        if (targetNode == null) return null;
        openSet.Add(startNode);
        ANode currentNode = null;
        while (openSet.Count > 0)
        {
            currentNode = openSet[0];
            openSet.RemoveAt(0);
            closeSet.Add(currentNode);
            if (currentNode == targetNode)
            {
                return RetracePathReturned(startNode, currentNode, tileList);
            }
            foreach (ANode n in GetNeighbors(currentNode, nodeMap, tileList, walls, cords))
            {
                if (n == null) continue;
                if (closeSet.Contains(n))
                {
                    continue;
                }
                int newMoveCost = currentNode.gCost + GetDistance(currentNode, n);
                if (newMoveCost < n.gCost || !openSet.Contains(n))
                {
                    n.gCost = newMoveCost;
                    n.hCost = GetDistance(n, targetNode);
                    n.parent = currentNode;
                    if (!openSet.Contains(n))
                    {
                        openSet.Add(n);
                    }
                }
            }
        }
        Debug.Log("Path not Complete");
        ANode[] nodeArray = new ANode[closeSet.Count];
        closeSet.CopyTo(nodeArray);
        ANode currentEndNode = currentNode;
        int dis = int.MinValue;
        for (int i = 0; i < nodeArray.Length; i++)
        {
            if (nodeArray[i] == null)
            {
                Debug.Log("Node Array Failed");
                return null;
            }
            int d = GetDistance(nodeArray[i], targetNode);
            if (d > dis)
            {
                currentEndNode = nodeArray[i];
                dis = d;
            }
        }
        return RetracePathReturned(startNode, currentEndNode, tileList);
    }
    public static Tile[] RetracePathReturned(ANode start, ANode end, List<Tile> allTiles)
    {
        List<ANode> path = new List<ANode>();
        ANode currentNode = end;
        while (currentNode != start)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        path.Reverse();
        Tile[] tilePath = new Tile[path.Count];
        for (int i = 0; i < path.Count; i++)
        {
            tilePath[i] = allTiles.Find(x => x.cord == path[i]);
        }
        return tilePath;
    }
    static int GetDistance(ANode a, ANode b)
    {
        int disX = Mathf.Abs(a.gridx - b.gridx);
        int disY = Mathf.Abs(a.gridy - b.gridy);
        if (disX > disY)
        {
            return (10 + (b.gridx)) * (disX - disY);
        }
        return (10 + (b.gridx)) * (disY - disX);
    }
    public static List<ANode> GetNeighbors(ANode node, List<ANode> nodeList, List<Tile> tileMap, WallCord[] walls, Cord[] cords)
    {
        List<ANode> neighbors = new List<ANode>();
        if (node == null) return neighbors;
        //Left!!
        ANode c = nodeList.Find(x => x.gridx == node.gridx - 1 && x.gridy == node.gridy);
        if (c != null && CheckLeft(cords, walls, Cord.NodeToCord(node)))
        {
            Tile t = tileMap.Find(x => c == x.cord);
            if (t != null)
            {
                neighbors.Add(c);
            }
        }
        //Right
        c = nodeList.Find(x => x.gridx == node.gridx + 1 && x.gridy == node.gridy);
        if (c != null && CheckRight(cords, walls, Cord.NodeToCord(node)))
        {
            Tile t = tileMap.Find(x => c == x.cord);
            if (t != null)
            {
                neighbors.Add(c);
            }
        }
        //uP
        c = nodeList.Find(x => x.gridx == node.gridx && x.gridy == node.gridy + 1);
        if (c != null && CheckUp(cords, walls, Cord.NodeToCord(node)))
        {
            Tile t = tileMap.Find(x => c == x.cord);
            if (t != null)
            {
                neighbors.Add(c);
            }
        }
        //doWn
        c = nodeList.Find(x => x.gridx == node.gridx && x.gridy == node.gridy - 1);
        if (c != null && CheckDown(cords, walls, Cord.NodeToCord(node)))
        {
            Tile t = tileMap.Find(x => c == x.cord);
            if (t != null)
            {
                neighbors.Add(c);
            }
        }
        return neighbors;
    }
    public static bool FloodFillCheck(Cord[] c)
    {
        Queue<Cord> open = new Queue<Cord>();
        open.Enqueue(c[0]);
        List<Cord> closed = new List<Cord>();
        List<Cord> all = new List<Cord>(c);
        while (open.Count > 0)
        {
            Cord temp = open.Dequeue();
            closed.Add(temp);
            Cord[] nei = GetNeighbors(temp, all);
            for (int i = 0; i < nei.Length; i++)
            {
                if (!closed.Contains(nei[i]) && !open.Contains(nei[i]))
                {
                    open.Enqueue(nei[i]);
                }
            }
        }

        return closed.Count == c.Length;
    }
    static Cord[] GetNeighbors(Cord c, List<Cord> all)
    {
        List<Cord> list = new List<Cord>();
        Cord left = new Cord(c.x - 1, c.y);
        if (all.Contains(left))
        {
            list.Add(left);
        }
        Cord right = new Cord(c.x + 1, c.y);
        if (all.Contains(right))
        {
            list.Add(right);
        }
        Cord up = new Cord(c.x, c.y + 1);
        if (all.Contains(up))
        {
            list.Add(up);
        }
        Cord down = new Cord(c.x, c.y - 1);
        if (all.Contains(down))
        {
            list.Add(down);
        }
        return list.ToArray();
    }

    public static bool CheckLeft(Cord[] cords, WallCord[] wallCords, Cord pos)
    {
        Cord left = new Cord(pos.x - 1, pos.y);
        bool check = false;
        for (int i = 0; i < cords.Length; i++)
        {
            if(left == cords[i])
            {
                check = true;
            }
        }
        if (!check) return false;
        for (int i = 0; i < wallCords.Length; i++)
        {
            if(left == wallCords[i].cord && wallCords[i].pos == WallPos.Right)
            {
                return false;
            }
        }
        return true;
    }
    public static bool CheckRight(Cord[] cords, WallCord[] wallCords, Cord pos)
    {
        Cord right = new Cord(pos.x + 1, pos.y);
        bool check = false;
        for (int i = 0; i < cords.Length; i++)
        {
            if (right == cords[i])
            {
                check = true;
            }
        }
        if (!check) return false;
        for (int i = 0; i < wallCords.Length; i++)
        {
            if (pos == wallCords[i].cord && wallCords[i].pos == WallPos.Right)
            {
                return false;
            }
        }
        return true;
    }
    public static bool CheckUp(Cord[] cords, WallCord[] wallCords, Cord posDontModify)
    {
        Cord up = new Cord(posDontModify.x, posDontModify.y + 1);
        bool check = false;
        for (int i = 0; i < cords.Length; i++)
        {
            if (up == cords[i])
            {
                check = true;
            }
        }
        if (!check) return false;
        for (int i = 0; i < wallCords.Length; i++)
        {
            if (posDontModify == wallCords[i].cord && wallCords[i].pos == WallPos.Up)
            {
                return false;
            }
        }
        return true;
    }
    public static bool CheckDown(Cord[] cords, WallCord[] wallCords, Cord pos)
    {
        Cord down = new Cord(pos.x, pos.y - 1);
        bool check = false;
        for (int i = 0; i < cords.Length; i++)
        {
            if (down == cords[i])
            {
                check = true;
            }
        }
        if (!check) return false;
        for (int i = 0; i < wallCords.Length; i++)
        {
            if (down == wallCords[i].cord && wallCords[i].pos == WallPos.Up)
            {
                return false;
            }
        }
        return true;
    }

    public static bool DoomsDayGeneratorCheck(Cord target, Cord[] cords, WallCord[] walls, Cord[] doomsGens)
    {
        for (int i = 0; i < doomsGens.Length; i++)
        {
            if(target == doomsGens[i])
            {
                return false;
            }
        }
        if(CheckUp(cords, walls, target))
        {
            Cord c = new Cord(target.x, target.y + 1);
            for (int i = 0; i < doomsGens.Length; i++)
            {
                if(c == doomsGens[i])
                {
                    return false;
                }
            }
        }
        if (CheckDown(cords, walls, target))
        {
            Cord c = new Cord(target.x, target.y - 1);
            for (int i = 0; i < doomsGens.Length; i++)
            {
                if (c == doomsGens[i])
                {
                    return false;
                }
            }
        }
        if (CheckLeft(cords, walls, target))
        {
            Cord c = new Cord(target.x - 1, target.y);
            for (int i = 0; i < doomsGens.Length; i++)
            {
                if (c == doomsGens[i])
                {
                    return false;
                }
            }
        }
        if (CheckRight(cords, walls, target))
        {
            Cord c = new Cord(target.x + 1, target.y);
            for (int i = 0; i < doomsGens.Length; i++)
            {
                if (c == doomsGens[i])
                {
                    return false;
                }
            }
        }
        return true;
    }
}
