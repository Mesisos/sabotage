﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class ANode : IHeapItem<ANode>
{
    public ANode parent;
    public int gCost;
    public int hCost;
    public int gridx;
    public int gridy;
    public int fCost
    {
        get
        {
            return gCost + hCost;
        }
    }
    public void Clear()
    {
        gCost = 0;
        hCost = 0;
    }
    int heapIndex;
    public ANode(int gridx, int gridy)
    {
        this.gridx = gridx;
        this.gridy = gridy;
    }
    public int HeapIndex
    {
        get
        {
            return heapIndex;
        }
        set
        {
            heapIndex = value;
        }
    }
    public int CompareTo(ANode n)
    {
        int compare = fCost.CompareTo(n.fCost);
        if (compare == 0)
        {
            compare = hCost.CompareTo(n.hCost);
        }
        return -compare;
    }
    public int Distance(ANode n)
    {
        int x = Mathf.Abs(gridx - n.gridx);
        int y = Mathf.Abs(gridy - n.gridy);
        return x + y;
    }
    public static ANode CordToNode(Cord c)
    {
        return new ANode(c.x, c.y);
    }
    public static bool operator ==(ANode c1, Cord c2)
    {
        return c1.gridx == c2.x && c1.gridy == c2.y;
    }
    public static bool operator !=(ANode c1, Cord c2)
    {
        return c1.gridx != c2.x || c1.gridy != c2.y;
    }
    public static bool operator ==(Cord c1, ANode c2)
    {
        return c1.x == c2.gridx && c1.y == c2.gridy;
    }
    public static bool operator !=(Cord c1, ANode c2)
    {
        return c1.x != c2.gridx || c1.y != c2.gridy;
    }
    public override string ToString()
    {
        return "X: " + gridx + " Y: " + gridy;
    }
}

