﻿using Doozy.Engine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CryBabyMaster : OverlayBase
{
    public static CryBabyMaster instance;

    private void Awake()
    {
        instance = this;
    }
    [SerializeField]
    TwoClickButton pre;
    [SerializeField]
    Button mainButton;
    public List<TwoClickButton> list = new List<TwoClickButton>();
    [SerializeField]
    UIView pushView;
    [SerializeField]
    UIView playerView;
    public override void Setup()
    {
        base.Setup();
        CreateButtons();
        pushView.Show();
        playerView.Hide();
    }
    public void ButtonPressed()
    {
        if (list.Count > 0)
        {
            ClearOverlay();
        }
        else
        {
            Setup();
        }
    }
    void CreateButtons()
    {
        for (int i = 0; i < GridMaster.instnace.allTiles.Count; i++)
        {
            CreateButton(GridMaster.instnace.allTiles[i]);
        }
        for (int i = 0; i < list.Count; i++)
        {
            list[0].pairedList.Add(list[i]);
        }
    }
    void CreateButton(Tile til)
    {
        Transform t = Instantiate(pre.transform) as Transform;
        TwoClickButton b = t.GetComponent<TwoClickButton>();
        Vector3 tilePos = til.transform.position;
        float amount = til.GetComponent<RectTransform>().sizeDelta.x / 2;
        tilePos.y += amount;
        tilePos.x += amount;
        t.SetParent(transform, false);
        t.transform.position = tilePos;
        b.Setup(ButtonPressed, "Cry Baby", til);
        list.Add(b);
    }
    public override void ClearOverlay()
    {
        if (overlayShown)
        {
            pushView.Hide();
            playerView.Show();
        }
        base.ClearOverlay();
        TwoClickButton[] temp = list.ToArray();
        for (int i = 0; i < temp.Length; i++)
        {
            Destroy(temp[i].gameObject);
        }
        list.Clear();
        mainButton.GetComponentInChildren<TextMeshProUGUI>().text = "Cry Baby";
    }
    public void ButtonPressed(Tile t)
    {
        Debug.Log(t.cord.ToString() + " Cry Baby");
        List<Tile> tiles = new List<Tile>();
        Tile up = GridMaster.instnace.FindTile(new Cord(t.cord.x, t.cord.y + 1));
        Tile down = GridMaster.instnace.FindTile(new Cord(t.cord.x, t.cord.y - 1));
        Tile left = GridMaster.instnace.FindTile(new Cord(t.cord.x - 1, t.cord.y));
        Tile right = GridMaster.instnace.FindTile(new Cord(t.cord.x + 1, t.cord.y));
        tiles.Add(up);
        tiles.Add(down);
        tiles.Add(left);
        tiles.Add(right);
        for (int k = 0; k < tiles.Count; k++)
        {
            if (tiles[k] == null) continue;
            for (int i = 0; i < GuardMaster.instance.guard.Length; i++)
            {
                if (GuardMaster.instance.guard[i].tile.cord == tiles[k].cord)
                {
                    Debug.Log("Cry Baby: Moved Guard " + GuardMaster.instance.guard[i].gameObject.name + " to " + t.cord.ToString());
                    GuardMaster.instance.guard[i].Move(t);
                }
            }
        }
        ClearOverlay();
    }
}
